package com.firstcut.enums;

public enum UserAccountStatus {
	ACTIVE("ACTIVE"), INACTIVE("INACTIVE");

	private final String value;

	private UserAccountStatus(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static UserAccountStatus fromValue(String value) {
		if (value != null) {
			for (UserAccountStatus type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
