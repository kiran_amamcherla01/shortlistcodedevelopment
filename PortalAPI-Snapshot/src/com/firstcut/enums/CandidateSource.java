package com.firstcut.enums;


public enum CandidateSource {
	NAUKRI("NAUKRI"),
	MONSTER("MONSTER"),
	NAUKRIDB("NAUKRIDB"),
	MONSTERDB("MONSTERDB"),
	LINKEDIN("LINKEDIN"),
	IIMJOBS("IIMJOBS"),
	DIRECT("DIRECT");
	
	
	private final String value;

	private CandidateSource(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}
	
	public static CandidateSource fromValue(String value) {
		if (value != null) {
			for (CandidateSource type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}

}
