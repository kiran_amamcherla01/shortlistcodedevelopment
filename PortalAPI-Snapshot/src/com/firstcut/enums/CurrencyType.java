package com.firstcut.enums;

public enum CurrencyType {
	USD("USD"), INR("INR");

	private final String value;

	private CurrencyType(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static CurrencyType fromValue(String value) {
		if (value != null) {
			for (CurrencyType type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
