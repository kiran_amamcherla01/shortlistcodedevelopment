package com.firstcut.enums;

public enum Grade {
	Exceeds_Expectations("Exceeds Expectations"), 
	Meets_Expectations("Meets Expectations"),
	Below_Expectations("Below Expectations"),
	Outstanding("Outstanding"),
	NA("N/A");
	
	private final String value;

	private Grade(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static Grade fromValue(String value) {
		if (value != null) {
			for (Grade type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
