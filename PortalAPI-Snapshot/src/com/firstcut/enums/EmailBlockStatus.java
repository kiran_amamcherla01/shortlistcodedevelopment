package com.firstcut.enums;

public enum EmailBlockStatus {
	BLOCKED("BLOCKED"), 
	UNBLOCKED("UNBLOCKED");
	
	private final String value;

	private EmailBlockStatus(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static EmailBlockStatus fromValue(String value) {
		if (value != null) {
			for (EmailBlockStatus type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
