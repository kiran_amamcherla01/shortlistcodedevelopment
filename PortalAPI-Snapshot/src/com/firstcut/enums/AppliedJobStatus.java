package com.firstcut.enums;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.firstcut.constant.Constant;

public enum AppliedJobStatus {
	PRESCREENING_PASS("PRESCREENING_PASS"), NEW("NEW"), PRESCREENING_FAIL(
			"PRESCREENING_FAIL"), ACCOUNT_CREATED("ACCOUNT_CREATED"), SCREENING_PASSED(
			"SCREENING_PASSED"), SCREENING_FAILED("SCREENING_FAILED"), ASSESSMENTS_PENDING(
			"ASSESSMENTS_PENDING"), ASSESSMENTS_COMPLETED(
			"ASSESSMENTS_COMPLETED"), PSYCHOMETRICS("PSYCHOMETRICS"), CASESTUDY(
			"CASESTUDY"), VOICEINTERVIEW("VOICEINTERVIEW"), PSYCHOMETRICS_CASESTUDY(
			"PSYCHOMETRICS_CASESTUDY"), PSYCHOMETRICS_VOICEINTERVIEW(
			"PSYCHOMETRICS_VOICEINTERVIEW"), CASESTUDY_VOICEINTERVIEW(
			"CASESTUDY_VOICEINTERVIEW"), PSYCHOMETRICS_CASESTUDY_VOICEINTERVIEW(
			"PSYCHOMETRICS_CASESTUDY_VOICEINTERVIEW"), APPLICATION_SUBMITTED(
			"APPLICATION_SUBMITTED"), SCORING_COMPLETE("SCORING_COMPLETE"), SHORTLIST(
			"SHORTLIST"), NOT_SHORTLIST("NOT_SHORTLIST"), APPROVED("APPROVED"), DECLINED(
			"DECLINED"), DECLINED_AUTO("DECLINED_AUTO"), WAITLIST("WAITLIST");

	private final String value;

	private AppliedJobStatus(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static AppliedJobStatus fromValue(String value) {
		if (value != null) {
			for (AppliedJobStatus type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}

	public static List<String> getDefaultFilterStatus(String companyName) {
		List<String> status = new ArrayList<String>();
		if (!companyName.equalsIgnoreCase(Constant.SHORTLIST)) {
			status.add(APPROVED.toString());
			status.add(DECLINED.toString());
			status.add(WAITLIST.toString());
			status.add(SHORTLIST.toString());
		} else {
			EnumSet<AppliedJobStatus> all = EnumSet
					.allOf(AppliedJobStatus.class);
			for (AppliedJobStatus s : all) {
				status.add(s.toValue());
			}
		}
		return status;
	}

	// public static List<String> getDefaultFilterStatus() {
	// List<String> status = new ArrayList<String>();
	// EnumSet<AppliedJobStatus> all = EnumSet
	// .allOf(AppliedJobStatus.class);
	// for (AppliedJobStatus s : all) {
	// status.add(s.toValue());
	// }
	// return status;
	// }
	//
	public static List<String> getEmployerStatus() {
		List<String> status = new ArrayList<String>();
		status.add(APPROVED.toString());
		status.add(DECLINED.toString());
		status.add(SHORTLIST.toString());
		status.add(NOT_SHORTLIST.toString());
		status.add(WAITLIST.toString());
		status.add(SCORING_COMPLETE.toString());
		status.add(APPLICATION_SUBMITTED.toString());
		status.add(DECLINED_AUTO.toString());
		return status;
	}

	public static Set<String> getAllStatus() {
		Set<String> status = new HashSet<String>();
		EnumSet<AppliedJobStatus> all = EnumSet.allOf(AppliedJobStatus.class);
		for (AppliedJobStatus s : all) {
			status.add(s.toValue());
		}
		return status;
	}

}
