package com.firstcut.enums;

public enum UserType {
	CANDIDATE("CANDIDATE"), EMPLOYER("EMPLOYER"),ADMIN("ADMIN");
	private final String value;

	private UserType(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static UserType fromValue(String value) {
		if (value != null) {
			for (UserType type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
