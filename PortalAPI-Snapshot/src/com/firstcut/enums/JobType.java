package com.firstcut.enums;

public enum JobType {
	//'FT','PT','UE','E'
	FT("FT"), PT("PT"), E("E"), UE("UE");
	private final String value;

	private JobType(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static JobType fromValue(String value) {
		if (value != null) {
			for (JobType type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
