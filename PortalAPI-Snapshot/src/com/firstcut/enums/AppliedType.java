package com.firstcut.enums;

public enum AppliedType {
	PREMIUM("PREMIUM"), EXCLUDED("EXCLUDED"), REGULAR("REGULAR");

	private final String value;

	private AppliedType(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}
	
	public static AppliedType fromValue(String value) {
		if (value != null) {
			for (AppliedType type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
