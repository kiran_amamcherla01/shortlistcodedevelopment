package com.firstcut.enums;

public enum JobOpeningStatus {
	OPEN("OPEN"), 
	CLOSED("CLOSED");
	
	private final String value;

	private JobOpeningStatus(String value) {
		this.value = value;
	}

	public String toValue() {
		return value;
	}

	public static JobOpeningStatus fromValue(String value) {
		if (value != null) {
			for (JobOpeningStatus type : values()) {
				if (type.value.equals(value)) {
					return type;
				}
			}
		}
		return null;
	}
}
