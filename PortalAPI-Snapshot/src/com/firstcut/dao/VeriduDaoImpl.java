package com.firstcut.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.firstcut.dto.VeriduUser;

public class VeriduDaoImpl extends BaseDao implements VeriduDao {

	@Override
	public void saveVeriduUser(VeriduUser veriduUser) {
		save(veriduUser);
	}

	public VeriduUser getUserById(int veriduId) {
		Session session = getCurrentSession();
		Criteria criteria = session.createCriteria(VeriduUser.class)
				.add(Restrictions.eq("veriduId", veriduId)).setCacheable(true);
		Object uniqueResult = criteria.uniqueResult();
		VeriduUser veriduUser = (VeriduUser) uniqueResult;
		return veriduUser;

	}
}
