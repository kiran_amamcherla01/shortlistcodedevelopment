package com.firstcut.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.stat.SecondLevelCacheStatistics;
import org.hibernate.stat.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class BaseDao {
	private Logger logger = Logger.getLogger(this.getClass().getName());
	@Autowired
	@Qualifier("sessionFactory")
	protected SessionFactory sessionFactory;
	protected Session session;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getCurrentSession() {
		try{
		session = sessionFactory.getCurrentSession();
		if (session == null) {
			session = sessionFactory.openSession();
		}
		}catch(Exception exception){
			session = sessionFactory.openSession();
		}
		return session;
	}

	protected void save(Object obj) {
		session = getCurrentSession();
		session.save(obj);
	}
	protected void merge(Object obj) {
		session = getCurrentSession();
		session.merge(obj);
	}
	protected void persist(Object obj) {
		session = getCurrentSession();
		session.persist(obj);
	}

	protected void saveOrUpdate(Object obj) {
		session = getCurrentSession();
		session.saveOrUpdate(obj);
	}

	protected void update(Object obj) {
		session = getCurrentSession();
		session.update(obj);
	}

	protected void delete(Object obj) {
		session = getCurrentSession();
		session.delete(obj);
	}

	protected Object findById(Class<?> className, int id) {
		Object object = null;
		session = getCurrentSession();
		object = session.get(className, id);
		return object;
	}

	public void printStatistics(SessionFactory sessionFactory) {
		Statistics stat = sessionFactory.getStatistics();
		String regions[] = stat.getSecondLevelCacheRegionNames();
		logger.info(regions.toString());
		for (String regionName : regions) {
			SecondLevelCacheStatistics stat2 = stat
					.getSecondLevelCacheStatistics(regionName);
			logger.info("2nd Level Cache(" + regionName + ") Put Count: "
					+ stat2.getPutCount());
			logger.info("2nd Level Cache(" + regionName + ") HIt Count: "
					+ stat2.getHitCount());
			logger.info("2nd Level Cache(" + regionName + ") Miss Count: "
					+ stat2.getMissCount());
		}
	}
}
