package com.firstcut.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.constant.Constant;
import com.firstcut.dto.AuditLog;
import com.firstcut.dto.ContactUs;
import com.firstcut.dto.OAuthClientDetails;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;

public class UserDaoImpl extends BaseDao implements UserDao {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	public User getUserByAuthToken(String token) throws DataBaseException {
		String METHOD_NAME = "getUserByAuthToken";
		User user = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(User.class).add(Restrictions.eq("authToken", token))
					.setCacheable(true);
			user = (User) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return user;
	}

	@Override
	public User getUserByConfirmToken(String confirmToken) throws DataBaseException {
		String METHOD_NAME = "getUserByConfirmToken";
		User user = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(User.class).add(Restrictions.eq("confirmToken", confirmToken))
					.setCacheable(true);
			user = (User) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return user;
	}

	public User getUserByUserNameAndType(String userName, UserType userType) throws DataBaseException {
		String METHOD_NAME = "getUserByUserName";
		User user = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(User.class).add(Restrictions.eq("userName", userName))
					.add(Restrictions.eq("userType", userType)).setCacheable(true);
			Object uniqueResult = criteria.uniqueResult();
			user = (User) uniqueResult;
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return user;
	}

	

	public User saveUser(User user) throws DataBaseException {
		String METHOD_NAME = "saveUser";
		try {
			save(user);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return user;
	}

	@Override
	public SocialProfile saveSocialProfile(SocialProfile socialProfile) throws DataBaseException {
		String METHOD_NAME = "saveSocialProfile";
		try {
			save(socialProfile);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return socialProfile;
	}

	@Override
	public User getUserById(int userId) throws DataBaseException {
		User user = null;
		String METHOD_NAME = "getUserById";
		try {
			Object findById = findById(User.class, userId);
			if (findById != null) {
				user = (User) findById;
			}
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return user;

	}

	@Override
	public void createConatctUsDetails(ContactUs contactUs) {
		String METHOD_NAME = "createConatctUsDetails";
		try {
			save(contactUs);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
	}

	@Override
	public void createAuditLog(AuditLog auditLog) {
		String METHOD_NAME = "auditLog";
		try {
			save(auditLog);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
		}
	}

}
