package com.firstcut.dao;

import java.util.List;
import java.util.Set;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.CompanyProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Media;
import com.firstcut.model.SearchFilterModel;

public interface CompanyDao {

	Media getMedia(int companyId, int mediaId);

	List<Media> getAllMedia(int companyId);

	CompanyProfile getCompanyById(int companyId);

	List<CompanyProfile> getAllCompany(String filter);

	CompanyProfile saveCompany(CompanyProfile companyProfile);

	CompanyProfile updateCompany(CompanyProfile companyProfile);

	HiringManager getManagerByEmail(String email);

	HiringManager saveManager(HiringManager manager);

	List<HiringManager> getAllManager(int companyId);

	HiringManager getManagerById(int companyId, int managerId);

	JobOpening getJobOpening(int companyId, int jobOpeningId);

	JobOpening getJobOpeningById(int jobOpeningId);

	JobOpening updateJobOpening(JobOpening jobOpening);

	CompanyProfile getCompanyByName(String companyName);

	JobOpening getJobOpening(String companyname, int jobOpeningId);

	void deleteMedia(Media media);

	List<CandidateProfile> getAppliedCandidatesByStatusAndJobId(int jobId,
			String emailStatus, String Status, int interval);

	List<JobOpening> getAllJobOpeningsByCompanyId(int companyId,
			String jobOpeningStatus);

	List<JobOpening> getAllJobOpenings(String jobOpeningStatus);

	List<JobOpening> getAllJobOpeningsByEmailStatus(String jobOpeningStatus);

	List<JobOpening> getAllJobOpeningsByCompanyName(String companyName,
			String jobOpeningStatus);

	List<CaseQuestion> getCaseQuestion(int jobOpeningId);

	List<JobOpening> getAllAppliedCandidatesByCompanyId(int companyId,
			String jobOpeningStatus, Set<String> jobStatus);

	List<CompanyProfile> getCompaniesById(SearchFilterModel model);

	JobOpening crateJob(JobOpening jobOpening);

}
