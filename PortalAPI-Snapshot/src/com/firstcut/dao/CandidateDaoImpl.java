package com.firstcut.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.constant.Constant;
import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseAnswer;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobHistory;
import com.firstcut.dto.Media;
import com.firstcut.dto.OAuthClientDetails;
import com.firstcut.dto.Psychometric;
import com.firstcut.dto.User;
import com.firstcut.util.OAuthTokenUtil;

public class CandidateDaoImpl extends BaseDao implements CandidateDao {

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateProfile> getAllCandidate() {
		String METHOD_NAME = "getAllCandidate";
		List<CandidateProfile> candidateProfiles = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateProfile.class);
			candidateProfiles = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateProfiles;
	}

	@Override
	public CandidateProfile getCandidateByPhone(String phoneNumber) {
		String METHOD_NAME = "getCandidateByPhone";
		CandidateProfile candidate = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateProfile.class)
					.add(Restrictions.eq("phone", phoneNumber))
					.setCacheable(true);
			candidate = (CandidateProfile) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidate;
	}

	@Override
	public CandidateProfile saveCandidate(CandidateProfile candidateProfile) {
		String METHOD_NAME = "saveCandidate";
		try {
			saveOrUpdate(candidateProfile);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateProfile;
	}

	@Override
	public CandidateProfile getCandidateById(int candidateId) {
		String METHOD_NAME = "getCandidateById";
		CandidateProfile candidate = null;
		try {
			Object findById = findById(CandidateProfile.class, candidateId);
			candidate = (CandidateProfile) findById;
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidate;
	}

	@Override
	public CandidateProfile getCandidateByIdAndJobId(int jobId, int candidateId) {
		String METHOD_NAME = "getCandidateByIdAndJobId";
		CandidateProfile candidate = null;
		try {
			Session session = getCurrentSession();
			Filter candidateIdFilter = session.enableFilter("candidateFilter");
			candidateIdFilter.setParameter("candidateId",candidateId);
			Filter jobIdFilter = session.enableFilter("jobIdFilter");
			jobIdFilter.setParameter("jobId",jobId);

			Filter stageFilter = session.enableFilter("stageFilter");
			stageFilter.setParameter("stage",Constant.POSTSCREEN);

			Criteria criteria = session.createCriteria(CandidateProfile.class)
					.add(Restrictions.eq("userId", candidateId));
			candidate = (CandidateProfile) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidate;
	}

	@Override
	public CandidateProfile getCandidateByEmail(String emailId) {
		String METHOD_NAME = "getCandidateByEmail";
		CandidateProfile candidate = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateProfile.class)
					.add(Restrictions.eq("email", emailId).ignoreCase()).setCacheable(true);
			candidate = (CandidateProfile) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidate;
	}

	@Override
	public CandidateProfile updateCandidate(CandidateProfile candidateProfile) {
		String METHOD_NAME = "updateCandidate";
		try {
			merge(candidateProfile);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateProfile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> getAllMedia(int candidateId) {
		String METHOD_NAME = "getAllMedia";
		List<Media> medias = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(Media.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			medias = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return medias;
	}

	@Override
	public Media getMedia(int candidateId, int mediaId) {
		String METHOD_NAME = "getMedia";
		Media media = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(Media.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("id", mediaId));
			media = (Media) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return media;
	}

	@Override
	public Media updateMedia(Media media) {
		String METHOD_NAME = "updateMedia";
		try {
			update(media);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return media;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobHistory> getAllJobHistory(int candidateId) {
		String METHOD_NAME = "getAllJobHistory";
		List<JobHistory> jobs = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobHistory.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			jobs = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobs;
	}

	@Override
	public JobHistory getJobHistory(int candidateId, int jobId) {
		String METHOD_NAME = "getJobHistory";
		JobHistory jobHistory = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobHistory.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("id", jobId));
			jobHistory = (JobHistory) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobHistory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobApplied> getAllAppliedJobs(int candidateId, int rowCount) {
		String METHOD_NAME = "getAllAppliedJobs";
		List<JobApplied> jobApplieds = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			ProjectionList projections = Projections.projectionList()
					.add(Projections.property("jobStatus").as("jobStatus"))
					.add(Projections.property("id").as("id"))
					.add(Projections.property("jobOpening").as("jobOpening"));
			Criteria criteria = session.createCriteria(JobApplied.class)
					.createAlias("candidateProfile", "candidate");
			criteria.setProjection(projections);
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.addOrder(Order.desc("createDate"));
			criteria.setMaxResults(rowCount);
			criteria.setResultTransformer(Transformers
					.aliasToBean(JobApplied.class));
			jobApplieds = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobApplieds;
	}

	@Override
	public JobApplied getAppliedJob(int candidateId, int jobId) {
		String METHOD_NAME = "getAppliedJob";
		JobApplied jobApplied = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobApplied.class)
					.createAlias("candidateProfile", "candidateProfile")
					.createAlias("jobOpening", "jobOpening");
			criteria.add(Restrictions
					.eq("candidateProfile.userId", candidateId));
			criteria.add(Restrictions.eq("jobOpening.id", jobId));
			jobApplied = (JobApplied) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobApplied;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EducationSummary> getAllEducation(int candidateId) {
		String METHOD_NAME = "getAllEducation";
		List<EducationSummary> educationSummaries = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(EducationSummary.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			educationSummaries = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return educationSummaries;
	}

	@Override
	public EducationSummary getEducation(int candidateId, int eId) {
		String METHOD_NAME = "getEducation";
		EducationSummary educationSummary = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(EducationSummary.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("id", eId));
			educationSummary = (EducationSummary) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return educationSummary;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AudioInterview> getAllAudioInterview(int candidateId) {
		String METHOD_NAME = "getAllAudioInterview";
		List<AudioInterview> audioInterviews = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(AudioInterview.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			audioInterviews = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return audioInterviews;
	}

	@Override
	public AudioInterview getAudioInterview(int candidateId, int jobId) {
		String METHOD_NAME = "getAudioInterview";
		AudioInterview audioInterview = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(AudioInterview.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("jobId", jobId));
			audioInterview = (AudioInterview) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return audioInterview;
	}

	@Override
	public void createAudioInterview(AudioInterview audioInterview) {
		String METHOD_NAME = "createAudioInterview";
		try {
			saveOrUpdate(audioInterview);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
	}

	@Override
	public void createScoring(CandidateScoring candidateScore) {
		String METHOD_NAME = "createScoring";
		try {
			session = getCurrentSession();
			session.save(candidateScore);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
	}

	@SuppressWarnings("unchecked")
	public AudioInterview getAudioInterviewBySid(String callsid) {
		String METHOD_NAME = "getAudioInterviewBySid";
		AudioInterview audioInterview = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(AudioInterview.class);
			criteria.add(Restrictions.eq("callsid", callsid));
			criteria.addOrder(Order.desc("createDate"));
			List<AudioInterview> list = criteria.list();
			if (list.size() > 0)
				audioInterview = (AudioInterview) list.get(0);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return audioInterview;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseAnswer> getAllWorkcases(int candidateId, String qtype) {
		String METHOD_NAME = "getAllWorkcases";
		List<CaseAnswer> caseAnswers = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CaseAnswer.class)
					.createAlias("candidateProfile", "candidate")
					.createAlias("caseQuestion", "caseQuestion");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("caseQuestion.qtype", qtype));
			caseAnswers = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return caseAnswers;
	}

	@Override
	public CaseAnswer getWorkcase(int candidateId, int caseId) {
		String METHOD_NAME = "getWorkcase";
		CaseAnswer caseAnswer = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CaseAnswer.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("id", caseId));
			caseAnswer = (CaseAnswer) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return caseAnswer;
	}

	@Override
	public CandidateProfile getCandidateByPin(String audioPin) {
		String METHOD_NAME = "getCandidateByPin";
		CandidateProfile candidate = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateProfile.class)
					.add(Restrictions.eq("audioPin", audioPin))
					.setCacheable(true);
			candidate = (CandidateProfile) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Psychometric> getPsychometric(int candidateId) {
		String METHOD_NAME = "getAllPsychometric";
		List<Psychometric> psychometrics = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(Psychometric.class)
					.createAlias("candidateProfile", "candidate");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			psychometrics = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return psychometrics;
	}

	@Override
	public CandidateScoring getCandidateScoring(int candidateId, int jobId,
			String stage) {
		String METHOD_NAME = "getCandidateScoring";
		CandidateScoring candidateScoring = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateScoring.class)
					.createAlias("candidateProfile", "candidate")
					.createAlias("jobOpening", "jobOpening");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("jobOpening.id", jobId));
			criteria.add(Restrictions.eq("stage", stage));
			candidateScoring = (CandidateScoring) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateScoring;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateScoring> getCandidateScoringForJob(int jobId,
			String stage){
		String METHOD_NAME = "getCandidateScoring";
		List<CandidateScoring> candidateScoring = new ArrayList<>();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateScoring.class)
					.createAlias("jobOpening", "jobOpening");
			criteria.add(Restrictions.eq("jobOpening.id", jobId));
			criteria.add(Restrictions.eq("stage", stage));
			candidateScoring = (List<CandidateScoring>) criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateScoring;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseAnswer> getWorkcaseByJob(int candidateId, int jobId,
			String qtype) {
		String METHOD_NAME = "getCandidateScoring";
		List<CaseAnswer> caseAnswers = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CaseAnswer.class)
					.createAlias("candidateProfile", "candidate")
					.createAlias("caseQuestion", "caseQuestion")
					.createAlias("jobOpening", "jobOpening");
			criteria.add(Restrictions.eq("candidate.userId", candidateId));
			criteria.add(Restrictions.eq("caseQuestion.qtype", qtype));
			criteria.add(Restrictions.eq("jobOpening.id", jobId));
			caseAnswers = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return caseAnswers;
	}

	@Override
	public void updateCandidateEmailStatus(String email, String emailStatus,
			int jobId) {
		String METHOD_NAME = "updateCandidateEmailStatus";
		try {
			Session session = getCurrentSession();
			Query query = session
					.createSQLQuery(
							"CALL UpdateEmailStatus(:emailList,:emailStatus,:jobId)")
					.addEntity(CandidateProfile.class)
					.setParameter("emailList", email)
					.setParameter("emailStatus", emailStatus)
					.setParameter("jobId", jobId);
			logger.info(query.getQueryString());
			query.executeUpdate();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
	}

	@Override
	public JobApplied updateAppliedJob(JobApplied jobApplied) {
		merge(jobApplied);
		return jobApplied;
	}

	@Override
	public int updateCandidateJobAndEmailStatusForAutoExpiry(
			String oldJobStatus, String newJobStatus, String newEmailStatus,
			int timeInterval) {
		String METHOD_NAME = "updateCandidateJobStatusForAutoExpiry";
		int updateCount = 0;
		try {
			Session session = getCurrentSession();
			Query query = session
					.createSQLQuery(
							"UPDATE jobapplied SET jobStatus=:newJobStatus , emailStatus=:emailStatus WHERE jobStatus=:jobStatus AND modifyDate + INTERVAL :timeInterval DAY <= NOW()")
					.setString("jobStatus", oldJobStatus)
					.setString("newJobStatus", newJobStatus)
					.setString("emailStatus", newEmailStatus)
					.setInteger("timeInterval", timeInterval);
			updateCount = query.executeUpdate();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return updateCount;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateProfile> getCandidatesByEmail(List<String> email) {
		String METHOD_NAME = "getCandidatesByEmail";
		List<CandidateProfile> candidates = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CandidateProfile.class)
					.add(Restrictions.in("email", email)).setCacheable(true);
			candidates = (List<CandidateProfile>) criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidates;
	}


	public void saveOAuthClientDetails(String authToken) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
		String encodedPassword = encoder.encode(authToken);
		String METHOD_NAME = "getOAuthClientDetails";
		OAuthClientDetails oAuthClientDetails = new OAuthClientDetails();
		logger.info("In saveOAuthClientDetails");
		try {
			oAuthClientDetails.setClientId(authToken);
			oAuthClientDetails.setResourceId("rest_api");
			oAuthClientDetails.setClientSecret(encodedPassword);
			oAuthClientDetails.setScope("trust,read,write");
			oAuthClientDetails
			.setAuthorizedGrantTypes("client_credentials,authorization_code,implicit,password,refresh_token");
			oAuthClientDetails.setAuthorities("ROLE_USER");
			oAuthClientDetails.setAccessTokenValidity(60);
			oAuthClientDetails.setRefreshTokenValidity(200);
			save(oAuthClientDetails);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
	}




	@Override
	public String getAuthToken(int candidateId) {
		String METHOD_NAME = "getAuthToken";
		List<User> users = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(User.class)
					.add(Restrictions.eq("id", candidateId)).setCacheable(true);
			users = (List<User>) criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return users.get(0).getAuthToken();
	}
	

}
