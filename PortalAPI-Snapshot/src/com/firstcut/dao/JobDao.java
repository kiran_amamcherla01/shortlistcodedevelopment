package com.firstcut.dao;

import java.util.List;
import java.util.Set;

import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.model.SearchFilterModel;

public interface JobDao {

	List<CaseQuestion> getCaseQuestion(int jobOpeningId, String qtype);

	void createCaseQuestions(CaseQuestion caseQuestion);

	void deleteCaseQuestion(CaseQuestion caseQuestion);

	CaseQuestion updateCaseQuestion(CaseQuestion caseQuestion);

	List<JobScoringCriteria> getDefaultScoringCriteria();

	List<JobScoringRelation> getJobScoringCriteria(int jobId);

	JobOpening getAllAppliedCandidatesByJobId(int jobId,
			Set<String> appliedJobStatus);

	List<JobOpening> getCandidatesForSearchCritera(SearchFilterModel model);

}
