package com.firstcut.dao;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.constant.Constant;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.CompanyProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Media;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.SearchFilterModel;

public class CompanyDaoImpl extends BaseDao implements CompanyDao {

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProfile> getAllCompany(String filter) {
		String METHOD_NAME = "getAllCompany";
		List<CompanyProfile> companyProfiles = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CompanyProfile.class);
			companyProfiles = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfiles;
	}

	@Override
	public CompanyProfile saveCompany(CompanyProfile companyProfile) {
		String METHOD_NAME = "saveCompany";
		try {
			save(companyProfile);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfile;
	}

	@Override
	public CompanyProfile getCompanyById(int companyId) {
		String METHOD_NAME = "getCompanyById";
		CompanyProfile companyProfile = null;
		try {
			Object findById = findById(CompanyProfile.class, companyId);
			companyProfile = (CompanyProfile) findById;
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CompanyProfile> getCompaniesById(SearchFilterModel model) {
		String METHOD_NAME = "getCompaniesById";
		List<CompanyProfile> companyProfiles = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CompanyProfile.class);
			criteria.add(Restrictions.in("id", model.getCompanyIds()));
			
			companyProfiles = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfiles;
	}
	
	@Override
	public CompanyProfile getCompanyByName(String companyName) {
		String METHOD_NAME = "getCompanyByName";
		CompanyProfile companyProfile = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CompanyProfile.class);
			criteria.add(Restrictions.eq("companyName", companyName));
			companyProfile = (CompanyProfile) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfile;
	}

	@Override
	public CompanyProfile updateCompany(CompanyProfile companyProfile) {
		String METHOD_NAME = "updateCompany";
		try {
			merge(companyProfile);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return companyProfile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> getAllMedia(int companyId) {
		String METHOD_NAME = "getAllMedia";
		List<Media> medias = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(Media.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			medias = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return medias;
	}

	@Override
	public Media getMedia(int companyId, int mediaId) {
		String METHOD_NAME = "getMedia";
		Media media = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(Media.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			criteria.add(Restrictions.eq("id", mediaId));
			media = (Media) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return media;
	}

	@Override
	public HiringManager getManagerByEmail(String email) {
		String METHOD_NAME = "getManagerByEmail";
		HiringManager manager = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(HiringManager.class);
			criteria.add(Restrictions.eq("email", email));
			manager = (HiringManager) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return manager;
	}

	@Override
	public HiringManager saveManager(HiringManager manager) {
		String METHOD_NAME = "saveManager";
		try {
			save(manager);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return manager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HiringManager> getAllManager(int companyId) {
		String METHOD_NAME = "getAllManager";
		List<HiringManager> managers = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(HiringManager.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			managers = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return managers;
	}

	@Override
	public HiringManager getManagerById(int companyId, int managerId) {
		String METHOD_NAME = "getManagerById";
		HiringManager manager = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(HiringManager.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			criteria.add(Restrictions.eq("id", managerId));
			manager = (HiringManager) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return manager;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getAllJobOpeningsByCompanyId(int companyId,
			String jobOpeningStatus) {
		String METHOD_NAME = "getAllJobOpenings";
		List<JobOpening> jobOpenings = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			criteria.add(Restrictions.eq("jobOpeningStatus",
					JobOpeningStatus.fromValue(jobOpeningStatus)));
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getAllJobOpenings(String jobOpeningStatus) {
		String METHOD_NAME = "getAllJobOpenings";
		List<JobOpening> jobOpenings = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("jobOpeningStatus",
					JobOpeningStatus.fromValue(jobOpeningStatus)));
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getAllJobOpeningsByEmailStatus(
			String jobOpeningStatus) {
		String METHOD_NAME = "getAllJobOpeningsByEmailStatus";
		List<JobOpening> jobOpenings = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("jobOpeningStatus",
					JobOpeningStatus.fromValue(jobOpeningStatus)));
			criteria.add(Restrictions.eq("sendEmail", Boolean.TRUE));
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

	@Override
	public JobOpening getJobOpening(int companyId, int jobOpeningId) {
		String METHOD_NAME = "getJobOpenings";
		JobOpening jobOpening = null;

		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id", companyId));
			criteria.add(Restrictions.eq("id", jobOpeningId));
			jobOpening = (JobOpening) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}

	@Override
	public JobOpening updateJobOpening(JobOpening jobOpening) {
		String METHOD_NAME = "updateJobOpenings";
		try {
			merge(jobOpening);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}

	@Override
	public JobOpening getJobOpening(String companyname, int jobOpeningId) {
		String METHOD_NAME = "getJobOpenings";
		JobOpening jobOpening = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.companyName", companyname));
			criteria.add(Restrictions.eq("id", jobOpeningId));
			jobOpening = (JobOpening) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}

	@Override
	public JobOpening getJobOpeningById(int jobOpeningId) {
		String METHOD_NAME = "getJobOpenings";
		JobOpening jobOpening = null;
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(JobOpening.class);
			criteria.add(Restrictions.eq("id", jobOpeningId));
			Object obj = criteria.uniqueResult();
			jobOpening = (JobOpening) obj;
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getAllJobOpeningsByCompanyName(String companyName,
			String jobOpeningStatus) {
		String METHOD_NAME = "getAllJobOpeningsByCompanyName";
		List<JobOpening> jobOpenings = null;
		try {
			Session session = getCurrentSession();
			Filter filter = session.enableFilter("jobStatusFilter");
			filter.setParameterList("jobStatusFilterParam",
					AppliedJobStatus.getDefaultFilterStatus(companyName));
			filter.validate();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.companyName", companyName));
			criteria.add(Restrictions.eq("jobOpeningStatus",
					JobOpeningStatus.fromValue(jobOpeningStatus)));
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CandidateProfile> getAppliedCandidatesByStatusAndJobId(
			int jobId, String emailStatus, String jobStatus, int interval) {
		String METHOD_NAME = "getAppliedCandidatesByStatusAndJobId";
		List<CandidateProfile> candidateProfiles = Collections.emptyList();
		logger.debug(jobId + "-" + emailStatus + "-" + jobStatus + "-"
				+ interval);
		try {
			Session session = getCurrentSession();
			Query query = session
					.createSQLQuery(
							"CALL followup(:jobId,:appliedStatus,:emailStatus,:timeInterval)")
					.addEntity(CandidateProfile.class)
					.setParameter("jobId", jobId)
					.setParameter("appliedStatus", jobStatus)
					.setParameter("emailStatus", emailStatus)
					.setParameter("timeInterval", interval);
			candidateProfiles = query.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return candidateProfiles;
	}

	@Override
	public void deleteMedia(Media media) {
		delete(media);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseQuestion> getCaseQuestion(int jobOpeningId) {
		String METHOD_NAME = "getCaseQuestion";
		List<CaseQuestion> caseQuestions = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CaseQuestion.class);
			criteria.createAlias("jobOpening", "job");
			criteria.add(Restrictions.eq("job.id", jobOpeningId));
			caseQuestions = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return caseQuestions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getAllAppliedCandidatesByCompanyId(int companyId,
			String jobOpeningStatus, Set<String> jobStatus) {
		String METHOD_NAME = "getAllJobOpenings";
		List<JobOpening> jobOpenings = null;
		try {
			Session session = getCurrentSession();
			Filter filter = session.enableFilter("jobStatusFilter");
			filter.setParameterList("jobStatusFilterParam", jobStatus);
			filter.validate();
			Criteria criteria = session.createCriteria(JobOpening.class)
					.createAlias("companyProfile", "company");
			criteria.add(Restrictions.eq("company.id",companyId));
			criteria.add(Restrictions.eq("jobOpeningStatus",
					JobOpeningStatus.fromValue(jobOpeningStatus)));
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

	@Override
	public JobOpening crateJob(JobOpening jobOpening) {
		String METHOD_NAME = "saveCompany";
		try {
			save(jobOpening);
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}
}
