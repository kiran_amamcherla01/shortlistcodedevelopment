package com.firstcut.dao;

import com.firstcut.dto.VeriduUser;

public interface VeriduDao {

	void saveVeriduUser(VeriduUser veriduUser);
	
	 VeriduUser getUserById(int veriduId);
}
