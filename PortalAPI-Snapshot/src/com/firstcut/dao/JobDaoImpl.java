package com.firstcut.dao;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Filter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.constant.Constant;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.SearchFilterModel;

public class JobDaoImpl extends BaseDao implements JobDao {

	private Logger logger = Logger.getLogger(this.getClass().getName());

	@SuppressWarnings("unchecked")
	@Override
	public List<CaseQuestion> getCaseQuestion(int jobOpeningId, String qtype) {
		String METHOD_NAME = "getCaseQuestion";
		List<CaseQuestion> caseQuestions = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session.createCriteria(CaseQuestion.class);
			criteria.createAlias("jobOpening", "job");
			criteria.add(Restrictions.eq("job.id", jobOpeningId));
			if (qtype != null && !qtype.isEmpty())
				criteria.add(Restrictions.eq("qtype", qtype));
			caseQuestions = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return caseQuestions;
	}

	@Override
	public void createCaseQuestions(CaseQuestion caseQuestion) {
		saveOrUpdate(caseQuestion);
	}

	@Override
	public void deleteCaseQuestion(CaseQuestion caseQuestion) {
		delete(caseQuestion);

	}

	@Override
	public CaseQuestion updateCaseQuestion(CaseQuestion caseQuestion) {
		update(caseQuestion);
		return caseQuestion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobScoringCriteria> getDefaultScoringCriteria() {
		String METHOD_NAME = "getCaseQuestion";
		List<JobScoringCriteria> jobScoringCriterias = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session
					.createCriteria(JobScoringCriteria.class);
			jobScoringCriterias = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobScoringCriterias;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobScoringRelation> getJobScoringCriteria(int jobId) {
		String METHOD_NAME = "getCaseQuestion";
		List<JobScoringRelation> jobScoringRelations = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			Criteria criteria = session
					.createCriteria(JobScoringRelation.class);
			criteria.createAlias("jobOpening", "job");
			criteria.add(Restrictions.eq("job.id", jobId));
			jobScoringRelations = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobScoringRelations;
	}

	@Override
	public JobOpening getAllAppliedCandidatesByJobId(int jobId,
			Set<String> appliedJobStatus) {
		String METHOD_NAME = "getAppliedJob";
		JobOpening jobOpening = null;
		try {
			Session session = getCurrentSession();
			Filter filter = session.enableFilter("jobStatusFilter");
			filter.setParameterList("jobStatusFilterParam", appliedJobStatus);
			Criteria criteria = session.createCriteria(JobOpening.class);
			criteria.add(Restrictions.eq("id", jobId));
			jobOpening = (JobOpening) criteria.uniqueResult();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpening;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<JobOpening> getCandidatesForSearchCritera(
			SearchFilterModel model) {
		String METHOD_NAME = "getCandidatesForSearchCritera";
		List<JobOpening> jobOpenings = Collections.emptyList();
		try {
			Session session = getCurrentSession();
			if (model.getJobStatuses() != null
					&& !model.getJobStatuses().isEmpty()) {
				Filter filter = session.enableFilter("jobStatusFilter");
				filter.setParameterList("jobStatusFilterParam",
						model.getJobStatuses());
			}
			if (model.getSearchParam() != null
					&& !model.getSearchParam().isEmpty()
					&& model.getSearchText() != null
					&& !model.getSearchText().isEmpty()) {
				ProjectionList projections = Projections.projectionList()
						.add(Projections.property("userId").as("id"));
				Criteria candidateCriteria = session.createCriteria(CandidateProfile.class)
						.add(Restrictions.like(model.getSearchParam(), model.getSearchText(), MatchMode.ANYWHERE));
				candidateCriteria.setProjection(projections);
				List<Integer> candidateList=candidateCriteria.list();
				Filter filter = session.enableFilter("candidateIdFilter");
				filter.setParameterList("candidateIdList",
						candidateList);
			}
			Criteria criteria = session.createCriteria(JobOpening.class);
			criteria.add(Restrictions.eq("jobOpeningStatus",JobOpeningStatus.OPEN));
			criteria.createAlias("companyProfile", "company");
			criteria.createAlias("jobApplieds", "jobApplieds");
//			criteria.addOrder(Order.asc("jobApplieds.modifyDate"));
			if(model.getCompanyIds()!=null && !model.getCompanyIds().isEmpty()){
				criteria.add(Restrictions.in("company.id", model.getCompanyIds()));
			}
			if (model.getJobIds() != null && !model.getJobIds().isEmpty()) {
				criteria.add(Restrictions.in("id", model.getJobIds()));
			}
			jobOpenings = criteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return jobOpenings;
	}

}
