package com.firstcut.dao;

import java.util.List;

import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobHistory;
import com.firstcut.dto.Media;
import com.firstcut.dto.OAuthClientDetails;
import com.firstcut.dto.Psychometric;
import com.firstcut.dto.CaseAnswer;

public interface CandidateDao {

	CandidateProfile getCandidateByPhone(String phoneNumber);

	CandidateProfile saveCandidate(CandidateProfile candidateProfile);

	CandidateProfile getCandidateById(int candidateId);

	CandidateProfile getCandidateByEmail(String email);
	List<CandidateProfile> getCandidatesByEmail(List<String> email);

	CandidateProfile updateCandidate(CandidateProfile candidateProfile);

	List<CandidateProfile> getAllCandidate();

	List<Media> getAllMedia(int candidateId);

	void createAudioInterview(AudioInterview audioInterview);

	Media getMedia(int candidateId, int mediaId);

	Media updateMedia(Media media);

	List<JobHistory> getAllJobHistory(int candidateId);

	JobHistory getJobHistory(int candidateId, int jobId);

	JobApplied getAppliedJob(int candidateId, int jobId);

	List<EducationSummary> getAllEducation(int candidateId);

	EducationSummary getEducation(int candidateId, int eId);

	List<AudioInterview> getAllAudioInterview(int candidateId);

	AudioInterview getAudioInterview(int candidateId, int audioId);

	CaseAnswer getWorkcase(int candidateId, int caseId);

	List<CaseAnswer> getAllWorkcases(int candidateId, String qtype);

	AudioInterview getAudioInterviewBySid(String callsid);

	void createScoring(CandidateScoring candidateScore);

	CandidateProfile getCandidateByPin(String phoneNumber);

	List<Psychometric> getPsychometric(int candidateId);

	List<JobApplied> getAllAppliedJobs(int candidateId, int rowCount);

	List<CaseAnswer> getWorkcaseByJob(int candidateId, int jobId, String qtype);

	JobApplied updateAppliedJob(JobApplied jobApplied);

	void updateCandidateEmailStatus(String emails, String emailStatus, int jobId);
	
	int updateCandidateJobAndEmailStatusForAutoExpiry(String oldJobStatus,String newJobStatus, String newEmailStatus,int timeInterval);

	CandidateScoring getCandidateScoring(int candidateId, int jobId,
			String stage);

	List<CandidateScoring> getCandidateScoringForJob(int jobId,
			String stage);
	CandidateProfile getCandidateByIdAndJobId(int jobId, int candidateId);
	
	public void saveOAuthClientDetails(String authToken);
	
	public String getAuthToken(int candidateId);
	

}
