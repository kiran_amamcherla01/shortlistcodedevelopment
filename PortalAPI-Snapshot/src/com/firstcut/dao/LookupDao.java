package com.firstcut.dao;

import java.util.List;

public interface LookupDao {

	/**
	 * This method is used to get all zone from city lookup
	 * 
	 * @return
	 */
	List<String> getAllZones();

	/**
	 * This method is used to get states by zone
	 * 
	 * @param zone
	 * @return list of state
	 */
	List<String> getStatesByZone(String zone);

	/**
	 * This method is used to get city by state
	 * 
	 * @param state
	 * @return list of city
	 */
	List<String> getCitiesByState(String state);

	List<String> getAllIndustries();

	List<String> getAllFunctions();

	List<String> getAllTopUniversities();

	List<String> getCitiesByZone(String zone);

	List<String> getAllEducationByType(String type);

	List<String> getAllCities();

	List<String> checkUniversityAndTier(String[] requiredTiers,
			List<String> attendedCollages);

	List<String> getUniversityByTier(String[] requiredTiers);
}
