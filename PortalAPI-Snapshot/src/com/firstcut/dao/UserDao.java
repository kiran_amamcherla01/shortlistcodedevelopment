package com.firstcut.dao;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.dto.AuditLog;
import com.firstcut.dto.ContactUs;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;

public interface UserDao {

	User getUserByAuthToken(String authToken) throws DataBaseException;

	User getUserByUserNameAndType(String email, UserType userType)
			throws DataBaseException;

	User saveUser(User user) throws DataBaseException;

	User getUserById(int userId) throws DataBaseException;

	SocialProfile saveSocialProfile(SocialProfile socialProfile)
			throws DataBaseException;

	User getUserByConfirmToken(String confirmToken) throws DataBaseException;

	void createConatctUsDetails(ContactUs contactUs);

	void createAuditLog(AuditLog auditLog);

}
