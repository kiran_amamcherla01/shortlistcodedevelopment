package com.firstcut.dao;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.firstcut.Exception.DataBaseException;
import com.firstcut.constant.Constant;
import com.firstcut.dto.CityLookup;
import com.firstcut.dto.EducationLookup;
import com.firstcut.dto.FunctionLookup;
import com.firstcut.dto.IndustryLookup;
import com.firstcut.dto.TopUniversityLookup;

public class LookupDaoImpl extends BaseDao implements LookupDao {
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllZones() {
		String METHOD_NAME = "getAllZones";
		List<String> zoneList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(CityLookup.class)
					.setProjection(
							Projections.distinct(Projections.property("zone")));
			zoneList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return zoneList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getStatesByZone(String zone) {
		String METHOD_NAME = "getStatesByZone";
		List<String> stateList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(CityLookup.class);
			if (zone != null && !zone.isEmpty())
				createCriteria.add(Restrictions.eq("zone", zone));
			createCriteria.setProjection(Projections.distinct(Projections
					.property("state")));
			stateList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return stateList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCitiesByState(String state) {
		String METHOD_NAME = "getCitiesByState";
		List<String> cityList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(CityLookup.class);
			if (state != null && !state.isEmpty())
				createCriteria.add(Restrictions.eq("state", state));
			createCriteria.setProjection(Projections.distinct(Projections
					.property("city")));
			cityList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return cityList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getCitiesByZone(String zone) {
		String METHOD_NAME = "getCitiesByZone";
		List<String> cityList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(CityLookup.class);
			if (zone != null && !zone.isEmpty())
				createCriteria.add(Restrictions.eq("zone", zone));
			createCriteria.setProjection(Projections.distinct(Projections
					.property("city")));
			cityList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return cityList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllCities() {
		String METHOD_NAME = "getAllCities";
		List<String> cityList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(CityLookup.class);
			createCriteria.setProjection(Projections.distinct(Projections
					.property("city")));
			cityList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return cityList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllFunctions() {
		String METHOD_NAME = "getAllFunctions";
		List<String> functionList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(
					FunctionLookup.class).setProjection(
					Projections.distinct(Projections.property("master")));
			functionList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return functionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllIndustries() {
		String METHOD_NAME = "getAllIndustries";
		List<String> industryList = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(
					IndustryLookup.class)
					.setProjection(
							Projections.distinct(Projections
									.property("master")));
			industryList = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return industryList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllTopUniversities() {
		String METHOD_NAME = "getAllTopUniversities";
		List<String> topUniversities = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(
					TopUniversityLookup.class)
					.setProjection(
							Projections.distinct(Projections
									.property("universityName")));
			topUniversities = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return topUniversities;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> checkUniversityAndTier(String[] requiredTiers,
			List<String> attendedCollages) {
		String METHOD_NAME = "checkUniversityTier";
		List<String> topUniversities = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session
					.createCriteria(TopUniversityLookup.class);
			createCriteria.add(Restrictions.in("tier", requiredTiers));
			createCriteria.add(Restrictions.disjunction()
					.add(Restrictions.in("universityName", attendedCollages))
					.add(Restrictions.in("acronym", attendedCollages))
					.add(Restrictions.in("original", attendedCollages)));
			createCriteria.setProjection(Projections.distinct(Projections
					.property("universityName")));
			topUniversities = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return topUniversities;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getUniversityByTier(String[] requiredTiers) {
		String METHOD_NAME = "checkUniversityTier";
		List<String> topUniversities = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session
					.createCriteria(TopUniversityLookup.class);
			createCriteria.add(Restrictions.in("tier", requiredTiers));
			createCriteria.setProjection(Projections.distinct(Projections
					.property("universityName")));
			topUniversities = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return topUniversities;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getAllEducationByType(String type) {
		String METHOD_NAME = "getAllEducationByType";
		List<String> list = Collections.emptyList();
		try {
			session = getCurrentSession();
			Criteria createCriteria = session.createCriteria(
					EducationLookup.class).setProjection(
					Projections.distinct(Projections.property("master")));
			if (type != null && !type.isEmpty())
				createCriteria.add(Restrictions.eq("group", type));
			list = createCriteria.list();
		} catch (HibernateException exception) {
			logger.error(METHOD_NAME, exception);
			throw new DataBaseException(Constant.DATBASE_EXCEPTION_MESSAGE);
		}
		return list;
	}

}
