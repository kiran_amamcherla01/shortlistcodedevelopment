package com.firstcut.s3.util;

import java.io.File;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.firstcut.constant.Constant;
import com.firstcut.util.ResourceManager;

public class AmazonClientUtil {
	Logger logger = Logger.getLogger(AmazonClientUtil.class);
	private static String BUCKET_NAME = ResourceManager
			.getProperty("amazon.bucket.name");
	private static AmazonClientUtil amazonClient;

	private AmazonClientUtil() {
	}

	public static AmazonClientUtil getInstance() {
		if (amazonClient == null) {
			amazonClient = new AmazonClientUtil();
		}
		return amazonClient;
	}

	public String uploadMultipart(String filePath, String keyName) {
		try {
			if (filePath == null || filePath.isEmpty()) {
				throw new Exception("File path is null");
			}
			AmazonS3 s3client = AmazonS3Util.connect();
			File file = new File(filePath);
			TransferManager tx = new TransferManager(s3client);
			PutObjectRequest request = new PutObjectRequest(BUCKET_NAME,
					keyName, file);
			request.withCannedAcl(CannedAccessControlList.PublicRead);
			Upload upload = tx.upload(request);
			while (upload.isDone() == false) {
				logger.info("Transfer: " + upload.getDescription());
				logger.info("  - Status: " + upload.getState());
				logger.info("  - Progress: "
						+ upload.getProgress().getBytesTransfered());
				// Do work while we wait for our upload to complete...
				try {
					Thread.sleep(500);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			logger.info("Upload complete.");
		} catch (AmazonClientException amazonClientException) {
			logger.error("Unable to upload file, upload was aborted.",
					amazonClientException);
		} catch (Exception ex) {
			logger.error("Unable to upload file, upload was aborted.", ex);
		}
		return Constant.S3_PATH + BUCKET_NAME
				+ File.separator + keyName;
	}

	public static void main(String a[]) {
		AmazonClientUtil.getInstance().uploadMultipart(
				"C:\\Users\\mindbowser-sunny\\Desktop\\shortlist_logo.png",
				"profilepic/shortlist_logo.png");
	}

}
