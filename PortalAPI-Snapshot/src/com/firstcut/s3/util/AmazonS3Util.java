package com.firstcut.s3.util;


import java.io.IOException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.firstcut.util.ResourceManager;

public class AmazonS3Util
{
	private static String awsAccessKey = ResourceManager.getProperty("amazon.s3.access.key.id");
	private static String awsSecretKey = ResourceManager.getProperty("amazon.s3.secret.access.key");
//	private static String awsAccessKey = "AKIAJ2YMRYD752BLF5RQ";
//	private static String awsSecretKey = "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";

	public static void createBucket(String bucketName)
	{
		AmazonS3Client s3Client = connect();
		s3Client.createBucket(bucketName);
	}

	public static AmazonS3Client connect()
	{
		AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
		AmazonS3Client s3client = new AmazonS3Client(awsCredentials);
		return s3client;
	}

	public static void deleteBucket(String bucketName) throws IOException
	{
		AmazonS3Client s3Client = connect();
		s3Client.deleteBucket(bucketName);
	}

}
