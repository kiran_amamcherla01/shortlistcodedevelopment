package com.firstcut.mapper;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;

import com.firstcut.Exception.ServiceException;
import com.firstcut.enums.ApiResponseStatusType;
import com.firstcut.model.ErrorModel;
import com.firstcut.model.ResponseModel;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class CustomExceptionMapper implements ExceptionMapper<Exception> {
	Logger logger = Logger.getLogger(CustomExceptionMapper.class);

	@Context
	private HttpHeaders headers;

	public Response toResponse(Exception e) {
		if (e instanceof ServiceException) {
			logger.error(e.getMessage());
		} else
			logger.error(e.getMessage(), e);
		ResponseModel response = ResponseModel
				.build(ApiResponseStatusType.ERROR);
		ErrorModel error = new ErrorModel();
		error.setMessage(e.getMessage());
		error.setCode(Status.INTERNAL_SERVER_ERROR.getStatusCode());
		error.setErrorType(e.getClass().getSimpleName());
		response.setError(error);
		response.setStatus(ApiResponseStatusType.ERROR);
		return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode())
				.entity(response).type(headers.getMediaType()).build();
	}
}