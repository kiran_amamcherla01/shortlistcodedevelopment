package com.firstcut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.dao.LookupDao;

public class FunctionLookupServiceImpl implements FunctionLookupService {

	@Autowired
	private LookupDao LookupDao;

	@Override
	public List<String> getAllFunctions() {
		return LookupDao.getAllFunctions();
	}

}
