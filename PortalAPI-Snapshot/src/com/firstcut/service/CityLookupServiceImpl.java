package com.firstcut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;

import com.firstcut.dao.LookupDao;
import com.firstcut.dao.UserDao;
import com.firstcut.dto.User;
import com.firstcut.util.BCryptPasswordEncoder;
import com.firstcut.util.TokenGenerator;

public class CityLookupServiceImpl implements CityLookupService {

	@Autowired
	private LookupDao lookupDao;

	@Override
	public List<String> getAllZones() {
		return lookupDao.getAllZones();
	}

	@Override
	public List<String> getStatesByZone(String zone) {
		return lookupDao.getStatesByZone(zone);
	}

	@Override
	public List<String> getCitiesByState(String state) {
		return lookupDao.getCitiesByState(state);
	}

	@Override
	public List<String> getCitiesByZone(String zone) {
		return lookupDao.getCitiesByZone(zone);
	}
}
