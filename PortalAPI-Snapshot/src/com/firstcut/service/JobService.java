package com.firstcut.service;

import java.util.List;
import java.util.Set;

import com.firstcut.Exception.InActiveUserException;
import com.firstcut.Exception.PasswordMismatchException;
import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;
import com.firstcut.model.SearchFilterModel;
import com.notify.exception.EmailSendFailureException;


public interface JobService {

	List<CaseQuestion> getCaseQuestions(int jobOpeningId, String qtype);

	void createCaseQuestions(int jobOpeningId,
			List<CaseQuestion> caseQuestions);

	void deleteCaseQuestion(CaseQuestion caseQuestion);

	CaseQuestion updateCaseQuestion(int jobOpeningId, CaseQuestion caseQuestions);

	List<JobScoringCriteria> getDefaultScoringCriteria();

	List<JobScoringRelation> getJobScoringCriteria(int jobId);

	Set<JobScoringRelation> createJobScoringCriteria(int jobId,
			Set<JobScoringRelation> scoringCriteria);

	Set<JobScoringRelation> updateJobScoringCriteria(int jobId,
			Set<JobScoringRelation> scoringCriteria);

	void calculateFinalScore(int jobId);

	JobOpening getAllAppliedCandidatesByJobId(int jobId,
			Set<String> jobStatuses);

	Set<JobOpening> getAllAppliedCandidatesForSearchCriteria(SearchFilterModel model);

}
