package com.firstcut.service;

import java.util.List;


public interface FunctionLookupService
{
	/**
	 * Get all functions from dao layer and pass it to controller
	 * @return list of functions
	 */
	public List<String> getAllFunctions();
}
