package com.firstcut.service;

import java.util.List;


public interface EducationLookupService {

	List<String> getAllEducationByType(String type);
}
