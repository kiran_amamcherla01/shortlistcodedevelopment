package com.firstcut.service;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.User;
import com.firstcut.dto.VeriduUser;

public interface VeridulService {

	User saveVeriduUser(VeriduUser veriduUser);
}
