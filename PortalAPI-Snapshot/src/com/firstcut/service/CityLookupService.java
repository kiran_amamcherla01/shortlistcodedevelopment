package com.firstcut.service;

import java.util.List;


public interface CityLookupService
{
	/**
	 * Get all zone list
	 * @return list of zone
	 */
	public List<String> getAllZones();
	
	/**
	 * Get state list by zone 
	 * @param zone
	 * @return list of state
	 */
	public List<String> getStatesByZone(String zone);
	
	/**
	 * Get city list by state
	 * @param state
	 * @return list of city
	 */
	public List<String> getCitiesByState(String state);

	/**
	 * Get city list by zone
	 * @param state
	 * @return list of city
	 */
	public List<String> getCitiesByZone(String zone);
}
