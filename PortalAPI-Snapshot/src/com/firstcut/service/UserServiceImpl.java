package com.firstcut.service;

import static com.firstcut.constant.Constant.INVALID_USERNAME_EXCEPTION;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.UserAlreadyExistException;
import com.firstcut.constant.Constant;
import com.firstcut.dao.UserDao;
import com.firstcut.dto.ContactUs;
import com.firstcut.dto.User;
import com.firstcut.enums.UserAccountStatus;
import com.firstcut.enums.UserType;
import com.firstcut.util.ResourceManager;

@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public User getUserByAuthToken(String authToken) {
		User userDto = userDao.getUserByAuthToken(authToken);
		return userDto;
	}
	
	@Override
	public User getUserByUserNameAndType(String userName,UserType userType) throws RecordNotExistException {
		User userDto = userDao.getUserByUserNameAndType(userName, userType);
		if (userDto == null) {
			throw new RecordNotExistException(
					ResourceManager.getProperty(INVALID_USERNAME_EXCEPTION));
		}
		return userDto;
	}

	@Override
	public User getUserByConfirmToken(String confirmToken) {
		User user = userDao.getUserByConfirmToken(confirmToken);
		user.setAccountStatus(UserAccountStatus.ACTIVE);
		user.setConfirmToken(null);
		return user;
	}

	@Override
	public User getUserById(int userId) {
		User user = userDao.getUserById(userId);
		if (user != null) {
			throw new UsernameNotFoundException(
					ResourceManager
							.getProperty(Constant.USER_ALREADY_EXIST_EXCEPTION));
		}
		return user;
	}

	@Override
	public void createConatctUsDetails(ContactUs contactUs) {
		userDao.createConatctUsDetails(contactUs);
	}
}
