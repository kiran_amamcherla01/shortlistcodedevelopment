package com.firstcut.service;

import java.util.List;

import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.CompanyProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Media;
import com.firstcut.model.SearchFilterModel;

public interface CompanyService {

	List<CompanyProfile> getAllCompany(String filter);

	CompanyProfile getCompany(int companyId) throws ServiceException;

	CompanyProfile createCompany(CompanyProfile companyProfile) throws ServiceException;

	CompanyProfile updateCompany(int companyId, CompanyProfile companyProfile) throws ServiceException;

	List<Media> getAllMedia(int companyId);

	Media getMedia(int companyId, int mediaId) throws ServiceException;

	List<Media> createMedia(int companyId, List<Media> media) throws ServiceException;

	List<HiringManager> getAllManager(int companyId);

	HiringManager getManager(int companyId, int managerId) throws ServiceException;

	HiringManager createManager(int companyId, HiringManager manager) throws ServiceException;

	List<JobOpening> getAllJobOpenings(int companyId,String jobOpeningStatus) throws ServiceException;

	JobOpening getJobOpening(int companyId, int jobOpeningId) throws ServiceException;

	JobOpening createJobOpening(int companyId, JobOpening jobOpening) throws ServiceException;

	JobOpening updateJobOpening(int companyId, JobOpening jobOpening) throws ServiceException;

	CompanyProfile getCompanyByName(String companyName) throws ServiceException;

	JobOpening getJobOpening(String companyname, int jobOpeningId) throws ServiceException;

	List<JobOpening> getAllJobOpenings(String jobOpeningStatus);

	void deleteMedia(int companyId, int mediaId) throws ServiceException;

	List<JobOpening> getAllAppliedCandidatesByCompanyId(int companyId,
			String jobOpeningStatus, String jobStatus);

	List<CompanyProfile> getJobsByCompanies(SearchFilterModel model)
			throws ServiceException;
}
