package com.firstcut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.dao.LookupDao;

public class EducationLookupServiceImpl implements EducationLookupService {

	@Autowired
	private LookupDao lookupDao;

	public List<String> getAllEducationByType(String type) {
		return lookupDao.getAllEducationByType(type);
	}

}
