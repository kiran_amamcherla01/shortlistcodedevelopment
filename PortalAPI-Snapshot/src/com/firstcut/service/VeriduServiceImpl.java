/**
 * 
 */
package com.firstcut.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.UserDao;
import com.firstcut.dao.VeriduDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.User;
import com.firstcut.dto.VeriduUser;
import com.firstcut.enums.UserAccountStatus;
import com.firstcut.enums.UserType;

/**
 * @author mindbowser-sunny
 * 
 */
public class VeriduServiceImpl implements VeridulService {

	@Autowired
	private VeriduDao veriduDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private CandidateDao candidateDao;

	@Override
	public User saveVeriduUser(VeriduUser veriduUser) {
		User user = null;
		VeriduUser veriduDBUser = veriduDao.getUserById(veriduUser
				.getVeriduId());
		if (veriduDBUser == null) {
			user = userDao.getUserByUserNameAndType(veriduUser.getVeriduEmail(),UserType.CANDIDATE);
			if (user == null) {
				user = new User(veriduUser.getVeriduEmail(), UserType.CANDIDATE,UserAccountStatus.ACTIVE);
				user.setVeriduUser(veriduUser);

				CandidateProfile candidate = new CandidateProfile(
						veriduUser.getVeriduEmail());
				candidate.setFullName(veriduUser.getVeriduFirstName()+veriduUser.getVeriduLastName());
				candidate.setUser(user);
				user.setCandidateProfile(candidate);
				candidateDao.saveCandidate(candidate);
			}
			veriduUser.setUser(user);
			veriduDao.saveVeriduUser(veriduUser);
			user.generateAuthToken();
		}
		return user;
	}
}
