package com.firstcut.service;

import java.util.List;


public interface LookupService
{
	/**
	 * Get all top universities
	 * @return list of universities
	 */
	public List<String> getAllTopUniversities();
	
}
