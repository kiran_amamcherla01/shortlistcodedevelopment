package com.firstcut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.dao.LookupDao;

public class LookupServiceImpl implements LookupService {

	@Autowired
	private LookupDao lookupDao;
	@Override
	public List<String> getAllTopUniversities() {
		return lookupDao.getAllTopUniversities();
	}

}
