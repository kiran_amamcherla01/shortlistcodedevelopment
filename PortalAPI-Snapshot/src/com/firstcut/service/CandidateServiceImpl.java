package com.firstcut.service;

import static com.firstcut.constant.Constant.COMPLETE_STATUS;
import static com.firstcut.constant.Constant.PROFILEPIC_FOLDER;
import static com.firstcut.constant.Constant.RESUME_FOLDER;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.Exception.UserAlreadyExistException;
import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dao.JobDao;
import com.firstcut.dao.UserDao;
import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseAnswer;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobHistory;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.OAuthClientDetails;
import com.firstcut.dto.Psychometric;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.CandidateSource;
import com.firstcut.enums.EmailBlockStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.UserAccountStatus;
import com.firstcut.index.IndexCreatorException;
import com.firstcut.model.AssessmentStatusResponse;
import com.firstcut.s3.util.AmazonClientUtil;
import com.firstcut.scoring.CVScreening;
import com.firstcut.scoring.PersonalityScore;
import com.firstcut.util.FileOperations;
import com.firstcut.util.OAuthTokenUtil;
import com.firstcut.util.ResourceManager;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.notification.ISecretEmailNotification;

public class CandidateServiceImpl implements CandidateService {

	@Autowired
	private CandidateDao candidateDao;
	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private JobDao jobDao;
	@Autowired
	private CVScreening cvScreening;
	@Autowired
	private ISecretEmailNotification welcomeEmailNotification;
	@Autowired
	private ISecretEmailNotification submissionEmailNotification;

	private static Logger logger = Logger.getLogger(CandidateServiceImpl.class);

	@Override
	public CandidateProfile createCandidate(CandidateProfile candidateProfile)
			throws ServiceException, EmailSendFailureException,
			UserAlreadyExistException {
		
		if (candidateProfile.getUser() == null) {
			throw new ServiceException("UserName and Password can't be blank !");
		}
		String userName = candidateProfile.getUser().getUserName();
		String password = candidateProfile.getUser().getPassword();
		logger.info("candidate is signup with userName(" + userName
				+ ") & password=(" + password + ")");
		if (userName == null || userName.isEmpty()) {
			throw new ServiceException("UserName can't be blank !");
		}
		if (password == null || password.isEmpty()) {
			throw new ServiceException("Password can't be blank !");
		}
		CandidateProfile candidateByEmail = candidateDao
				.getCandidateByEmail(userName);
		
		
		if (candidateByEmail != null
				&& UserAccountStatus.ACTIVE.equals(candidateByEmail.getUser()
						.getAccountStatus())) {
			logger.info(ResourceManager
					.getProperty(Constant.USER_ALREADY_EXIST_EXCEPTION)
					+ " = (" + userName + ")");
			throw new UserAlreadyExistException(
					ResourceManager
							.getProperty(Constant.USER_ALREADY_EXIST_EXCEPTION));
		}
		if (candidateByEmail != null
				&& UserAccountStatus.INACTIVE.equals(candidateByEmail.getUser()
						.getAccountStatus())) {
			candidateByEmail.setUserId(candidateByEmail.getUserId());
			candidateByEmail.getUser().encodePassword(password);
			candidateByEmail.getUser().generateAuthToken();
			candidateByEmail.getUser().setAccountStatus(
					UserAccountStatus.ACTIVE);
			candidateProfile = candidateByEmail;
		} else {
			candidateProfile.setEmail(userName);
			candidateProfile.getUser().encodePassword(password);
			candidateProfile.getUser().generateAuthToken();
			candidateProfile.getUser().setAccountStatus(
					UserAccountStatus.ACTIVE);
			candidateProfile.getUser().setCandidateProfile(candidateProfile);
			candidateProfile.setSource(CandidateSource.DIRECT);
			candidateProfile = candidateDao.saveCandidate(candidateProfile);
		}
		ContentModel contentModel = Utils.getContentModel(candidateProfile,
				null);
		try {
			welcomeEmailNotification.sendNotification(
					candidateProfile.getEmail(), contentModel);
		} catch (EmailSendFailureException e) {
			logger.error("Unable to send submit application email", e);
		}
		// update All applied job status
		List<JobApplied> allAppliedJobs = getAllAppliedJobs(
				candidateProfile.getUserId(), 0);
		for (JobApplied jobApplied : allAppliedJobs) {
			updateAppliedJobStatus(candidateProfile.getUserId(), jobApplied
					.getJobOpening().getId(), AppliedJobStatus.ACCOUNT_CREATED,
					"false");
			candidateDao.updateCandidateEmailStatus(
					candidateProfile.getEmail(), EmailStatus.WELCOME_EMAIL_SENT
							.toValue(), jobApplied.getJobOpening().getId());
		}
		candidateDao.saveOAuthClientDetails(candidateProfile.getUser().getAuthToken());
		logger.info("OAuth client details saved");
		return candidateProfile;
	}

	@Override
	public CandidateProfile getCandidate(int candidateId)
			throws ServiceException {
		CandidateProfile candidate = candidateDao.getCandidateById(candidateId);
		if (candidate == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		return candidate;
	}

	@Override
	public CandidateProfile getCandidateForJob(int jobId, int candidateId)
			throws ServiceException {
		CandidateProfile candidate = candidateDao.getCandidateByIdAndJobId(
				jobId, candidateId);
		if (candidate == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		return candidate;
	}

	@Override
	public List<CandidateProfile> getAllCandidate() {
		List<CandidateProfile> candidate = candidateDao.getAllCandidate();
		return candidate;
	}

	@Override
	public CandidateProfile updateCandidate(int candidateId,
			CandidateProfile candidateProfile) throws ServiceException {
		CandidateProfile candidateDb = getCandidate(candidateId);
		logger.info("KK updateCandidate CandidateServiceImpl --->"+candidateDb+" Candidate Id "+candidateId);
		if (!StringUtils.isEmpty(candidateProfile.getProfilePic())) {
			String key = PROFILEPIC_FOLDER + File.separator
					+ candidateDb.getEmail() + ".jpg";
			String filePath = FileOperations.writeFile(
					candidateProfile.getProfilePic(), candidateDb.getEmail(),
					".jpg");
			String picURL = AmazonClientUtil.getInstance().uploadMultipart(
					filePath, key);
			candidateProfile.setProfilePicURL(picURL);
		}
		if (!StringUtils.isEmpty(candidateProfile.getResume())) {
			String key = RESUME_FOLDER + File.separator
					+ candidateDb.getEmail()
					+ candidateProfile.getResumeFileExt();
			String filePath = FileOperations.writeFile(
					candidateProfile.getResume(), candidateDb.getEmail(),
					candidateProfile.getResumeFileExt());
			String resumeURL = AmazonClientUtil.getInstance().uploadMultipart(
					filePath, key);
			candidateProfile.setResumeUrl(resumeURL);
		}
		candidateProfile.setUserId(candidateDb.getUserId());
		candidateProfile.getAudioInterviews().addAll(
				candidateDb.getAudioInterviews());
		logger.info("KK updateCandidate CandidateServiceImpl getCaseAnswers--->");
		candidateProfile.getCaseAnswers().addAll(candidateDb.getCaseAnswers());
		candidateProfile.getJobApplieds().addAll(candidateDb.getJobApplieds());
		candidateProfile.getPsychometrics().addAll(
				candidateDb.getPsychometrics());
		candidateProfile.setProfileStatus(COMPLETE_STATUS);
		candidateProfile.setSource(candidateDb.getSource());
		logger.info("KK updateCandidate CandidateServiceImpl after getCaseAnswers--->");
		CandidateProfile candidate = candidateDao
				.updateCandidate(candidateProfile);
		return candidate;
	}

	@Override
	public List<JobHistory> getAllJobHistory(int candidateId) {
		List<JobHistory> jobHistories = candidateDao
				.getAllJobHistory(candidateId);
		return jobHistories;
	}

	@Override
	public JobHistory getJobHistory(int candidateId, int jobHistoryId)
			throws ServiceException {
		JobHistory jobHistory = candidateDao.getJobHistory(candidateId,
				jobHistoryId);
		if (jobHistory == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.MEDIA_NOT_EXIST_EXCEPTION));
		}
		return jobHistory;
	}

	@Override
	public Set<JobHistory> createJobHistory(int candidateId,
			Set<JobHistory> jobHistory) {
		CandidateProfile candidateProfile = candidateDao
				.getCandidateById(candidateId);
		candidateProfile.getJobHistories().addAll(jobHistory);
		return jobHistory;
	}

	@Override
	public List<JobApplied> getAllAppliedJobs(int candidateId, int rowCount) {
		List<JobApplied> jobApplieds = candidateDao.getAllAppliedJobs(
				candidateId, rowCount);
		String authToken = candidateDao.getAuthToken(candidateId);
		logger.info("authToken getAllAppliedJobs---"+authToken);
		//logger.info("Response from Microservice "+invokeMicroService(authToken));
		return jobApplieds;
	}
	
	private String invokeMicroService(String authToken) {
		//String oAuthToken = candidateDao.getOAuthToken(authToken);
		String oAuthToken = OAuthTokenUtil.getOAuthToken(authToken);
		logger.info("oauth token "+oAuthToken);
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://localhost:8080/oauthsecurity-1.0/rest/message");
		String serviceresponse = "";
		try {

			httppost.addHeader("Authorization", "Bearer "+oAuthToken);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			serviceresponse = httpclient.execute(httppost, responseHandler);
			System.out.println("serviceresponse : " + serviceresponse);

		} catch (Exception e) {
			System.out.println("�xception message "+e.getMessage());
			e.printStackTrace();
		}  finally {
		}
		return serviceresponse;
	}
	
	
	

	@Override
	public JobApplied getAppliedJob(int candidateId, int jobId)
			throws ServiceException {
		JobApplied jobApplied = candidateDao.getAppliedJob(candidateId, jobId);
		if (jobApplied == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_APPLIED_FOR_JOB));
		}
		return jobApplied;
	}

	@Override
	public Boolean updateEmailSendStatusForAppliedJob(int jobId, String email,
			String status) throws ServiceException {
		boolean result = true;
		CandidateProfile candidateByEmail = candidateDao
				.getCandidateByEmail(email);
		if (candidateByEmail == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		JobApplied jobApplied = candidateDao.getAppliedJob(
				candidateByEmail.getUserId(), jobId);
		if (jobApplied == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_APPLIED_FOR_JOB));
		}
		jobApplied.setEmailBlockStatus(EmailBlockStatus.fromValue(status));
		return result;
	}

	@Override
	public Boolean updateEmailBlockStatusForRejectedMails(int jobId,
			List<String> email, String status) throws ServiceException {
		boolean result = true;
		List<CandidateProfile> candidatesByEmail = candidateDao
				.getCandidatesByEmail(email);
		if (candidatesByEmail == null || candidatesByEmail.isEmpty()) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		for (CandidateProfile candidate : candidatesByEmail) {
			JobApplied jobApplied = candidateDao.getAppliedJob(
					candidate.getUserId(), jobId);
			if (jobApplied != null) {
				jobApplied.setEmailBlockStatus(EmailBlockStatus
						.fromValue(status));
			}
		}
		return result;
	}

	@Override
	public JobApplied createAppliedJob(int candidateId, JobApplied jobApplied)
			throws ServiceException {
		logger.info("candidate=" + candidateId + " is applying for Job="
				+ jobApplied.getJobOpening().getId());
		JobOpening jobOpening = companyDao.getJobOpeningById(jobApplied
				.getJobOpening().getId());
		if (jobOpening.getSubmissionDeadline().after(new Date())) {
			jobApplied.setJobOpening(jobOpening);
			JobApplied jobAppliedDb = candidateDao.getAppliedJob(candidateId,
					jobOpening.getId());
			if (jobAppliedDb == null) {
				CandidateProfile candidateProfile = getCandidate(candidateId);
				jobApplied.setEmailBlockStatus(EmailBlockStatus.UNBLOCKED);
				jobApplied.setCandidateProfile(candidateProfile);
				jobApplied.setJobStatus(AppliedJobStatus.ACCOUNT_CREATED);
				jobApplied.setEmailStatus(EmailStatus.WELCOME_EMAIL_SENT);
				candidateProfile.getJobApplieds().add(jobApplied);
			} else {
				jobApplied = jobAppliedDb;
			}
		}else{
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.JOB_ALREADY_EXPIRED));
		}

		return jobApplied;
	}

	@Override
	public List<EducationSummary> getAllEducation(int candidateId) {
		List<EducationSummary> educationSummaries = candidateDao
				.getAllEducation(candidateId);
		return educationSummaries;
	}

	@Override
	public EducationSummary getEducation(int candidateId, int eId)
			throws ServiceException {
		EducationSummary education = candidateDao
				.getEducation(candidateId, eId);
		if (education == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.EDUCATION_NOT_EXIST_EXCEPTION));
		}
		return education;
	}

	@Override
	public Set<EducationSummary> createEducation(int candidateId,
			Set<EducationSummary> educationSummary) throws ServiceException {
		CandidateProfile candidateProfile = getCandidate(candidateId);
		candidateProfile.getEducationSummaries().addAll(educationSummary);
		return educationSummary;
	}

	@Override
	public List<AudioInterview> getAllAudioInterview(int candidateId) {
		List<AudioInterview> audioInterviews = candidateDao
				.getAllAudioInterview(candidateId);
		return audioInterviews;
	}

	@Override
	public AudioInterview getAudioInterview(int candidateId, int jobId) {
		return candidateDao.getAudioInterview(candidateId, jobId);
	}

	@Override
	public List<CaseAnswer> getWorkcaseByJob(int candidateId, int jobId,
			String qtype) {
		return candidateDao.getWorkcaseByJob(candidateId, jobId, qtype);
	}

	@Override
	public AudioInterview createAudioInterview(AudioInterview audioInterview) {
		logger.info(audioInterview);
		String audioPin = audioInterview.getDigits() != null ? audioInterview
				.getDigits().replaceAll("\"", "") : audioInterview.getDigits();
		if (audioInterview.getDigits() != null) {
			logger.info("candidate enter pin = " + audioPin);
			int candidateId = Integer.parseInt(audioPin.substring(0, 5));
			int jobId = Integer.parseInt(audioPin.substring(5,
					audioPin.length()));
			logger.info("candidate id = " + candidateId);
			logger.info("jobId = " + jobId);
			CandidateProfile candidate = candidateDao
					.getCandidateById(candidateId);
			logger.info("canidadate oject from database = " + candidate);
			audioInterview.setCandidateProfile(candidate);
			audioInterview.setJobId(jobId);
			AudioInterview oldAudioInterview = getAudioInterview(candidateId,
					jobId);
			if (oldAudioInterview != null) {
				logger.info("Removing old auto recording for pin = " + audioPin);
				candidate.getAudioInterviews().remove(oldAudioInterview);
			}
			candidate.getAudioInterviews().add(audioInterview);
		}
		if (audioInterview.getRecordingUrl() != null) {
			AudioInterview audioInterviewBySid = candidateDao
					.getAudioInterviewBySid(audioInterview.getCallsid());
			if (audioInterviewBySid != null)
				audioInterviewBySid.setRecordingUrl(audioInterview
						.getRecordingUrl());
		}
		return audioInterview;
	}

	@Override
	public List<CaseAnswer> createWorkcase(int candidateId, int jobId,
			List<CaseAnswer> caseAnswers) throws RecordNotExistException {
		CandidateProfile candidate = candidateDao.getCandidateById(candidateId);
		List<CaseAnswer> allWorkcase = getWorkcaseByJob(candidateId, jobId,
				Constant.LONG);
		if (candidate == null) {
			throw new RecordNotExistException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		candidate.getCaseAnswers().removeAll(allWorkcase);
		candidate.getCaseAnswers().addAll(caseAnswers);
		return caseAnswers;
	}

	@Override
	public List<CaseAnswer> getAllWorkcase(int candidateId, String qtype) {
		List<CaseAnswer> caseAnswers = candidateDao.getAllWorkcases(
				candidateId, qtype);
		return caseAnswers;
	}

	@Override
	public CaseAnswer getWorkcase(int candidateId, int caseId)
			throws RecordNotExistException {
		CaseAnswer caseAnswer = candidateDao.getWorkcase(candidateId, caseId);
		if (caseAnswer == null) {
			throw new RecordNotExistException(
					ResourceManager
							.getProperty(Constant.WORKCASE_NOT_EXIST_EXCEPTION));
		}
		return caseAnswer;
	}

	@Override
	public void createScoring(CandidateScoring candidateScore) {
		candidateDao.createScoring(candidateScore);
	}

	@Override
	public List<Psychometric> getPsychometric(int candidateId) {
		List<Psychometric> psychometrics = candidateDao
				.getPsychometric(candidateId);
		return psychometrics;
	}

	@Override
	public JobApplied updateAppliedJobStatus(int candidateId, int jobId,
			AppliedJobStatus status, String sendMail)
			throws RecordNotExistException {
		JobApplied jobApplied = candidateDao.getAppliedJob(candidateId, jobId);
		if (jobApplied == null) {
			throw new RecordNotExistException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_APPLIED_FOR_JOB));
		}
		jobApplied.setJobStatus(status);
		if (AppliedJobStatus.APPLICATION_SUBMITTED.equals(status)
				&& "true".equalsIgnoreCase(sendMail)) {
			ContentModel convertObjectToContent = Utils.getContentModel(
					jobApplied.getCandidateProfile(),
					jobApplied.getJobOpening());
			try {
				submissionEmailNotification.sendNotification(jobApplied
						.getCandidateProfile().getEmail(),
						convertObjectToContent);
				jobApplied
						.setEmailStatus(EmailStatus.APPLICATION_SUBMIT_EMAIL_SENT);
			} catch (EmailSendFailureException e) {
				logger.error("Unable to send submit application email", e);
			}
		}
		return jobApplied;
	}

	@Override
	public JobApplied updateAppliedJob(int candidateId, int jobId,
			JobApplied jobApplied) throws RecordNotExistException {
		JobApplied jobAppliedDB = candidateDao
				.getAppliedJob(candidateId, jobId);
		if (jobAppliedDB == null) {
			throw new RecordNotExistException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_APPLIED_FOR_JOB));
		}
		jobApplied.setCandidateProfile(jobAppliedDB.getCandidateProfile());
		jobApplied.setJobOpening(jobAppliedDB.getJobOpening());
		jobApplied.setEmailStatus(jobAppliedDB.getEmailStatus());
		jobApplied.setEmailBlockStatus(jobAppliedDB.getEmailBlockStatus());
		jobApplied.setId(jobAppliedDB.getId());
		jobApplied.setAppliedType(jobAppliedDB.getAppliedType());
		if (jobApplied.getJobStatus() == null
				|| jobApplied.getJobStatus().toString().isEmpty())
			jobApplied.setJobStatus(jobAppliedDB.getJobStatus());
		candidateDao.updateAppliedJob(jobApplied);
		return jobApplied;
	}

	@Override
	public AssessmentStatusResponse getAssessmentStatus(int candidateId,
			int jobId) throws RecordNotExistException {
		JobApplied appliedJob = candidateDao.getAppliedJob(candidateId, jobId);
		String currentStatus = appliedJob.getJobStatus().toString();
		AssessmentStatusResponse statusResponse = new AssessmentStatusResponse();
		StringBuilder newStatus = new StringBuilder();
		List<CaseQuestion> caseQuestion = jobDao.getCaseQuestion(jobId,
				Constant.LONG);
		List<CaseAnswer> allWorkcase = getWorkcaseByJob(candidateId, jobId,
				Constant.LONG);

		List<AudioInterview> allAudioInterview = new ArrayList<>();
		AudioInterview audioInterview = getAudioInterview(candidateId, jobId);
		if (audioInterview != null)
			allAudioInterview.add(audioInterview);

		List<Psychometric> allPsychometric = getPsychometric(candidateId);
		if (allPsychometric != null && !allPsychometric.isEmpty()) {
			statusResponse.setPsychometric(Boolean.TRUE);
			statusResponse.setPsychometrics(allPsychometric);
			newStatus.append(AppliedJobStatus.PSYCHOMETRICS);
			newStatus.append(Constant.UNDER_SCORE);
		}
		if (allWorkcase != null && !allWorkcase.isEmpty()) {
			if (allWorkcase.get(0) != null
					&& "Done".equalsIgnoreCase(allWorkcase.get(0).getStage())) {
				statusResponse.setWorkcase(Boolean.TRUE);
				newStatus.append(AppliedJobStatus.CASESTUDY);
				newStatus.append(Constant.UNDER_SCORE);
			} else {
				statusResponse.setWorkcase(Boolean.FALSE);
			}
			statusResponse.setWorkcases(allWorkcase);
		}
		if (allAudioInterview != null && !allAudioInterview.isEmpty()) {
			statusResponse.setAudioInterview(Boolean.TRUE);
			statusResponse.setAudioInterviews(allAudioInterview);
			newStatus.append(AppliedJobStatus.VOICEINTERVIEW);
		}
		if (caseQuestion.isEmpty())
			statusResponse.setWorkcase(Boolean.TRUE);
		if (statusResponse.isAudioInterview()
				&& statusResponse.isPsychometric()
				&& statusResponse.isWorkcase()) {
			currentStatus = AppliedJobStatus.ASSESSMENTS_COMPLETED.toValue();
		} else if (newStatus.length() > 0) {
			currentStatus = newStatus.toString().endsWith(Constant.UNDER_SCORE) ? newStatus
					.toString().substring(0, newStatus.toString().length() - 1)
					: newStatus.toString();
		}
		if (!appliedJob.getJobStatus().toString().equals(currentStatus)
				&& !AppliedJobStatus.getEmployerStatus().contains(
						appliedJob.getJobStatus().toValue())) {
			appliedJob.setJobStatus(AppliedJobStatus.fromValue(currentStatus));
		}
		return statusResponse;
	}

	@Override
	public CandidateScoring getCandidatePostScoring(int candidateId, int jobId) {
		CandidateScoring candidateScoring = null;
		CandidateProfile candidateById = candidateDao
				.getCandidateById(candidateId);
		candidateScoring = candidateDao.getCandidateScoring(candidateId, jobId,
				Constant.POSTSCREEN);
		if (candidateScoring == null
				&& COMPLETE_STATUS.equalsIgnoreCase(candidateById
						.getProfileStatus())) {
			candidateScoring = cvScreening.postscreeningCandidates(candidateId,
					jobId);
			JobApplied appliedJob = candidateDao.getAppliedJob(candidateId,
					jobId);
			if (Boolean.valueOf(candidateScoring.isExclude()))
				appliedJob.setJobStatus(AppliedJobStatus.SCREENING_FAILED);
			else
				appliedJob.setJobStatus(AppliedJobStatus.ASSESSMENTS_PENDING);
		}
		return candidateScoring;
	}

	@Override
	public CandidateScoring getCandidatePreScoring(int candidateId, int jobId)
			throws IndexCreatorException {
		CandidateScoring candidateScoring = null;
		candidateScoring = candidateDao.getCandidateScoring(candidateId, jobId,
				Constant.PRESCREEN);
		if (candidateScoring == null) {
			candidateScoring = cvScreening.prescreeningCandidates(candidateId,
					jobId);
		}
		return candidateScoring;
	}

	@Override
	public CandidateScoring updateCandidateScoring(
			CandidateScoring candidateScoring, int candidateId, int jobId) {
		CandidateScoring candidateScoringDb = getCandidatePostScoring(
				candidateId, jobId);
		candidateScoringDb.setQuestionnaireScore(candidateScoring
				.getQuestionnaireScore());
		candidateScoringDb.setVoiceScore(candidateScoring.getVoiceScore());
		candidateScoringDb
				.setWorkcaseScore(candidateScoring.getWorkcaseScore());
		candidateScoringDb.setVoiceGrade(candidateScoring.getVoiceGrade());
		candidateScoringDb
				.setWorkcaseGrade(candidateScoring.getWorkcaseGrade());
		candidateScoringDb.setFinalScore(candidateScoring.getFinalScore());
		return null;
	}

	@Override
	public CandidateProfile getCandidateByEmail(String email) {
		return candidateDao.getCandidateByEmail(email);
	}

	@Override
	public CandidateScoring unlockAppliedJob(String email, int jobId)
			throws ServiceException {
		CandidateProfile candidateByEmail = candidateDao
				.getCandidateByEmail(email);
		if (candidateByEmail == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.CANDIDATE_NOT_EXIST_EXCEPTION));
		}
		CandidateScoring candidateScoring = getCandidatePostScoring(
				candidateByEmail.getUserId(), jobId);
		if (candidateScoring == null) {
			throw new ServiceException("There is no scoring for this Job");
		}
		candidateScoring.setExclude(Boolean.FALSE);
		updateAppliedJobStatus(candidateByEmail.getUserId(), jobId,
				AppliedJobStatus.ASSESSMENTS_PENDING, "false");
		return candidateScoring;
	}

	@Override
	public double calculateFinalScore(int candidateId, int jobId) {
		double finalScore = 0;
		PersonalityScore.Score personalityScore = null;
		JobOpening jobOpening = companyDao.getJobOpeningById(jobId);
		CandidateScoring scoring = candidateDao.getCandidateScoring(
				candidateId, jobId, Constant.POSTSCREEN);
		if (scoring != null) {
			List<Psychometric> psychometrics = candidateDao
					.getPsychometric(candidateId);
			System.out.println(psychometrics);
			if (psychometrics != null && psychometrics.size() > 0) {
				Psychometric psychometric = psychometrics.get(0);
				personalityScore = PersonalityScore.calculatePersonalityScore(
						psychometric, jobOpening);
				scoring.setExclude(personalityScore.isExclude());
				if (personalityScore.isExclude())
					scoring.appendExclusionReason("psychometric");
			}
			double voiceScore = scoring.getVoiceScore();
			double CVScore = scoring.getQuestionnaireScore();
			double caseScore = scoring.getWorkcaseScore();
			finalScore = personalityScore.getScore()
					+ ((voiceScore * jobOpening.getVoiceWeightage())
							+ (CVScore * jobOpening.getCVScoreWeightage()) + (caseScore * jobOpening
							.getCaseWeightage())) / 100;
			logger.info("Final Score=" + finalScore);
			scoring.setPsycScore(personalityScore.getScore());
			scoring.setFinalScore(finalScore);
		}
		return finalScore;
	}

	@Override
	public boolean updateCandidateVoiceScoring(int candidateId, int jobId,
			double score, String grade) throws ServiceException {
		CandidateScoring scoring = getCandidatePostScoring(candidateId, jobId);
		if (scoring == null) {
			throw new ServiceException(
					"There is no scoring available to update");
		}
		scoring.setVoiceGrade(grade);
		scoring.setVoiceScore(score);
		return true;
	}

	@Override
	public boolean updateCandidateCaseScoring(int candidateId, int jobId,
			double score, String grade) throws ServiceException {
		CandidateScoring scoring = getCandidatePostScoring(candidateId, jobId);
		if (scoring == null) {
			throw new ServiceException(
					"There is no scoring available to update");
		}
		scoring.setWorkcaseGrade(grade);
		scoring.setWorkcaseScore(score);
		return true;
	}

	@Override
	public boolean updateCandidateNameAndPhone(int candidateId, String name,
			String phone) throws ServiceException {
		CandidateProfile candidate = getCandidate(candidateId);
		candidate.setFullName(name);
		candidate.setPhone(phone);
		return true;
	}
	
}
