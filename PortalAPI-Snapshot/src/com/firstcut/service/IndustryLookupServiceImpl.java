package com.firstcut.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.dao.LookupDao;

public class IndustryLookupServiceImpl implements IndustryLookupService {

	@Autowired
	private LookupDao lookupDao;

	@Override
	public List<String> getAllIndustries() {
		return lookupDao.getAllIndustries();
	}

}
