package com.firstcut.service;

import static com.firstcut.constant.Constant.INACTIVE_USER_EXCEPTION;
import static com.firstcut.constant.Constant.INVALID_PASSWORD_EXCEPTION;
import static com.firstcut.constant.Constant.INVALID_USERNAME_EXCEPTION;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.firstcut.Exception.InActiveUserException;
import com.firstcut.Exception.PasswordMismatchException;
import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.UserDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.OAuthClientDetails;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserAccountStatus;
import com.firstcut.enums.UserType;
import com.firstcut.util.BCryptPasswordEncoder;
import com.firstcut.util.PasswordGenerator;
import com.firstcut.util.ResourceManager;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.notification.ISecretEmailNotification;

public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserDao userDao;
	

	@Autowired
	private CandidateDao candidateDao;

	/*@Autowired
	private UserDetailsService detailsService;*/
	
	@Autowired
	private ISecretEmailNotification forgotPasswordEmailNotification;
	
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Override
	public User authenticate(String email, String password, UserType userType)
			throws RecordNotExistException, PasswordMismatchException,
			InActiveUserException {
		User user = userDao.getUserByUserNameAndType(email, userType);
		
		if (user == null) {
			throw new RecordNotExistException(
					ResourceManager.getProperty(INVALID_USERNAME_EXCEPTION));
		}
		if (UserAccountStatus.INACTIVE.equals(user.getAccountStatus())) {
			logger.info(email+ " have inactive account status");
			throw new InActiveUserException(
					ResourceManager.getProperty(INVALID_USERNAME_EXCEPTION));
		}
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		if (user.getPassword() == null
				|| !passwordEncoder.isPasswordValid(user.getPassword(),
						password, user.getSalt())) {
			throw new PasswordMismatchException(
					ResourceManager.getProperty(INVALID_PASSWORD_EXCEPTION));
		}
		user.generateAuthToken();
		candidateDao.saveOAuthClientDetails(user.getAuthToken());
		return user;
	}
	

	@Override
	public void forgotPassword(String email, UserType userType)
			throws RecordNotExistException, InActiveUserException, EmailSendFailureException {
		User user = userDao.getUserByUserNameAndType(email, userType);
		if (user == null) {
			throw new RecordNotExistException(
					ResourceManager.getProperty(INVALID_USERNAME_EXCEPTION));
		}
		if (UserAccountStatus.INACTIVE.equals(user.getAccountStatus())) {
			throw new InActiveUserException(
					ResourceManager.getProperty(INACTIVE_USER_EXCEPTION));
		}
		String newPassword = PasswordGenerator.generatePassword();
		user.encodePassword(newPassword);
		ContentModel content=new ContentModel();
		content.setPassword(newPassword);
		forgotPasswordEmailNotification.sendNotification(email, content);
	}

	@Override
	public void logout(String token) {
		User user;
		user = userDao.getUserByAuthToken(token);
		if (user != null) {
			user.disableAuthToken();
			user.setLastLogin(new Date());
		}
	}

	@Override
	public User saveSocialProfiles(SocialProfile socialProfile) throws ServiceException {
		String type=socialProfile.getSocialType();
		String emailId = socialProfile.getEmail();
		if(emailId==null || emailId.isEmpty()){
			throw new ServiceException("Email is not provided by social website.Please signup!!");
		}
		User user = userDao.getUserByUserNameAndType(emailId, UserType.CANDIDATE);
		if (user == null) {
			user = new User(emailId, UserType.CANDIDATE,UserAccountStatus.ACTIVE);
			user.setSocialProfile(socialProfile);
			CandidateProfile candidate = new CandidateProfile(emailId);
			candidate.setFullName((socialProfile.getFirstName()
					+ Constant.SPACE + socialProfile.getLastName()));
			candidate.setUser(user);
			user.setCandidateProfile(candidate);
			candidateDao.saveCandidate(candidate);
			socialProfile.setUser(user);
			userDao.saveSocialProfile(socialProfile);
		} else if (user.getSocialProfile() == null) {
			user.setSocialProfile(socialProfile);
			socialProfile.setUser(user);
			userDao.saveSocialProfile(socialProfile);
		} else {
			SocialProfile socialProfile2 = user.getSocialProfile();
			if (Constant.FACEBOOK.equalsIgnoreCase(type)) {
				if (socialProfile2.getFacebookId() == null
						|| socialProfile2.getFacebookId().isEmpty())
					socialProfile2.setFacebookId(socialProfile.getFacebookId());
				socialProfile2.setFacebookProfileUrl(socialProfile
						.getFacebookProfileUrl());
			} else if (Constant.LINKEDIN.equalsIgnoreCase(type)) {
				if (socialProfile2.getLinkedInId() == null
						|| socialProfile2.getLinkedInId().isEmpty())
					socialProfile2.setLinkedInId(socialProfile.getLinkedInId());
				socialProfile2.setLinkedInProfileUrl(socialProfile
						.getLinkedInProfileUrl());
				socialProfile2.setCountry(socialProfile.getCountry());
			}
			user.setSocialProfile(socialProfile2);
		}
		user.generateAuthToken();
		return user;
	}
}
