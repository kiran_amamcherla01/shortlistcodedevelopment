package  com.firstcut.service;

import java.util.List;
import java.util.Set;

import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseAnswer;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobHistory;
import com.firstcut.dto.Psychometric;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.index.IndexCreatorException;
import com.firstcut.model.AssessmentStatusResponse;
import com.notify.exception.EmailSendFailureException;


public interface CandidateService {

	AudioInterview createAudioInterview(AudioInterview audioInterview);
	
	CandidateProfile getCandidate(int candidateId) throws ServiceException;

	CandidateProfile updateCandidate(int candidateId, CandidateProfile candidateProfile) throws ServiceException;

	List<CandidateProfile> getAllCandidate();

	List<JobHistory> getAllJobHistory(int candidateId);

	JobHistory getJobHistory(int candidateId, int jobId) throws ServiceException;

	Set<JobHistory> createJobHistory(int candidateId, Set<JobHistory> jobHistory);

	JobApplied createAppliedJob(int candidateId, JobApplied jobApplied) throws ServiceException;

	List<EducationSummary> getAllEducation(int candidateId);

	EducationSummary getEducation(int candidateId, int eId) throws ServiceException;

	Set<EducationSummary> createEducation(int candidateId,
			Set<EducationSummary> educationSummary) throws ServiceException;

	List<AudioInterview> getAllAudioInterview(int candidateId);

	AudioInterview getAudioInterview(int candidateId, int jobId);

	CaseAnswer getWorkcase(int candidateId, int caseId) throws ServiceException;

	List<CaseAnswer> getAllWorkcase(int candidateId, String qtype);

	void createScoring(CandidateScoring candidateScore);

	List<Psychometric> getPsychometric(int candidateId);

	JobApplied getAppliedJob(int candidateId, int jobId) throws ServiceException;

	JobApplied updateAppliedJobStatus(int candidateId, int jobId,
			AppliedJobStatus status, String sendMail) throws ServiceException;

	List<JobApplied> getAllAppliedJobs(int candidateId, int rowCount);

	CandidateProfile createCandidate(CandidateProfile candidateProfile) throws ServiceException, EmailSendFailureException;

	AssessmentStatusResponse getAssessmentStatus(int candidateId, int jobId) throws RecordNotExistException;

	CandidateScoring getCandidatePostScoring(int candidateId, int jobId);

	CandidateScoring updateCandidateScoring(CandidateScoring candidateScoring,
			int candidateId, int jobId);

	CandidateProfile getCandidateByEmail(String email);

	List<CaseAnswer> getWorkcaseByJob(int candidateId, int jobId, String qtype);

	JobApplied updateAppliedJob(int candidateId, int jobId, JobApplied jobApplied) throws RecordNotExistException;

	CandidateScoring unlockAppliedJob(String email, int jobId) throws ServiceException;


	CandidateScoring getCandidatePreScoring(int candidateId, int jobId) throws IndexCreatorException;

	List<CaseAnswer> createWorkcase(int candidateId, int jobId,
			List<CaseAnswer> caseAnswers) throws RecordNotExistException;
	
	Boolean updateEmailSendStatusForAppliedJob(int jobId, String email, String status) throws ServiceException;

	double calculateFinalScore(int candidateId, int jobId);

	CandidateProfile getCandidateForJob(int jobId, int candidateId)
			throws ServiceException;

	Boolean updateEmailBlockStatusForRejectedMails(int jobId, List<String> emails,
			String status) throws ServiceException;
	
	boolean updateCandidateVoiceScoring(int candidateId, int jobId, double score, String grade) throws ServiceException;
	
	boolean updateCandidateCaseScoring(int candidateId, int jobId, double score, String grade) throws ServiceException;

	boolean updateCandidateNameAndPhone(int candidateId, String name,
			String phone) throws ServiceException;

}
