package com.firstcut.service;

import java.util.List;


public interface IndustryLookupService
{
	/**
	 * Get all Industries from dao layer and pass it to controller
	 * @return list of industry
	 */
	public List<String> getAllIndustries();
	
	
}
