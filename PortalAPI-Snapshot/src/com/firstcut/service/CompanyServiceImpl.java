package com.firstcut.service;

import static com.firstcut.constant.Constant.COMPANYLOGO_FOLDER;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.Exception.ServiceException;
import com.firstcut.constant.Constant;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CompanyProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Media;
import com.firstcut.dto.User;
import com.firstcut.enums.UserAccountStatus;
import com.firstcut.enums.UserType;
import com.firstcut.model.SearchFilterModel;
import com.firstcut.s3.util.AmazonClientUtil;
import com.firstcut.util.FileOperations;
import com.firstcut.util.ResourceManager;

public class CompanyServiceImpl implements CompanyService {
	@Autowired
	private CompanyDao companyDao;

	@Override
	public List<CompanyProfile> getAllCompany(String filter) {
		List<CompanyProfile> allCompany = companyDao.getAllCompany(filter);
		return allCompany;
	}

	@Override
	public CompanyProfile getCompany(int companyId) throws ServiceException {
		CompanyProfile companyById = companyDao.getCompanyById(companyId);
		if (companyById == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.COMPANY_NOT_EXIST_EXCEPTION));
		}
		return companyById;
	}

	@Override
	public List<CompanyProfile> getJobsByCompanies(SearchFilterModel model)
			throws ServiceException {
		List<CompanyProfile> companiesById = companyDao.getCompaniesById(model);
		if (companiesById == null || companiesById.isEmpty()) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.COMPANY_NOT_EXIST_EXCEPTION));
		}
		return companiesById;
	}

	@Override
	public CompanyProfile getCompanyByName(String companyName)
			throws ServiceException {
		CompanyProfile companyById = companyDao.getCompanyByName(companyName);
		if (companyById == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.COMPANY_NOT_EXIST_EXCEPTION));
		}
		return companyById;
	}

	@Override
	public CompanyProfile createCompany(CompanyProfile companyProfile) {
		CompanyProfile company = companyDao.getCompanyByName(companyProfile
				.getCompanyName());
		if (company == null) {
			company = companyDao.saveCompany(companyProfile);
		}
		return company;
	}

	@Override
	public CompanyProfile updateCompany(int companyId,
			CompanyProfile companyProfile) throws ServiceException {
		CompanyProfile company = getCompany(companyId);
		if (!StringUtils.isEmpty(companyProfile.getCompanyLogo())) {
			String key = COMPANYLOGO_FOLDER + File.separator
					+ company.getCompanyName() + ".jpg";
			String filePath = FileOperations.writeFile(
					companyProfile.getCompanyLogo(), company.getCompanyName(),
					".jpg");
			String logoURL = AmazonClientUtil.getInstance().uploadMultipart(
					filePath, key);
			companyProfile.setCompanyLogoUrl(logoURL);
		} else {
			companyProfile.setCompanyLogoUrl(company.getCompanyLogoUrl());
		}
		companyProfile.setId(company.getId());
		companyProfile.setJobOpenings(company.getJobOpenings());
		companyProfile.getMedias().addAll(company.getMedias());
		return companyDao.updateCompany(companyProfile);
	}

	public List<Media> getAllMedia(int companyId) {
		return companyDao.getAllMedia(companyId);
	}

	public Media getMedia(int companyId, int mediaId) throws ServiceException {
		Media media = companyDao.getMedia(companyId, mediaId);
		if (media == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.MEDIA_NOT_EXIST_EXCEPTION));
		}
		return media;
	}

	public List<Media> createMedia(int companyId, List<Media> medias)
			throws ServiceException {
		CompanyProfile companyProfile = getCompany(companyId);
		companyProfile.getMedias().addAll(medias);
		return medias;
	}

	@Override
	public List<HiringManager> getAllManager(int companyId) {
		return companyDao.getAllManager(companyId);
	}

	@Override
	public HiringManager getManager(int companyId, int managerId)
			throws ServiceException {
		HiringManager managerById = companyDao.getManagerById(companyId,
				managerId);
		if (managerById == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.MANAGER_NOT_EXIST_EXCEPTION));
		}
		return managerById;
	}

	@Override
	public HiringManager createManager(int companyId, HiringManager manager)
			throws ServiceException {
		HiringManager hiringManager = companyDao.getManagerByEmail(manager
				.getEmail());
		if (hiringManager != null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.USER_ALREADY_EXIST_EXCEPTION));
		}
		if (manager.getUser() == null) {
			User user = new User(manager.getEmail(), UserType.EMPLOYER,
					UserAccountStatus.ACTIVE);
			manager.setUser(user);
		}
		if (manager.getUser().getPassword() != null)
			manager.getUser().encodePassword(manager.getUser().getPassword());
		manager.getUser().generateAuthToken();
		manager.getUser().setHiringManager(manager);
		CompanyProfile company = companyDao.getCompanyById(companyId);
		manager.setCompanyProfile(company);
		company.getHiringManagers().add(manager);
		return manager;
	}

	@Override
	public List<JobOpening> getAllJobOpenings(int companyId,
			String jobOpeningStatus) throws ServiceException {
		return companyDao.getAllJobOpeningsByCompanyId(companyId,
				jobOpeningStatus);
	}

	@Override
	public List<JobOpening> getAllAppliedCandidatesByCompanyId(int companyId,
			String jobOpeningStatus, String jobStatus) {
		Set<String> jobStatuses = StringUtils
				.commaDelimitedListToSet(jobStatus);
		return companyDao.getAllAppliedCandidatesByCompanyId(companyId,
				jobOpeningStatus, jobStatuses);
	}

	@Override
	public JobOpening getJobOpening(int companyId, int jobOpeningId)
			throws ServiceException {
		JobOpening jobOpening = null;
		jobOpening = companyDao.getJobOpeningById(jobOpeningId);
		if (jobOpening == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.JOBOPENING_NOT_EXIST_EXCEPTION));
		}
		return jobOpening;
	}

	@Override
	public JobOpening createJobOpening(int companyId, JobOpening jobOpening)
			throws ServiceException {
		CompanyProfile companyProfile = getCompany(companyId);
		Date deadline = jobOpening.getSubmissionDeadline();
		jobOpening
				.setSubmissionDeadline(changeSubmissionDeadlinetoMidnight(deadline));
		jobOpening.setCompanyProfile(companyProfile);
		jobOpening = companyDao.crateJob(jobOpening);
		return jobOpening;
	}

	@Override
	public JobOpening updateJobOpening(int companyId, JobOpening jobOpening)
			throws ServiceException {
		JobOpening jobOpeningDb = getJobOpening(companyId, jobOpening.getId());
		jobOpening.setHiringManager(jobOpeningDb.getHiringManager());
		Date deadline = jobOpening.getSubmissionDeadline();
		jobOpening
				.setSubmissionDeadline(changeSubmissionDeadlinetoMidnight(deadline));
		jobOpening.setCompanyProfile(jobOpeningDb.getCompanyProfile());
		return companyDao.updateJobOpening(jobOpening);
	}

	@Override
	public JobOpening getJobOpening(String companyname, int jobOpeningId)
			throws ServiceException {
		JobOpening jobOpening = companyDao.getJobOpening(companyname,
				jobOpeningId);
		if (jobOpening == null) {
			throw new ServiceException(
					ResourceManager
							.getProperty(Constant.JOBOPENING_NOT_EXIST_EXCEPTION));
		}
		return jobOpening;
	}

	@Override
	public void deleteMedia(int companyId, int mediaId) throws ServiceException {
		Media media = getMedia(companyId, mediaId);
		CompanyProfile company = getCompany(companyId);
		company.getMedias().remove(media);
	}

	@Override
	public List<JobOpening> getAllJobOpenings(String jobOpeningStatus) {
		return companyDao.getAllJobOpenings(jobOpeningStatus);
	}

	private Date changeSubmissionDeadlinetoMidnight(Date date) {
		if (date != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			date = cal.getTime();
		}
		return date;
	}

}
