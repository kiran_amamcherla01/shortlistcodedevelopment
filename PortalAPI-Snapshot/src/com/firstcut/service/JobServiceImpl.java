package com.firstcut.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dao.JobDao;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.Psychometric;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.model.SearchFilterModel;
import com.firstcut.scoring.PersonalityScore;
import com.firstcut.scoring.PersonalityScore.Score;

public class JobServiceImpl implements JobService {

	@Autowired
	private JobDao jobDao;
	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private CandidateDao candidateDao;
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@Override
	public List<CaseQuestion> getCaseQuestions(int jobOpeningId, String qtype) {
		return jobDao.getCaseQuestion(jobOpeningId, qtype);
	}

	@Override
	public void createCaseQuestions(int jobOpeningId,
			List<CaseQuestion> caseQuestions) {
		JobOpening jobOpeningById = companyDao.getJobOpeningById(jobOpeningId);
		for (CaseQuestion caseQuestion : caseQuestions) {
			caseQuestion.setJobOpening(jobOpeningById);
			jobDao.createCaseQuestions(caseQuestion);
		}
	}

	@Override
	public void deleteCaseQuestion(CaseQuestion caseQuestion) {
		jobDao.deleteCaseQuestion(caseQuestion);

	}

	@Override
	public CaseQuestion updateCaseQuestion(int jobOpeningId,
			CaseQuestion caseQuestion) {
		JobOpening jobOpeningById = companyDao.getJobOpeningById(jobOpeningId);
		caseQuestion.setJobOpening(jobOpeningById);
		return jobDao.updateCaseQuestion(caseQuestion);
	}
	
	@Override
	public List<JobScoringCriteria> getDefaultScoringCriteria() {
		return jobDao.getDefaultScoringCriteria();
	}

	@Override
	public List<JobScoringRelation> getJobScoringCriteria(int jobId) {
		return jobDao.getJobScoringCriteria(jobId);
	}
	
	@Override
	public JobOpening getAllAppliedCandidatesByJobId(int jobId,Set appliedJobStatus) {
		return jobDao.getAllAppliedCandidatesByJobId(jobId,appliedJobStatus);
	}

	@Override
	public Set<JobScoringRelation> createJobScoringCriteria(int jobId,
			Set<JobScoringRelation> jobScoringRelations) {
		JobOpening jobOpeningById = companyDao.getJobOpeningById(jobId);
		jobOpeningById.getJobscoringRelations().clear();
		jobOpeningById.getJobscoringRelations().addAll(jobScoringRelations);
		return jobScoringRelations;
	}

	@Override
	public Set<JobScoringRelation> updateJobScoringCriteria(int jobId,
			Set<JobScoringRelation> jobScoringRelations) {
		JobOpening jobOpeningById = companyDao.getJobOpeningById(jobId);
		jobOpeningById.setJobscoringRelations(jobScoringRelations);
		return jobScoringRelations;
	}

	@Override
	public void calculateFinalScore(int jobId) {
		double finalScore = 0;
		PersonalityScore.Score personalityScore = new Score();
		JobOpening jobOpening = jobDao.getAllAppliedCandidatesByJobId(
				jobId, AppliedJobStatus.getAllStatus());
		Set<JobApplied> jobApplieds=jobOpening.getJobApplieds();
		for (JobApplied jobApplied : jobApplieds) {
			int candidateId = jobApplied.getCandidateProfile().getUserId();
			CandidateScoring scoring = candidateDao.getCandidateScoring(
					candidateId, jobId, Constant.POSTSCREEN);
			if (scoring == null)
				System.out.println(candidateId);
			if (scoring != null) {
				List<Psychometric> psychometrics = candidateDao
						.getPsychometric(candidateId);
				System.out.println(psychometrics);
				if (psychometrics != null && psychometrics.size() > 0) {
					Psychometric psychometric = psychometrics.get(0);
					personalityScore = PersonalityScore
							.calculatePersonalityScore(psychometric, jobOpening);
					scoring.setExclude(personalityScore.isExclude());
					if (personalityScore.isExclude())
						scoring.appendExclusionReason("psychometric");
				}
				double voiceScore = scoring.getVoiceScore();
				double CVScore = scoring.getQuestionnaireScore();
				double caseScore = scoring.getWorkcaseScore();
				finalScore = personalityScore.getScore()
						+ ((voiceScore * jobOpening.getVoiceWeightage())
								+ (CVScore * jobOpening.getCVScoreWeightage()) + (caseScore * jobOpening
								.getCaseWeightage())) / 100;
				logger.info("Final Score=" + finalScore);
				scoring.setPsycScore(personalityScore.getScore());
				scoring.setFinalScore(finalScore);
			}
		}
	}


	@Override
	public Set<JobOpening> getAllAppliedCandidatesForSearchCriteria(SearchFilterModel model) {
		Set<JobOpening> openings=new HashSet<>();
		List<JobOpening> jobOpenings=jobDao.getCandidatesForSearchCritera(model); 
		openings.addAll(jobOpenings);
		for (JobOpening opening:openings){
			List<CandidateScoring> scores= candidateDao.getCandidateScoringForJob(opening.getId(), Constant.POSTSCREEN);
			for(JobApplied applied:opening.getJobApplieds()){
				for(CandidateScoring score:scores){
					if(applied.getCandidateProfile().getUserId()==score.getCandidateProfile().getUserId()){
						applied.setCandidateScoring(score);
						break;
					}
				}
			}
		}
		return openings;
	}
}
