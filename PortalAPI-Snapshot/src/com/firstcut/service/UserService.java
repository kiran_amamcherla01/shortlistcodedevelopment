package com.firstcut.service;

import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.dto.ContactUs;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;

public interface UserService {
	public User getUserByAuthToken(String authToken);

	public User getUserByConfirmToken(String confirmToken);

	public User getUserById(int userId);

	void createConatctUsDetails(ContactUs contactUs);

	User getUserByUserNameAndType(String userName, UserType userType) throws RecordNotExistException;

}
