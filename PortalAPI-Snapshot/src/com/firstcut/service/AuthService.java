package com.firstcut.service;

import com.firstcut.Exception.InActiveUserException;
import com.firstcut.Exception.PasswordMismatchException;
import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;
import com.notify.exception.EmailSendFailureException;


public interface AuthService {

	User authenticate(String email, String password,UserType userType) throws RecordNotExistException, PasswordMismatchException, InActiveUserException;

	void forgotPassword(String email,UserType userType) throws RecordNotExistException, InActiveUserException, EmailSendFailureException;

	void logout(String email);
	
	public User saveSocialProfiles(SocialProfile socialProfile) throws ServiceException;

}
