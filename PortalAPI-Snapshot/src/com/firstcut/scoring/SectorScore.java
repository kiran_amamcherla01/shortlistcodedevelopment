/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class SectorScore implements IScore {

	private static Logger logger = Logger.getLogger(SectorScore.class);

	/**
	 * Method calculates industry score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String sector = candidateProfile.getTargetIndustry();
		float score = 0.0F;
		try {
			if (Utils.checkNUllOrEmpty(sector)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_SECTOR);
					}
				}
			} else {
				score = (float) (candidateScoring.getSectorSemScore() * jobscoringcriteria
						.getWeightage());
				if (candidateScoring.getSectorScore_1_1() > 0)
					score = (float) (1 * jobscoringcriteria.getWeightage());
				if (score == 0) {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_SECTOR);
					}
				}
			}
			candidateScoring.setSectorScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}
}
