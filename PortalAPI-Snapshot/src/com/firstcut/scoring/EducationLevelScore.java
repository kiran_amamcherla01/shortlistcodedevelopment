/**
 * 
 */
package com.firstcut.scoring;

import java.util.Set;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;

/**
 * @author MindBowser-Android
 * 
 */
public class EducationLevelScore implements IScore {

	private static Logger logger = Logger.getLogger(EducationLevelScore.class);
	private float score = 0.0F;
	private String capeducation1;
	private String capeducation2;
	private String jobeducation1;
	private String jobeducation2;
	private boolean notReported = false;

	// @Override
	// public void calculateScoreWithWeightage(JobOpening jobOpening,
	// JobScoringRelation jobscoringcriteria,
	// CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
	// String METHOD_NAME = "calculateScoreWithWeightage";
	// String degree = null;
	// Set<EducationSummary> educationSummaries = candidateProfile
	// .getEducationSummaries();
	// for (EducationSummary educationSummarie : educationSummaries) {
	// degree = educationSummarie.getDegree();
	// }
	// try {
	// if (degree == null || degree.isEmpty()) {
	// if (jobscoringcriteria.isForgiveNr())
	// score = 1.0F * jobscoringcriteria.getWeightage();
	// else {
	// if (jobscoringcriteria.isExclusion()) {
	// candidateScoring.setExclude(true);
	// candidateScoring
	// .appendExclusionReason(EXCLUDE_REASON_EDUCATION);
	// }
	// }
	// } else {
	// score = (float) (candidateScoring.getEducationScore() *
	// jobscoringcriteria
	// .getWeightage());
	// }
	// candidateScoring.setEducationScore(score);
	// } catch (Exception e) {
	// logger.error(METHOD_NAME, e);
	// }
	// }

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		try {
			capeducation1 = candidateProfile.getUGDegree();
			capeducation2 = candidateProfile.getPGDegree();
			jobeducation1 = jobOpening.getEducation();
			jobeducation2 = jobOpening.getEducation2();
			if (capeducation1 == null && capeducation2 == null) {
				notReported = true;
			} else if (capeducation1 != null && capeducation2 == null) {
				UGAvailableAndPGNot(jobOpening);
			} else if (capeducation1 == null && capeducation2 != null) {
				UGNotAndPGAvailable(jobOpening);
			} else if (capeducation1 != null && capeducation2 != null) {
				UGAvailableAndPGAvailable(jobOpening);
			}
			if (notReported) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F;
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_EDUCATION);
					}
				}
			}
			if (jobscoringcriteria.isExclusion() && score == 0.0F) {
				candidateScoring.setExclude(true);
				candidateScoring
						.appendExclusionReason(EXCLUDE_REASON_EDUCATION);
			}
			candidateScoring.setEducationScore(score
					* jobscoringcriteria.getWeightage());
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}

	}

	private void UGAvailableAndPGNot(JobOpening jobOpening) {
		if (ANY_UNDERGRADUATE.equalsIgnoreCase(jobeducation1))
			score = 1.0F;
		if (jobeducation1.equalsIgnoreCase(capeducation1))
			score = 1.0F;
		else if (capeducation2 == null) {
			notReported = true;
			score = 0.0F;
		}

	}

	private void UGNotAndPGAvailable(JobOpening jobOpening) {
		if (ANY_UNDERGRADUATE.equalsIgnoreCase(jobeducation1))
			score = 1.0F;
		if (ANY_POSTGRADUATE.equalsIgnoreCase(jobeducation2))
			score = 1.0F;
		if (capeducation2.equalsIgnoreCase(jobeducation2))
			score = 1.0F;
		if (!ANY_UNDERGRADUATE.equalsIgnoreCase(jobeducation1)) {
			notReported = true;
			score = 0.0F;
		}

	}

	private void UGAvailableAndPGAvailable(JobOpening jobOpening) {
		if (ANY_UNDERGRADUATE.equalsIgnoreCase(jobeducation1))
			score = 1.0F;
		if (capeducation1.equalsIgnoreCase(jobeducation1))
			score = 1.0F;
		if (capeducation1.equalsIgnoreCase(jobeducation1)
				&& ANY_POSTGRADUATE.equalsIgnoreCase(jobeducation2))
			score = 1.0F;
		if (capeducation2.equalsIgnoreCase(jobeducation2)
				&& ANY_UNDERGRADUATE.equalsIgnoreCase(jobeducation1))
			score = 1.0F;
		if (capeducation2.equalsIgnoreCase(jobeducation2))
			score = 1.0F;
		if (capeducation1.equalsIgnoreCase(jobeducation1)
				&& capeducation2.equalsIgnoreCase(jobeducation2))
			score = 1.0F;
	}
}
