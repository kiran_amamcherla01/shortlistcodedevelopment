/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.enums.CurrencyType;

/**
 * @author MindBowser-Android
 * 
 */
public class AnnualSalaryScore implements IScore {

	private static Logger logger = Logger.getLogger(AnnualSalaryScore.class);

	/**
	 * Method calculates annual salary score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */


	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String salaryNegotiable = null;
		try {
			float score = 0.0F;
			Double opsMinAnnualSalary = jobOpening.getOpsMinAnnualSalary();
			Double opsMaxAnnualSalary = jobOpening.getOpsMaxAnnualSalary();
			Double jdMaxAnnualSalary = jobOpening.getMaxAnnualSalary();
			Double jdMinAnnualSalary = jobOpening.getMinAnnualSalary();
			Double currentAnnualSalary = candidateProfile
					.getCurrentAnnualSalary();
			String currency = candidateProfile.getCurrency();
			JobApplied appliedJob = candidateProfile.getAppliedJob(jobOpening
					.getId());
			if (appliedJob != null) {
				salaryNegotiable = appliedJob.getSalaryNegotiable();
			}
			if (currency != null
					&& CurrencyType.USD.toValue().equalsIgnoreCase(currency)) {
				currentAnnualSalary = candidateProfile.getCurrentAnnualSalary() / 100;
				currentAnnualSalary = currentAnnualSalary * 65;
			}
			if (currentAnnualSalary == null) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_ANNUAL_SALARY);
					}
				}
			} else {
				if ((currentAnnualSalary >= jdMinAnnualSalary && currentAnnualSalary <= jdMaxAnnualSalary)) {
					score = 1.0F * jobscoringcriteria.getWeightage();
				} else if ((currentAnnualSalary >= opsMinAnnualSalary && currentAnnualSalary <= jdMinAnnualSalary)
						|| (currentAnnualSalary >= jdMaxAnnualSalary && currentAnnualSalary <= opsMaxAnnualSalary)
						|| (YES.equalsIgnoreCase(salaryNegotiable) && currentAnnualSalary > opsMaxAnnualSalary)) {
					score = 0.5F * jobscoringcriteria.getWeightage();
				} else if (currentAnnualSalary < opsMinAnnualSalary
						|| currentAnnualSalary > opsMaxAnnualSalary) {
					score = 0.0F;
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_ANNUAL_SALARY);
					}
				}
			}
			candidateScoring.setAnnualSalaryScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}

	}
}
