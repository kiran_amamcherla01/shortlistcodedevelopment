/**
 * 
 */
package com.firstcut.scoring;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class CompaniesToExcludeScore implements IScore {

	private static Logger logger = Logger
			.getLogger(CompaniesToExcludeScore.class);

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		List<String> companiesToExcludeList = new ArrayList<>();
		try {
			float score = 0.0F;
			companiesToExcludeList
					.addAll(StringUtils.commaDelimitedListToSet(jobOpening
							.getCompaniesToExclude()));
			String candidateCurrentCompany = candidateProfile
					.getCurrentCompany();
			if (Utils.checkNUllOrEmpty(candidateCurrentCompany)) {
				score = 1.0F * jobscoringcriteria.getWeightage();
			} else {
				if (match(companiesToExcludeList, candidateCurrentCompany)) {
					score = 0.0F;
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_COMPANY_EXCLUSION);
					}
				} else {
					score = 1.0F * jobscoringcriteria.getWeightage();
				}
			}
			candidateScoring.setCompanyToExcludeScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}

	}

	private boolean match(List<String> companiesToExcludeList,
			String candidateCurrentCompany) {
		for (String companyExc : companiesToExcludeList) {
			if (companyExc.toLowerCase().contains(
					candidateCurrentCompany.toLowerCase())
					|| candidateCurrentCompany.toLowerCase().contains(
							companyExc.toLowerCase()))
				return true;
		}
		return false;
	}
}
