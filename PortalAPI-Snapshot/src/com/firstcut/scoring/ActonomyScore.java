package com.firstcut.scoring;

import java.util.List;

import org.apache.log4j.Logger;

import com.actonomy.client.MatchClient;
import com.actonomy.xmp.GroupDetailScore;
import com.actonomy.xmp.MatchHit;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.service.CandidateServiceImpl;

public class ActonomyScore {
	private static Logger logger = Logger.getLogger(ActonomyScore.class);

	public static void getScore(JobOpening jobOpening,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		int userId = candidateProfile.getUserId();
		Integer jobId = jobOpening.getId();
		MatchClient client = new MatchClient();
		logger.info("Match score for candiadte " + userId + " with jobId= "
				+ jobId);
		MatchHit matchHit = client.matchCandidateToJob(jobId, userId);
		if (matchHit == null) {
			logger.info("Recieved Match score null from Aconomy");
		} else {
			candidateScoring.setActonomyScore(matchHit.getScore());
			List<GroupDetailScore> profileDetailScore = client
					.getProfileDetailScore(matchHit);
			for (GroupDetailScore groupDetailScore : profileDetailScore) {
				if ("JOB_TITLE".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setCurrentJobTitleScore(groupDetailScore
							.getScore());
				if ("DOMAINS".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setDomainSemScore(groupDetailScore
							.getScore());
				if ("DOMAIN_1_1".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setDomainScore_1_1(groupDetailScore
							.getScore());
				if ("SECTORS".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setSectorSemScore(groupDetailScore
							.getScore());
				if ("SECTOR_1_1".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setSectorScore_1_1(groupDetailScore
							.getScore());
				if ("COMPETENCIES".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring
							.setSkillsScore(groupDetailScore.getScore());
				if ("EDUCATION".equalsIgnoreCase(groupDetailScore.getName()))
					candidateScoring.setEducationScore(groupDetailScore
							.getScore());
			}
		}
	}
}
