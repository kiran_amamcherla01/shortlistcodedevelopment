/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class DomainScore implements IScore {

	private static Logger logger = Logger.getLogger(DomainScore.class);

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String domain = candidateProfile.getTargetFunction();
		float score = 0.0F;
		try {
			if (Utils.checkNUllOrEmpty(domain)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_DOMAIN);
					}
				}
			} else {
				score = (float) (candidateScoring.getDomainSemScore() * jobscoringcriteria
						.getWeightage());
				if (candidateScoring.getDomainScore_1_1() > 0)
					score = (float) (1 * jobscoringcriteria.getWeightage());
				if (score == 0) {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_DOMAIN);
					}
				}
			}
			candidateScoring.setDomainScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}
}
