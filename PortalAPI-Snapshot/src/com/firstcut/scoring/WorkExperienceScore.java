/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;

/**
 * @author MindBowser-Android
 * 
 */

public class WorkExperienceScore implements IScore {

	private static Logger logger = Logger.getLogger(WorkExperienceScore.class);

	/**
	 * Method calculates work experience score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		float score = 0.0F;
		try {
			Double opsMinimumExp = jobOpening.getOpsMinimumExp();
			Double opsMaximumExp = jobOpening.getOpsMaximumExp();
			Double jdMinimumExp = jobOpening.getMinimumExp();
			Double jdMaximumExp = jobOpening.getMaximumExp();
			Double totalExperience = candidateProfile.getTotalExperience();

			if (totalExperience == null) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_WORK_EXPERIENCE);
					}
				}
			} else {
				if (totalExperience >= jdMinimumExp
						&& totalExperience <= jdMaximumExp) {
					score = 1.0F * jobscoringcriteria.getWeightage();
				} else if ((totalExperience >= opsMinimumExp && totalExperience <= jdMinimumExp)
						|| (totalExperience >= jdMaximumExp && totalExperience <= opsMaximumExp)) {
					score = 0.5F * jobscoringcriteria.getWeightage();
				} else if (totalExperience < opsMinimumExp
						|| totalExperience > opsMaximumExp) {
					score = 0.0F;
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_WORK_EXPERIENCE);
					}
				}
			}
			candidateScoring.setWorkExperienceScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

}
