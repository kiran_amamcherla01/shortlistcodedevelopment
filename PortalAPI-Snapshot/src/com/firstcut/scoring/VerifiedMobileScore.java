/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;

/**
 * @author MindBowser-Android
 * 
 */
public class VerifiedMobileScore implements IScore {

	private static Logger logger = Logger
			.getLogger(VerifiedMobileScore.class);

	/**
	 * Method calculates verified mobile score of a candidate
	 * 
	 *  @param jobOpening
	 *  @param jobscoringcriteria
	 *  @param candidateProfile
	 *  @param candidateScoring
	 *
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		
	}
}
