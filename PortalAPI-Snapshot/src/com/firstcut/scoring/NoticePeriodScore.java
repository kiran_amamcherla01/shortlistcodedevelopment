/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;

/**
 * @author MindBowser-Android
 * 
 */
public class NoticePeriodScore implements IScore {

	private static Logger logger = Logger.getLogger(NoticePeriodScore.class);

	/**
	 * Method calculates notice period score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		try {
			float score = 0.0F;
			Integer candidateNoticePeriod = candidateProfile.getNoticePeriod();
			if (candidateNoticePeriod == null) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_NOTICE_PERIOD);
					}
				}
			} else {
				//0-30 ---> 1
				if (candidateNoticePeriod <= 30) {
					score = 1.0F * jobscoringcriteria.getWeightage();
					// 31-60 --->0.5
				} else if (candidateNoticePeriod <= 60) {
					score = 0.5F * jobscoringcriteria.getWeightage();
					// >60 ----> 0
				} else if (candidateNoticePeriod > 60){
					score = 0.0F ;
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_NOTICE_PERIOD);
					}
				}
			}
			candidateScoring.setNoticePeriodScore(score);
		}catch(Exception e){
			logger.error(METHOD_NAME, e);
		}
		
	}
}
