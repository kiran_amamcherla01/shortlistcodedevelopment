/**
 * 
 */
package com.firstcut.scoring;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dao.LookupDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.ApplicationBeanUtil;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class TopUniversitiesScore implements IScore {

	private static Logger logger = Logger.getLogger(TopUniversitiesScore.class);

	/**
	 * Method calculates top university score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		try {
			float score = 0.0F;
			List<String> checkUniversityAndTier = Collections.emptyList();
			String universityTier = jobOpening.getTopUniversities();
			List<String> collList = candidateProfile.getAttendedCollegeNames();
			collList.remove(null);
			collList.remove("");
			if (universityTier == null
					|| (universityTier != null && universityTier.trim()
							.isEmpty())) {
				score = 1 * jobscoringcriteria.getWeightage();
			} else if (collList.size() == 0) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_TOP_UNIVERSITY);
					}
				}
			} else {
				LookupDao lookupDao = (LookupDao) ApplicationBeanUtil
						.getBean("lookupDao");
				if (TIER_ONE.equalsIgnoreCase(universityTier)) {
					String[] tiers = { TIER_ONE };
					checkUniversityAndTier = lookupDao
							.getUniversityByTier(tiers);
				} else if (TIER_TWO.equalsIgnoreCase(universityTier)) {
					String[] tiers = { TIER_ONE, TIER_TWO };
					checkUniversityAndTier = lookupDao
							.getUniversityByTier(tiers);
				}
				if (match(checkUniversityAndTier, collList)) {
					score = 1.0F * jobscoringcriteria.getWeightage();
				} else {
					score = 0.0F;
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_TOP_UNIVERSITY);
					}
				}
			}
			candidateScoring.setTopUniversitiesScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

	private boolean match(List<String> list1, List<String> list2) {
		list1.remove(null);
		list2.remove(null);
		for (String item1 : list1) {
			for (String item2 : list2) {
				if (item2 != null
						&& (item1.toLowerCase().contains(item2.toLowerCase()) || item2
								.toLowerCase().contains(item1.toLowerCase())))
					return true;
			}
		}
		return false;
	}
}
