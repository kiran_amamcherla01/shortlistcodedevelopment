package com.firstcut.scoring;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseAnswer;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;

@Component
public class WorkcaseScore implements IScore {

	@Autowired
	private CandidateDao candidateDao;

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		float score = 0.0F;
		List<CaseAnswer> shortCaseAnswers = candidateDao.getAllWorkcases(
				candidateProfile.getUserId(), Constant.SHORT);
		for (CaseAnswer shortCaseAnswer : shortCaseAnswers) {
			String answer = shortCaseAnswer.getCaseQuestion().getAnswer();
			if (shortCaseAnswer.getAnswer().equalsIgnoreCase(answer)) {
				score += 1.0F;
			}
		}
		score = (score / shortCaseAnswers.size());
		candidateScoring.setShortcaseScore(score);
	}
}
