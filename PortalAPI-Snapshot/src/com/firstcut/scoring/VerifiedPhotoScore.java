/**
 * 
 */
package com.firstcut.scoring;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;

/**
 * @author MindBowser-Android
 *
 */
public class VerifiedPhotoScore implements IScore {

	/**
	 * Method calculates verified photo score of a candidate
	 * 
	 *  @param jobOpening
	 *  @param jobscoringcriteria
	 *  @param candidateProfile
	 *  @param candidateScoring
	 *
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		
	}
}
