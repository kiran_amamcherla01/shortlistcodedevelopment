package com.firstcut.scoring;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Psychometric;

public class PersonalityScore {

	private static Logger logger = Logger.getLogger(PersonalityScore.class);

	public static PersonalityScore.Score calculatePersonalityScore(
			Psychometric psychometric, JobOpening jobOpening) {
		PersonalityScore.Score personalityScore = new Score();
		double finalScore = 0;
		double recommendationScore = 0;
		double shortlistScore = 0;
		double employeeScore = 0;

		String[] recommendations = StringUtils
				.commaDelimitedListToStringArray(jobOpening
						.getRecommendations());
		String[] startupFits = StringUtils
				.commaDelimitedListToStringArray(jobOpening.getStartupFit());
		String[] companyFits = StringUtils
				.commaDelimitedListToStringArray(jobOpening.getCompanyFit());
		String[] jobFits = StringUtils
				.commaDelimitedListToStringArray(jobOpening.getJobFit());

		double proprietaryThresold = jobOpening.getProprietaryThresold();
		try {
			for (String recommendation : recommendations) {
				Field field1 = Psychometric.class
						.getDeclaredField(recommendation);
				field1.setAccessible(Boolean.TRUE);
				String value = (String) field1.get(psychometric);
				double score = getRecommendationScore(value);
				if (score == 0) {
					personalityScore.setExclude(Boolean.TRUE);
				}
				recommendationScore += score;
			}

			for (String startupFit : startupFits) {
				Field field = Psychometric.class.getDeclaredField(startupFit);
				field.setAccessible(Boolean.TRUE);
				Double score = (Double) field.get(psychometric);
				if (score < proprietaryThresold) {
					personalityScore.setExclude(Boolean.TRUE);
				}
				shortlistScore += score;
			}
			shortlistScore = (shortlistScore) / 3;

			for (String companyFit : companyFits) {
				Field field = Psychometric.class.getDeclaredField(companyFit);
				field.setAccessible(Boolean.TRUE);
				Double score = (Double) field.get(psychometric);
				if (score < proprietaryThresold) {
					personalityScore.setExclude(Boolean.TRUE);
				}
				employeeScore += score;
			}

			for (String jobFit : jobFits) {
				Field field = Psychometric.class.getDeclaredField(jobFit);
				field.setAccessible(Boolean.TRUE);
				Double score = (Double) field.get(psychometric);
				if (score == 0) {
					personalityScore.setExclude(Boolean.TRUE);
				}
				employeeScore += score;
			}
			employeeScore = (employeeScore) / 6;

			if (!psychometric.getResponseStyle().startsWith("Genuine")) {
				personalityScore.setExclude(Boolean.TRUE);
			}

		} catch (SecurityException | NoSuchFieldException
				| IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		finalScore = (recommendationScore * jobOpening.getPsyRecmWeightage()
				+ shortlistScore * jobOpening.getPsyOwnWeightage() + employeeScore
				* jobOpening.getPsyEmpWeightage())/100;
		personalityScore.setScore(finalScore);
		logger.info("Psychometric Score=" + personalityScore.getScore()
				+ " & exclude=" + personalityScore.isExclude());
		return personalityScore;

	}

	private static double getRecommendationScore(String value) {
		double score = 0;
		if ("Recommended".equalsIgnoreCase(value)) {
			score = 100;
		}
		if ("Moderately Recommended".equalsIgnoreCase(value)) {
			score = 50;
		}
		if ("Not Recommended".equalsIgnoreCase(value)) {
			score = 0;
		}
		return score;
	}

	public static class Score {
		double score;
		boolean exclude;

		public double getScore() {
			return score;
		}

		public void setScore(double score) {
			this.score = score;
		}

		public boolean isExclude() {
			return exclude;
		}

		public void setExclude(boolean exclude) {
			this.exclude = exclude;
		}

		@Override
		public String toString() {
			return "Score [score=" + score + ", exclude=" + exclude + "]";
		}

	}
}
