/**
 * 
 */
package com.firstcut.scoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class JobTitleScore implements IScore {

	private static Logger logger = Logger.getLogger(JobTitleScore.class);

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String currentJobTitle = candidateProfile.getCurrentJobTitle();
		List<String> jobTitles = new ArrayList<>();
		float score = 0.0F;
		try {
			jobTitles.addAll(StringUtils.commaDelimitedListToSet(jobOpening
					.getJobTitle()));
			if (Utils.checkNUllOrEmpty(currentJobTitle)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_JOB_TITLE);
					}
				}
			} else {
				score = (float) (candidateScoring.getCurrentJobTitleScore() * jobscoringcriteria
						.getWeightage());
				if (score == 0) {
					if (match(jobTitles, currentJobTitle)) {
						score = 1 * jobscoringcriteria.getWeightage();
					}
				}
				if (score == 0) {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_JOB_TITLE);
					}
				}
			}
			candidateScoring.setCurrentJobTitleScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

	private boolean match(List<String> list, String value) {
		for (String item : list) {
			if (item.toLowerCase().contains(value.toLowerCase())
					|| value.toLowerCase().contains(item.toLowerCase()))
				return true;
		}
		return false;
	}
}
