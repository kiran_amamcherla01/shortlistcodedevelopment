/**
 * 
 */
package com.firstcut.scoring;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class CompaniesToIncludeScore implements IScore {

	private static Logger logger = Logger
			.getLogger(CompaniesToIncludeScore.class);

	/**
	 * Method calculates company inclusion score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		List<String> companiesToIncludeList = new ArrayList<>();
		try {
			float score = 0.0F;
			companiesToIncludeList
					.addAll(StringUtils.commaDelimitedListToSet(jobOpening
							.getCompaniesToInclude()));
			List<String> allCompanyNames = candidateProfile
					.getAttendedCompanyNames();
			if (match(companiesToIncludeList, allCompanyNames)) {
				score = 1.0F * jobscoringcriteria.getWeightage();
			}
			candidateScoring.setCompanyToIncludeScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

	private boolean match(List<String> companiesToIncludes,
			List<String> allCompanyNames) {
		for (String companyInc : companiesToIncludes) {
			for (String candidateCurrentCompany : allCompanyNames) {
				if (candidateCurrentCompany != null
						&& (companyInc.toLowerCase().contains(
								candidateCurrentCompany.toLowerCase()) || candidateCurrentCompany
								.toLowerCase().contains(
										companyInc.toLowerCase())))
					return true;
			}
		}
		return false;
	}
}
