/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class CurrentlyEmployedScore implements IScore {

	private static Logger logger = Logger
			.getLogger(CurrentlyEmployedScore.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.firstcut.scoring.IScore#calculateScoreWithWeightage(com
	 * .firstcut.dto.JobOpening, com.firstcut.dto.JobScoringRelation,
	 * com.firstcut.dto.CandidateProfile, com.firstcut.dto.CandidateScoring)
	 */
	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String currentJobStatus = candidateProfile.getCurrentJobStatus();
		try {
			float score = 0.0F;
			if (Utils.checkNUllOrEmpty(currentJobStatus)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_CURRENTLY_EMPLOYED);
					}
				}
			} else {
				if (YES.equalsIgnoreCase(candidateProfile.getCurrentJobStatus())) {
					score = 1.0F * jobscoringcriteria.getWeightage();
				} else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_CURRENTLY_EMPLOYED);
					}
				}
			}
			candidateScoring.setCurrentlyEmployedScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

}
