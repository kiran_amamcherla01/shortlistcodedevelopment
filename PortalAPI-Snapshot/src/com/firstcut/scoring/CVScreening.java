/**
 * 
 */
package com.firstcut.scoring;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.audit.Auditable;
import com.firstcut.audit.AuditingActionType;
import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dao.JobDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.enums.AppliedJobStatus;

/**
 * @author MindBowser-Android
 * 
 */
public class CVScreening {

	@Autowired
	private CandidateDao candidateDao;
	@Autowired
	private CompanyDao companyDao;
	@Autowired
	private JobDao jobDao;
	private Logger logger = Logger.getLogger(this.getClass().getName());

//	public static void main(String a[]) throws Exception {
//		ApplicationContext context = new ClassPathXmlApplicationContext(
//				"applicationContext.xml");
//		candidateDao = (CandidateDao) context.getBean("candidateDao");
//		companyDao = (CompanyDao) context.getBean("companyDao");
//		jobDao = (JobDao) context.getBean("jobDao");
//		// JobOpening jobOpening = jobDao.getAllAppliedCandidatesByJobId(12,
//		// AppliedJobStatus.getAllStatus());
//		// Set<JobApplied> jobApplieds = jobOpening.getJobApplieds();
//		// for (JobApplied jobApplied : jobApplieds) {
//		// if (jobApplied.getCandidateProfile().getProfileStatus()
//		// .equalsIgnoreCase("COMPLETE")) {
//		// int candidateId = jobApplied.getCandidateProfile().getUserId();
//		// new CVScreening().postscreeningCandidates(candidateId, 11);
//		// }
//		// }
//		Integer[] candiadtes = { 19662,19700};
//		for (int candidateId : candiadtes) {
//			new CVScreening().prescreeningCandidates(candidateId, 11);
//		}
//	}

	@Auditable(actionType = AuditingActionType.EXPERIENCE_SCORING_COMPLETED)
	public CandidateScoring prescreeningCandidates(int candidateId, int jobId) {
		CandidateScoring candidateScore = null;
		JobOpening jobOpenings = companyDao.getJobOpeningById(jobId);
		if (jobOpenings != null) {
			Set<JobScoringRelation> jobscoringRelations = jobOpenings
					.getJobscoringRelations();
			Set<JobScoringRelation> preScreeningCriteria = new HashSet<>();
			for (JobScoringRelation jobScoringRelation : jobscoringRelations) {
				if (Constant.PRESCREEN.equalsIgnoreCase(jobScoringRelation
						.getStage())) {
					preScreeningCriteria.add(jobScoringRelation);
				}
			}
			CandidateProfile candidate = candidateDao
					.getCandidateById(candidateId);
			candidateScore = calculateCandidateScore(candidate, jobOpenings,
					preScreeningCriteria);
			candidateScore.setStage(Constant.PRESCREEN);
			candidateDao.createScoring(candidateScore);
			logger.info(candidateScore);
		}
		return candidateScore;
	}

	@Auditable(actionType = AuditingActionType.EXPERIENCE_SCORING_COMPLETED)
	public CandidateScoring postscreeningCandidates(int candidateId, int jobId) {
		CandidateScoring candidateScore = null;
		JobOpening jobOpenings = companyDao.getJobOpeningById(jobId);
		if (jobOpenings != null) {
			Set<JobScoringRelation> postScreeningCriteria = new HashSet<>();
			Set<JobScoringRelation> jobscoringRelations = jobOpenings
					.getJobscoringRelations();
			for (JobScoringRelation jobScoringRelation : jobscoringRelations) {
				if (Constant.POSTSCREEN.equalsIgnoreCase(jobScoringRelation
						.getStage())) {
					postScreeningCriteria.add(jobScoringRelation);
				}
			}
			CandidateProfile candidate = candidateDao
					.getCandidateById(candidateId);
			candidateScore = calculateCandidateScore(candidate, jobOpenings,
					postScreeningCriteria);
			candidateScore.setStage(Constant.POSTSCREEN);
			candidateDao.createScoring(candidateScore);
			logger.info(candidateScore);
			logger.info("KK candidate score "+candidateScore);
		}
		return candidateScore;
	}

	private CandidateScoring calculateCandidateScore(
			CandidateProfile candidate, JobOpening jobOpening,
			Set<JobScoringRelation> jobscoringcriteria) {
		CandidateScoring scoring = new CandidateScoring();
		// uncomment for new scoring
		ActonomyScore.getScore(jobOpening, candidate, scoring);
		try {
			for (JobScoringRelation criteria : jobscoringcriteria) {
				JobScoringCriteria scoringcriteria = criteria
						.getJobScoringCriteria();
				String className = scoringcriteria.getType();
				Class<?> loadClass = CVScreening.class.getClassLoader()
						.loadClass(className);
				IScore calculatorObj = (IScore) loadClass.newInstance();
				// change method to calculateScoreWtihWeightage
				calculatorObj.calculateScoreWithWeightage(jobOpening, criteria,
						candidate, scoring);
			}
		} catch (ClassNotFoundException | SecurityException
				| IllegalAccessException | InstantiationException e) {
			e.printStackTrace();
		}
		scoring.setJobOpening(jobOpening);
		scoring.setQuestionnaireScore(scoring.calculateQuestionnaireScore());
		scoring.setCandidateProfile(candidate);
		return scoring;
	}
}
