/**
 * 
 */
package com.firstcut.scoring;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class LocationScore implements IScore {

	private static Logger logger = Logger.getLogger(LocationScore.class);

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String willingToRelocate = null;
		List<String> openingCitiesList = new ArrayList<>();
		List<String> relocationCitiesList = new ArrayList<>();
		float score = 0.0F;
		try {
			JobApplied appliedJob = candidateProfile.getAppliedJob(jobOpening
					.getId());
			if (appliedJob != null)
				willingToRelocate = appliedJob.getWillingToRelocate();
			String currentLocation = StringUtils
					.trimAllWhitespace(candidateProfile.getAddress());
			if (jobOpening.getJobLocation() != null)
				openingCitiesList.addAll(StringUtils
						.commaDelimitedListToSet(StringUtils
								.trimAllWhitespace(jobOpening.getJobLocation()
										.toLowerCase())));
			if (jobOpening.getNearJobLocation() != null)
				openingCitiesList.addAll(StringUtils
						.commaDelimitedListToSet(StringUtils
								.trimAllWhitespace(jobOpening
										.getNearJobLocation().toLowerCase())));
			if (candidateProfile.getRelocationCities() != null) {
				relocationCitiesList.addAll(StringUtils
						.commaDelimitedListToSet(StringUtils
								.trimAllWhitespace(candidateProfile
										.getRelocationCities().toLowerCase())));
			}
			if (Utils.checkNUllOrEmpty(currentLocation)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_LOCATION);
					}
				}
			} else {
				if (match(openingCitiesList, currentLocation)) {
					score = 1.0F * jobscoringcriteria.getWeightage();
				} else if (YES.equalsIgnoreCase(willingToRelocate)) {
					score = 0.5F * jobscoringcriteria.getWeightage();
				} else if (match(openingCitiesList, relocationCitiesList)) {
					score = 0.5F * jobscoringcriteria.getWeightage();
				} else {
					score = 0.0F * jobscoringcriteria.getWeightage();
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_LOCATION);
					}
				}
			}
			candidateScoring.setLocationScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}

	private boolean match(List<String> openingCitiesList, String currentLocation) {
		openingCitiesList.remove(null);
		for (String openingCity : openingCitiesList) {
			if (currentLocation.toLowerCase().contains(
					openingCity.toLowerCase().trim()))
				return true;
		}
		return false;
	}

	private boolean match(List<String> list1, List<String> list2) {
		list1.remove(null);
		list2.remove(null);
		for (String item1 : list1) {
			for (String item2 : list2) {
				if (item2 != null
						&& (item1.toLowerCase().contains(item2.toLowerCase()) || item2
								.toLowerCase().contains(item1.toLowerCase())))
					return true;
			}
		}
		return false;
	}
}
