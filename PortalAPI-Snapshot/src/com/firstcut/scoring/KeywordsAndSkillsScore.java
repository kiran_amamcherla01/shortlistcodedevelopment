/**
 * 
 */
package com.firstcut.scoring;

import org.apache.log4j.Logger;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.util.Utils;

/**
 * @author MindBowser-Android
 * 
 */
public class KeywordsAndSkillsScore implements IScore {
	private static Logger logger = Logger
			.getLogger(KeywordsAndSkillsScore.class);

	/**
	 * Method calculates keywords and skills score of a candidate
	 * 
	 * @param jobOpening
	 * @param jobscoringcriteria
	 * @param candidateProfile
	 * @param candidateScoring
	 * 
	 */

	@Override
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring) {
		String METHOD_NAME = "calculateScoreWithWeightage";
		String skills = candidateProfile.getSkills();
		float score = 0.0F;
		try {
			if (Utils.checkNUllOrEmpty(skills)) {
				if (jobscoringcriteria.isForgiveNr())
					score = 1.0F * jobscoringcriteria.getWeightage();
				else {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_KEYWORD_SKILLS);
					}
				}
			} else {
				score = (float) (candidateScoring.getSkillsScore() * jobscoringcriteria
						.getWeightage());
				if (score == 0) {
					if (jobscoringcriteria.isExclusion()) {
						candidateScoring.setExclude(true);
						candidateScoring
								.appendExclusionReason(EXCLUDE_REASON_KEYWORD_SKILLS);
					}
				}
			}
			candidateScoring.setSkillsScore(score);
		} catch (Exception e) {
			logger.error(METHOD_NAME, e);
		}
	}
}
