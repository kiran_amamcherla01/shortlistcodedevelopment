/**
 * 
 */
package com.firstcut.scoring;

import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringRelation;

/**
 * @author MindBowser-Android
 *
 */
public interface IScore {
	
	public static final String NR = "NR";
	public static final double ZERO = 0;
	public static final double ONE = 1;
	public static final String HALF = "0.5";
	public static final String YES = "Yes";
	public static final String ANY = "ANY";
	public static final String COMMA = ",";
	public static final String UG = "UG";
	public static final String PG = "PG";
	public static final String FULL_TIME = "Full Time";
	public static final String NO = "no";
	public static final String ANY_UNDERGRADUATE="Any Under Graduate";
	public static final String ANY_POSTGRADUATE="Any Post Graduate";
	public static final String NOT_REQUIRED="Not Required";
	public static final String TIER_ONE="Tier 1";
	public static final String TIER_TWO="Tier 2";
	
	
	public static final String EXCLUDE_REASON_ANNUAL_SALARY="Annual-salary";
	public static final String EXCLUDE_REASON_COMPANY_EXCLUSION="Company-to-exclude";
	public static final String EXCLUDE_REASON_COMPANY_INCLUSION="Company-to-include";
	public static final String EXCLUDE_REASON_EDUCATION="Education";
	public static final String EXCLUDE_REASON_FUNCTION="domain";
	public static final String EXCLUDE_REASON_INDUSTRY="sector";
	public static final String EXCLUDE_REASON_JOB_TITLE="Job-title";
	public static final String EXCLUDE_REASON_KEYWORD_SKILLS="Keyword-and-skills";
	public static final String EXCLUDE_REASON_LOCATION="Location";
	public static final String EXCLUDE_REASON_NOTICE_PERIOD="Notice-period";
	public static final String EXCLUDE_REASON_TOP_UNIVERSITY="Top-university";
	public static final String EXCLUDE_REASON_VERIFIED_MOBILE="Verified-mobile";
	public static final String EXCLUDE_REASON_VERIFIED_PHOTO="Verified-photo";
	public static final String EXCLUDE_REASON_WORK_EXPERIENCE="Work-experience";
	public static final String EXCLUDE_REASON_CURRENTLY_EMPLOYED="Currently-employed";
	public static final String EXCLUDE_REASON_DOMAIN="domain";
	public static final String EXCLUDE_REASON_SECTOR="sector";
	
	public void calculateScoreWithWeightage(JobOpening jobOpening,
			JobScoringRelation jobscoringcriteria,
			CandidateProfile candidateProfile, CandidateScoring candidateScoring);

}
