package  com.firstcut.interceptor;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class CustomOutFaultInterceptor extends
		AbstractPhaseInterceptor<Message> {
	private boolean handleMessageCalled;

	public CustomOutFaultInterceptor() {
		super(Phase.MARSHAL);
	}


	public void handleMessage(Message message) throws Fault {
		handleMessageCalled = true;
		Exception ex = message.getContent(Exception.class);
		if (ex == null) {
			throw new RuntimeException("Exception is expected");
		}
		if (!(ex instanceof Fault)) {
			throw new RuntimeException("Fault is expected");
		}
		message.getInterceptorChain().abort();

	}

	protected boolean handleMessageCalled() {
		return handleMessageCalled;
	}
}