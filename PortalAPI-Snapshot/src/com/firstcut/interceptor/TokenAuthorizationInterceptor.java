package com.firstcut.interceptor;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.firstcut.constant.Constant;
import com.firstcut.dto.User;
import com.firstcut.enums.ApiResponseStatusType;
import com.firstcut.model.ErrorModel;
import com.firstcut.model.ResponseModel;
import com.firstcut.service.UserService;
import com.firstcut.util.ResourceManager;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class TokenAuthorizationInterceptor extends
		AbstractPhaseInterceptor<Message> {
	private static Logger logger = Logger
			.getLogger(TokenAuthorizationInterceptor.class);
	@Autowired
	private UserService userService;

	public TokenAuthorizationInterceptor() {
		super(Phase.PRE_INVOKE); // Put this interceptor in this phase
	}

	public void handleMessage(Message message) throws RuntimeException {
		HttpServletRequest httpRequest = (HttpServletRequest) message
				.get(AbstractHTTPDestination.HTTP_REQUEST);
		// get the authToken value from header
		String path = httpRequest.getPathInfo();
		String method = httpRequest.getMethod();
		if (!((method.equalsIgnoreCase("POST") && path.contains("auth"))
				||(method.equalsIgnoreCase("PUT") && path.contains("forgotpassword"))
				|| method.contains("OPTIONS")
				|| path.contains("test-notification")
				|| path.endsWith("contactus")
				|| path.contains("lookup")
				|| (method.equalsIgnoreCase("POST") && path
						.endsWith("managers"))
				|| (method.equalsIgnoreCase("POST") && path.endsWith("company"))
				|| (method.equalsIgnoreCase("POST") && path
						.endsWith("candidates"))
				|| (method.equalsIgnoreCase("GET") && path.contains("view"))
				|| (method.equalsIgnoreCase("GET") && path
						.contains("validateuser")) || (method
				.equalsIgnoreCase("GET") && path.contains("audios")))) {
			if(!(path.contains("api-docs"))) {
				String authToken = httpRequest.getHeader("Auth-Token");
				User userDto = userService.getUserByAuthToken(authToken);
				if (userDto == null) {
					logger.info("No User is exist with authToken=   " + authToken);
					throwError(message);
				}
				httpRequest.setAttribute(Constant.USER, userDto);
			}
		}
	}

	public void throwError(Message message) {
		String errorResponse = null;
		String errorMessage = ResourceManager
				.getProperty(Constant.INVALID_TOKEN_EXCEPTION);
		ResponseModel responseModel = ResponseModel
				.build(ApiResponseStatusType.ERROR);
		ErrorModel error = new ErrorModel();
		responseModel.setError(error);

		ObjectMapper mapper = new ObjectMapper();
		try {
			errorResponse = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		HttpServletResponse response = (HttpServletResponse) message
				.get(AbstractHTTPDestination.HTTP_RESPONSE);
		response.setStatus(401);
		try {
			response.getWriter().write(errorResponse);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		throw new org.apache.cxf.interceptor.security.AccessDeniedException(
				errorMessage);
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
