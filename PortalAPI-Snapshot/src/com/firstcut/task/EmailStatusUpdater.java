package com.firstcut.task;

import static com.notify.constant.NotifyConstants.MANDRILL_STATUS_QUEUED;
import static com.notify.constant.NotifyConstants.MANDRILL_STATUS_REJECTED;
import static com.notify.constant.NotifyConstants.MANDRILL_STATUS_SENT;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.Exception.ServiceException;
import com.firstcut.dao.CandidateDao;
import com.firstcut.enums.EmailBlockStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.service.CandidateService;
import com.notify.model.NotificationStatusModel;

@Aspect
public class EmailStatusUpdater {

	@Autowired
	private CandidateDao candidateDao;

	@Autowired
	private CandidateService candidateService;

	private static Logger logger = Logger.getLogger(EmailStatusUpdater.class);

	@AfterReturning(pointcut = "execution(* *.execute (..))", returning = "emailStatusList")
	public void afterReturning(List<EmailStatusModel> emailStatusList) {
		logger.info(emailStatusList);
		for (EmailStatusModel emailStatusModel : emailStatusList) {
			if (!emailStatusModel.getStatus().isEmpty()) {
				List<String> emails = getSentEmails(emailStatusModel
						.getStatus());
				String collectionToCommaDelimitedString = StringUtils
						.collectionToCommaDelimitedString(emails);
				candidateDao.updateCandidateEmailStatus(
						collectionToCommaDelimitedString, emailStatusModel
								.getEmailStatus().toValue(), emailStatusModel
								.getJobId());
				emails = getFailedEmails(emailStatusModel.getStatus());
				try {
					if (!emails.isEmpty()) {
						candidateService
								.updateEmailBlockStatusForRejectedMails(
										emailStatusModel.getJobId(), emails,
										EmailBlockStatus.BLOCKED.toValue());
					}
				} catch (ServiceException e) {
					logger.error(e);
				}

			}
		}
	}

	private List<String> getSentEmails(List<NotificationStatusModel> statuses) {
		List<String> emails = new ArrayList<>();
		for (NotificationStatusModel status : statuses) {
			if (status.getStatus().equals(MANDRILL_STATUS_SENT)
					|| status.getStatus().equals(MANDRILL_STATUS_QUEUED))
				emails.add(status.getRecipient().toLowerCase());
		}
		return emails;
	}

	private List<String> getFailedEmails(List<NotificationStatusModel> statuses) {
		List<String> emails = new ArrayList<>();
		for (NotificationStatusModel status : statuses) {
			if (status.getStatus().equals(MANDRILL_STATUS_REJECTED))
				emails.add(status.getRecipient().toLowerCase());
		}
		return emails;
	}
}
