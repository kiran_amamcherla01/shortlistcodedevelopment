package com.firstcut.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.ShortlistEmailNotification;

public class ShortlistClousreTask {
	private static Logger logger = Logger.getLogger(ShortlistClousreTask.class);
	@Autowired
	CompanyDao companyDao;
	@Autowired
	private ShortlistEmailNotification shortlistEmailNotification;

	/**
	 * Execute this task
	 * 
	 */
	public List<EmailStatusModel> execute() {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = { AppliedJobStatus.SHORTLIST.toValue() };
		String[] emailStatus = { EmailStatus.SHORTLIST_CLOSURE_EMAIL_SENT
				.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);

			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				EmailStatusModel status = new EmailStatusModel(
						jobOpening.getId(),
						EmailStatus.SHORTLIST_CLOSURE_EMAIL_SENT);
				List<CandidateProfile> candidates = companyDao
						.getAppliedCandidatesByStatusAndJobId(
								jobOpening.getId(), delimitedEmailStatus,
								delimitedJobStatus, Constant.ZERO_DAYS_INTERVAL);
				for (CandidateProfile candidate : candidates) {
					logger.debug("Total no of candidates : "
							+ candidates.size());
					ContentModel convertObjectToContent = Utils
							.getContentModel(candidate, jobOpening);
					try {
						NotificationStatusModel sendNotification = shortlistEmailNotification
								.sendNotification(candidate.getEmail(),
										convertObjectToContent);
						status.getStatus().add(sendNotification);
					} catch (EmailSendFailureException e) {
						logger.error(e);
					}
				}
				emailStatusList.add(status);
			}
			logger.info("Shortlist Clousre Task runs successfully...");
		} catch (Exception e) {
			logger.error("Shortlist Clousre Task ", e);
		}
		logger.info("ShortlistClousreTask runs successfully...");
		return emailStatusList;
	}
}
