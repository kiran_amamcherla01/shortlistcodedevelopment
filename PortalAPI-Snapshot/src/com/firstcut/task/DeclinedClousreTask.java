package com.firstcut.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.DeclinedEmailNotification;

public class DeclinedClousreTask {
	private static Logger logger = Logger.getLogger(DeclinedClousreTask.class);
	@Autowired
	CompanyDao companyDao;

	@Autowired
	private CandidateDao candidateDao;

	@Autowired
	DeclinedEmailNotification declinedEmailNotification;

	/**
	 * Execute this task
	 * 
	 */
	public List<EmailStatusModel> execute() {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = { AppliedJobStatus.DECLINED.toValue() };
		String[] emailStatus = { EmailStatus.DECLINED_CLOSURE_EMAIL_SENT
				.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.DECLINED_CLOSURE_EMAIL_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.ZERO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						JobApplied job = candidateDao.getAppliedJob(
								candidate.getUserId(), jobOpening.getId());
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						convertObjectToContent.setEmployerRejectionReason(getEmployerRejectionMessage(job
								.getRejectionReason()));
						try {
							NotificationStatusModel sendNotification = declinedEmailNotification
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error("Declined closure task", e);
						}
					}
					emailStatusList.add(status);
				}
			}
			logger.info("Declined closure task runs successfully...");
		} catch (Exception e) {
			logger.error("Declined closure task", e);
		}
		return emailStatusList;
	}

	private String getEmployerRejectionMessage(String rejectionReason) {
		String message;
		switch (rejectionReason) {
		case Constant.PREVIOUS_EXPERIENCE_NOT_RELEVANT:
			message = Constant.PREVIOUS_EXPERIENCE_NOT_RELEVANT_MESSAGE;
			break;
		case Constant.TOO_SENIOR_FOR_THE_ROLE:
			message=Constant.TOO_SENIOR_FOR_THE_ROLE_MESSAGE;
			break;
		case Constant.COMMUNICATION_SKILLS_NOT_STRONG_ENOUGH:
			message=Constant.COMMUNICATION_SKILLS_NOT_STRONG_ENOUGH_MESSAGE;
			break;
		case Constant.CASE_EXERCISE_NOT_STRONG_ENOUGH:
			message=Constant.CASE_EXERCISE_NOT_STRONG_ENOUGH_MESSAGE;
			break;
		case Constant.CULTURAL_FIT_NOT_RIGHT:
			message=Constant.CULTURAL_FIT_NOT_RIGHT_MESSAGE;
			break;
		default:
			message=Constant.OTHER_REJECTION_REASON_MESSAGE;
		}
		return message;
	}
}
