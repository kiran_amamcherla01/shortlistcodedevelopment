/**
 * 
 */
package com.firstcut.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.IdleApplicationEmailNotification;

/**
 * @author MindBowser-Android
 * 
 */
public class IdleApplicationTask {
	private static Logger logger = Logger.getLogger(IdleApplicationTask.class);

	@Autowired
	CompanyDao companyDao;

	@Autowired
	private IdleApplicationEmailNotification idleApplicationEmailNotification;

	/**
	 * Execute this task
	 * 
	 */
	public List<EmailStatusModel> execute() {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = {
				AppliedJobStatus.APPLICATION_SUBMITTED.toValue(),
				AppliedJobStatus.ASSESSMENTS_COMPLETED.toValue() };
		String[] emailStatus = { EmailStatus.IDLE_APPLICATION_EMAIL_SENT
				.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				EmailStatusModel status = new EmailStatusModel(
						jobOpening.getId(),
						EmailStatus.IDLE_APPLICATION_EMAIL_SENT);
				List<CandidateProfile> candidates = companyDao
						.getAppliedCandidatesByStatusAndJobId(
								jobOpening.getId(), delimitedEmailStatus,
								delimitedJobStatus,
								Constant.SEVEN_DAYS_INTERVAL);
				logger.debug("Total no of candidates : " + candidates.size());
				for (CandidateProfile candidate : candidates) {
					ContentModel convertObjectToContent = Utils
							.getContentModel(candidate, jobOpening);
					try {
						NotificationStatusModel sendNotification = idleApplicationEmailNotification
								.sendNotification(candidate.getEmail(),
										convertObjectToContent);
						status.getStatus().add(sendNotification);
					} catch (EmailSendFailureException e) {
						logger.error("Idle application task", e);
					}
				}
				emailStatusList.add(status);
			}
			logger.info("Idle application task runs successfully...");
		} catch (Exception e) {
			logger.error("Idle application task", e);
		}
		return emailStatusList;
	}

}
