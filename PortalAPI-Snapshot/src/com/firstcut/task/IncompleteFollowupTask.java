/**
 * 
 */
package com.firstcut.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.ResourceManager;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.IncompleteAssessmentFollowup;

/**
 * @author MindBowser-Android
 * 
 */
public class IncompleteFollowupTask {
	private static Logger logger = Logger
			.getLogger(IncompleteFollowupTask.class);
	@Autowired
	CompanyDao companyDao;
	
	@Autowired
	private CandidateDao candidateDao;
	
	@Autowired
	private IncompleteAssessmentFollowup incompleteAssessmentFollowup;
	@Autowired
	private EmailStatusUpdater emailStatusUpdater;

	public void execute() {
		List<JobOpening> allJobOpenings = companyDao
				.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
		List<EmailStatusModel> emailStatusList = null;
		emailStatusList = executeFirstFollowup(allJobOpenings);
		emailStatusUpdater.afterReturning(emailStatusList);
		emailStatusList = executeSecondFollowup(allJobOpenings);
		emailStatusUpdater.afterReturning(emailStatusList);
	}

	private List<EmailStatusModel> executeFirstFollowup(
			List<JobOpening> allJobOpenings) {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = {
				AppliedJobStatus.ACCOUNT_CREATED.toValue(),
				AppliedJobStatus.PRESCREENING_PASS.toValue(),
				AppliedJobStatus.ASSESSMENTS_PENDING.toValue(),
				AppliedJobStatus.PSYCHOMETRICS.toValue(),
				AppliedJobStatus.CASESTUDY.toValue(),
				AppliedJobStatus.VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.CASESTUDY_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY_VOICEINTERVIEW
						.toValue() };
		String[] emailStatus = {
				EmailStatus.INCOMPLETE_FIRST_FOLLOWUP_SENT.toValue(),
				EmailStatus.INCOMPLETE_SECOND_FOLLOWUP_SENT.toValue(),
				EmailStatus.REJECTED_CLOSURE_EMAIL_SENT.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.INCOMPLETE_FIRST_FOLLOWUP_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.TWO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						JobApplied job = candidateDao.getAppliedJob(
								candidate.getUserId(), jobOpening.getId());
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						convertObjectToContent.setMessage(ResourceManager
								.getProperty(Constant.FOLLOWUP+"_"+job.getJobStatus().toValue()));
						try {
							NotificationStatusModel sendNotification = incompleteAssessmentFollowup
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					logger.info(candidates);
					emailStatusList.add(status);
				}
			}
			logger.info("Incomplete Assessment first followup runs successfully...");
		} catch (Exception e) {
			logger.error("Incomplete Assessment first followup", e);
		}
		return emailStatusList;

	}

	private List<EmailStatusModel> executeSecondFollowup(
			List<JobOpening> allJobOpenings) {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = {
				AppliedJobStatus.ACCOUNT_CREATED.toValue(),
				AppliedJobStatus.PRESCREENING_PASS.toValue(),
				AppliedJobStatus.ASSESSMENTS_PENDING.toValue(),
				AppliedJobStatus.PSYCHOMETRICS.toValue(),
				AppliedJobStatus.CASESTUDY.toValue(),
				AppliedJobStatus.VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.CASESTUDY_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY_VOICEINTERVIEW
						.toValue() };
		String[] emailStatus = { EmailStatus.WELCOME_EMAIL_SENT.toValue(),
				EmailStatus.INCOMPLETE_SECOND_FOLLOWUP_SENT.toValue(),
				EmailStatus.REJECTED_CLOSURE_EMAIL_SENT.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.INCOMPLETE_SECOND_FOLLOWUP_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.TWO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						JobApplied job = candidateDao.getAppliedJob(
								candidate.getUserId(), jobOpening.getId());
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						convertObjectToContent.setMessage(ResourceManager
								.getProperty(Constant.FOLLOWUP+"_"+job.getJobStatus().toValue()));
						try {
							NotificationStatusModel sendNotification = incompleteAssessmentFollowup
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					logger.debug(candidates);
					emailStatusList.add(status);
				}
			}
			logger.info("Incomplete Assessment second followup runs successfully...");
		} catch (Exception e) {
			logger.error("Incomplete Assessment second followup", e);
		}
		return emailStatusList;
	}

	public List<String> getcandidateEmails(List<CandidateProfile> candidates) {
		List<String> emails = new ArrayList<>();
		for (CandidateProfile candidate : candidates) {
			emails.add(candidate.getEmail());
		}
		return emails;

	}
}
