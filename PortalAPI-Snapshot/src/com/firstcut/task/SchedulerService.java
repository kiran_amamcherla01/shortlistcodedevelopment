package com.firstcut.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.constant.Constant;
import com.firstcut.util.ResourceManager;

@Transactional
public class SchedulerService implements ISchedulerService {

	@Autowired
	private ShortlistClousreTask shortlistClousreTask;
	@Autowired
	private NotShortlistClousreTask notShortlistClousreTask;
	@Autowired
	private ApprovedClousreTask approvedClousreTask;
	@Autowired
	private DeclinedClousreTask declinedClousreTask;
	@Autowired
	private RejectedClousreTask rejectedClousreTask;
	@Autowired
	private WaitlistClousreTask waitlistClousreTask;
	@Autowired
	private InactiveFollowupTask inactiveFollowupTask;
	@Autowired
	private IncompleteFollowupTask incompleteFollowupTask;
	@Autowired
	private AutoExpireShortlistsTask autoExpireShortlistsTask;
	@Autowired
	private AutoExpireWaitlistsTask autoExpireWaitlistsTask;
	@Autowired
	private IdleApplicationTask idleApplicationTask;
	@Autowired
	private InvitationEmailTask invitationEmailTask;

	@Override
	public void executeNotShortlistClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			notShortlistClousreTask.execute();
		}
	}

	@Override
	public void executeShortlistClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			shortlistClousreTask.execute();
		}
	}

	@Override
	public void executeApprovedClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			approvedClousreTask.execute();
		}

	}

	@Override
	public void executeRejectedClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			rejectedClousreTask.execute();
		}

	}

	@Override
	public void executeWaitlistClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			waitlistClousreTask.execute();
		}

	}

	@Override
	public void executeDeclinedClousreTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			declinedClousreTask.execute();
		}

	}

	@Override
	public void executeInactiveFollowupTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			inactiveFollowupTask.execute();
		}

	}

	@Override
	public void executeIncompleteTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			incompleteFollowupTask.execute();
		}

	}

	@Override
	public void executeAutoExpireShortlistsTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			autoExpireShortlistsTask.execute();
		}
	}

	@Override
	public void executeAutoExpireWaitlistsTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			autoExpireWaitlistsTask.execute();
		}
	}

	@Override
	public void executeIdleApplicationTask() {
		if (Constant.TRUE.equalsIgnoreCase(ResourceManager
				.getProperty(Constant.NOTIFICATION_ENABLED))) {
			idleApplicationTask.execute();
		}
	}

	@Override
	public void executeInvitationEmailTask() {
		invitationEmailTask.execute();
	}

}
