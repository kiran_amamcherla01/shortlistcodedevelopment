package com.firstcut.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.CandidateSource;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.ResourceManager;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.RejectionEmailNotification;

public class RejectedClousreTask {
	private static Logger logger = Logger.getLogger(RejectedClousreTask.class);
	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private CandidateDao candidateDao;

	@Autowired
	private RejectionEmailNotification rejectionEmailNotification;

	/**
	 * Execute this task
	 * 
	 * @throws ParseException
	 * 
	 */
	public List<EmailStatusModel> execute() {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = {
				AppliedJobStatus.NEW.toValue(),
				AppliedJobStatus.ACCOUNT_CREATED.toValue(),
				AppliedJobStatus.PRESCREENING_PASS.toValue(),
				AppliedJobStatus.ASSESSMENTS_PENDING.toValue(),
				AppliedJobStatus.PSYCHOMETRICS.toValue(),
				AppliedJobStatus.CASESTUDY.toValue(),
				AppliedJobStatus.VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.CASESTUDY_VOICEINTERVIEW.toValue(),
				AppliedJobStatus.PSYCHOMETRICS_CASESTUDY_VOICEINTERVIEW
						.toValue() };
		String[] emailStatus = { EmailStatus.REJECTED_CLOSURE_EMAIL_SENT
				.toValue() };
		try {
			// getting current time
			Calendar cal = Calendar.getInstance();
			String currentDate = dateFormat.format(cal.getTime());
			Date current = dateFormat.parse(currentDate);
			logger.info("current Date = " + currentDate);
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN
							.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				// getting submission time
				String submissionDeadline = dateFormat.format(jobOpening
						.getSubmissionDeadline().getTime());
				Date deadline = dateFormat.parse(submissionDeadline);
				logger.info(jobOpening.getJobTitle()
						+ " , submission deadline= " + submissionDeadline);

				if (submissionDeadline != null && deadline.before(current)) {
					logger.info("Rejected closure task Running for "
							+ jobOpening.getJobTitle()
							+ " and submission deadline"
							+ jobOpening.getSubmissionDeadline());
					/*EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.REJECTED_CLOSURE_EMAIL_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.ZERO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						JobApplied job = candidateDao.getAppliedJob(
								candidate.getUserId(), jobOpening.getId());
						if(AppliedJobStatus.NEW.toValue().equalsIgnoreCase(job.getJobStatus().toValue())
								&& (CandidateSource.DIRECT.equals(candidate.getSource())
										||CandidateSource.MONSTERDB.equals(candidate.getSource()))
										||CandidateSource.NAUKRIDB.equals(candidate.getSource())){
							continue;
						}
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						convertObjectToContent.setMessage(ResourceManager
								.getProperty(job.getJobStatus().toValue())
								.replace(
										Constant.COMPANY_PLACEHOLDER,
										jobOpening.getCompanyProfile()
												.getCompanyName()));
						try {
							NotificationStatusModel sendNotification = rejectionEmailNotification
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					emailStatusList.add(status);*/
					logger.info("Rejected closure task runs successfully...");
					logger.info("-----------------------------------------------------");
				}
			}
		} catch (Exception e) {
			logger.error("Rejected closure task ", e);
		}

		return emailStatusList;
	}

	// public static void main(String a[]) throws Exception {
	// ApplicationContext context = new ClassPathXmlApplicationContext(
	// "applicationContext.xml");
	// candidateDao = (CandidateDao) context.getBean("candidateDao");
	// companyDao = (CompanyDao) context.getBean("companyDao");
	// new RejectedClousreTask().execute();
	// }

}
