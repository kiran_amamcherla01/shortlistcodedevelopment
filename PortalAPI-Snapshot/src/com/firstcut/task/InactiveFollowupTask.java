/**
 * 
 */
package com.firstcut.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.InactiveCandidateFollowup;

/**
 * @author MindBowser-Android
 * 
 */
public class InactiveFollowupTask {

	private static Logger logger = Logger.getLogger(InactiveFollowupTask.class);
	@Autowired
	CompanyDao companyDao;

	@Autowired
	private InactiveCandidateFollowup inactiveCandidateFollowup;

	@Autowired
	private EmailStatusUpdater emailStatusUpdater;

	/**
	 * Execute this task
	 * 
	 */
	public void execute() {
		List<JobOpening> allJobOpenings = companyDao
				.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
		List<EmailStatusModel> emailStatusList = executeFirstFollowup(allJobOpenings);
		emailStatusUpdater.afterReturning(emailStatusList);
		emailStatusList = executeSecondFollowup(allJobOpenings);
		emailStatusUpdater.afterReturning(emailStatusList);
	}

	private List<EmailStatusModel> executeFirstFollowup(
			List<JobOpening> allJobOpenings) {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = { AppliedJobStatus.NEW.toValue() };
		String[] emailStatus = {
				EmailStatus.INACTIVE_FIRST_FOLLOWUP_SENT.toValue(),
				EmailStatus.INACTIVE_SECOND_FOLLOWUP_SENT.toValue()};
		String delimitedJobStatus = StringUtils.arrayToDelimitedString(
				jobStatus, Constant.COMMA);
		try {
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.INACTIVE_FIRST_FOLLOWUP_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.TWO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						try {
							NotificationStatusModel sendNotification = inactiveCandidateFollowup
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					emailStatusList.add(status);
				}
			}
			logger.info("Inactive candidate first followup task runs successfully...");
		} catch (Exception e) {
			logger.error(e);
		}
		return emailStatusList;
	}

	private List<EmailStatusModel> executeSecondFollowup(
			List<JobOpening> allJobOpenings) {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = { AppliedJobStatus.NEW.toValue() };
		String[] emailStatus = { EmailStatus.WELCOME_EMAIL_SENT.toValue(),
				EmailStatus.INACTIVE_SECOND_FOLLOWUP_SENT.toValue()};
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.INACTIVE_SECOND_FOLLOWUP_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.TWO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						try {
							NotificationStatusModel sendNotification = inactiveCandidateFollowup
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					emailStatusList.add(status);
				}
			}
			logger.info("Inactive candidate second followup task runs successfully...");
		} catch (Exception e) {
			logger.error(e);
		}

		return emailStatusList;
	}

	public List<String> getcandidateEmails(List<CandidateProfile> candidates) {
		List<String> emails = new ArrayList<>();
		for (CandidateProfile candidate : candidates) {
			emails.add(candidate.getEmail());
		}
		return emails;

	}
}
