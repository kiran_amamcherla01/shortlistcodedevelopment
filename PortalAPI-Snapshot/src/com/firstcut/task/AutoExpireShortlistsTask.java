/**
 * 
 */
package com.firstcut.task;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.AutoExpireShortlistEmailNotification;

/**
 * @author MindBowser-Android
 * 
 */
public class AutoExpireShortlistsTask {

	private static Logger logger = Logger
			.getLogger(AutoExpireShortlistsTask.class);

	@Autowired
	CandidateDao candidateDao;

	@Autowired
	CompanyDao companyDao;

	@Autowired
	private AutoExpireShortlistEmailNotification autoExpireShortlistEmailNotification;

	public void execute() {
		String[] jobStatus = { AppliedJobStatus.SHORTLIST.toValue() };
		String[] emailStatus = { EmailStatus.AUTO_EXPIRY_EMAIL_SENT.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);
			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				List<CandidateProfile> candidates = companyDao
						.getAppliedCandidatesByStatusAndJobId(
								jobOpening.getId(), delimitedEmailStatus,
								delimitedJobStatus,
								Constant.SEVEN_DAYS_INTERVAL);
				logger.debug("Total no of candidates : " + candidates.size());
				for (CandidateProfile candidate : candidates) {
					ContentModel convertObjectToContent = Utils
							.getContentModel(candidate, jobOpening);
					try {
						NotificationStatusModel sendNotification = autoExpireShortlistEmailNotification
								.sendNotification(candidate.getEmail(),
										convertObjectToContent);
						logger.info(sendNotification);
					} catch (EmailSendFailureException e) {
						logger.error("Auto expire shortlist candidates task", e);
					}
				}
			}
			logger.info("Auto expiring shortlisted candidates");
			int updateCount = candidateDao
					.updateCandidateJobAndEmailStatusForAutoExpiry(
							AppliedJobStatus.SHORTLIST.toValue(),
							AppliedJobStatus.DECLINED_AUTO.toValue(),
							EmailStatus.AUTO_EXPIRY_EMAIL_SENT.toValue(),
							Constant.SEVEN_DAYS_INTERVAL);
			logger.info("Auto expired shortlisted candidates:" + updateCount);
		} catch (Exception e) {
			logger.error("Auto expire shortlist candidates task", e);
		}
	}

}
