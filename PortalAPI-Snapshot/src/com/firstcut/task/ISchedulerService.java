package com.firstcut.task;

public interface ISchedulerService {

	
	 /**
     * Execute not shortlist Task
     * 
     * @param  
     * @throws 
     * @return 
     */
    public void executeNotShortlistClousreTask();
     
    /**
     * Execute Shorlist task
     * 
     * @param  
     * @throws 
     * @return 
     */
    public void executeShortlistClousreTask();
    /**
     * Execute Approved task
     * 
     * @param  
     * @throws 
     * @return 
     */
    public void executeApprovedClousreTask();
    /**
     * Execute rejected task
     * 
     * @param  
     * @throws 
     * @return 
     */
    public void executeRejectedClousreTask();

	void executeWaitlistClousreTask();

	void executeDeclinedClousreTask();

	void executeIncompleteTask();

	void executeInactiveFollowupTask();
	
	void executeAutoExpireShortlistsTask();
	
	void executeAutoExpireWaitlistsTask();
	
	void executeIdleApplicationTask();
	
	void executeInvitationEmailTask();
    
}
