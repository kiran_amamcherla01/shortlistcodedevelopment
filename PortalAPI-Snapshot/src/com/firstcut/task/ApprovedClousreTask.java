package com.firstcut.task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailStatus;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.model.EmailStatusModel;
import com.firstcut.util.Utils;
import com.notify.exception.EmailSendFailureException;
import com.notify.model.ContentModel;
import com.notify.model.NotificationStatusModel;
import com.notify.notification.ApprovedEmailNotification;

public class ApprovedClousreTask {
	private static Logger logger = Logger.getLogger(ApprovedClousreTask.class);
	@Autowired
	CompanyDao companyDao;

	@Autowired
	private ApprovedEmailNotification approvedEmailNotification;

	/**
	 * Execute this task
	 * 
	 */
	public List<EmailStatusModel> execute() {
		List<EmailStatusModel> emailStatusList = new ArrayList<>();
		String[] jobStatus = { AppliedJobStatus.APPROVED.toValue() };
		String[] emailStatus = { EmailStatus.APPROVED_CLOSURE_EMAIL_SENT
				.toValue() };
		try {
			String delimitedJobStatus = StringUtils.arrayToDelimitedString(
					jobStatus, Constant.COMMA);
			String delimitedEmailStatus = StringUtils.arrayToDelimitedString(
					emailStatus, Constant.COMMA);

			List<JobOpening> allJobOpenings = companyDao
					.getAllJobOpeningsByEmailStatus(JobOpeningStatus.OPEN.toValue());
			for (JobOpening jobOpening : allJobOpenings) {
				if (jobOpening.getSubmissionDeadline()!=null && jobOpening.getSubmissionDeadline().after(new Date())) {
					EmailStatusModel status = new EmailStatusModel(
							jobOpening.getId(),
							EmailStatus.APPROVED_CLOSURE_EMAIL_SENT);
					List<CandidateProfile> candidates = companyDao
							.getAppliedCandidatesByStatusAndJobId(
									jobOpening.getId(), delimitedEmailStatus,
									delimitedJobStatus,
									Constant.ZERO_DAYS_INTERVAL);
					logger.debug("Total no of candidates : "
							+ candidates.size());
					for (CandidateProfile candidate : candidates) {
						ContentModel convertObjectToContent = Utils
								.getContentModel(candidate, jobOpening);
						try {
							NotificationStatusModel sendNotification = approvedEmailNotification
									.sendNotification(candidate.getEmail(),
											convertObjectToContent);
							status.getStatus().add(sendNotification);
						} catch (EmailSendFailureException e) {
							logger.error(e);
						}
					}
					emailStatusList.add(status);
				}
			}
			logger.info("Approved closure task runs successfully...");
		} catch (Exception e) {
			logger.error("Approved closure task", e);
		}
		return emailStatusList;
	}
}
