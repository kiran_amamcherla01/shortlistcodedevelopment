package com.firstcut.audit;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.firstcut.constant.Constant;
import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.UserDao;
import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.AuditLog;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.MettlNotification;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;

@Aspect
public class AuditingAspect {
	@Autowired
	private UserDao userDao;
	@Autowired
	private CandidateDao candidateDao;

	@AfterReturning("@annotation(auditable)")
	public void logAuditActivity(JoinPoint jp, Auditable auditable) {
		AuditLog auditLog = new AuditLog();
		User currentUser = null;
		try {
			ServletRequestAttributes t = (ServletRequestAttributes) RequestContextHolder
					.currentRequestAttributes();
			HttpServletRequest request = t.getRequest();
			String auditingUsernameIp = request.getHeader("X-FORWARDED-FOR");
			if (auditingUsernameIp == null) {
				auditingUsernameIp = request.getRemoteAddr();
			}
			String userAgent = request.getHeader("User-Agent");
			String userName = null;
			UserType userType = null;
			currentUser = (User) request.getAttribute(Constant.USER);
			String methodName=jp.getSignature().getName();
			String action = auditable.actionType().toString();
			String targetUser="";
			if (jp.getSignature().getName().equalsIgnoreCase("createCandidate")) {
				Object[] args = jp.getArgs();
				CandidateProfile candidateProfile = (CandidateProfile) args[0];
				currentUser = candidateProfile.getUser();
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			} else if (jp.getSignature().getName()
					.equalsIgnoreCase("createManager")) {
				Object[] args = jp.getArgs();
				HiringManager hiringManager = (HiringManager) args[1];
				currentUser = hiringManager.getUser();
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			} else if (jp.getSignature().getName().equalsIgnoreCase("login")) {
				Object[] args = jp.getArgs();
				currentUser = (User) args[0];
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			} else if (jp.getSignature().getName()
					.equalsIgnoreCase("socialLoginCallBack")) {
				Object[] args = jp.getArgs();
				SocialProfile socialProfile = (SocialProfile) args[0];
				userName = socialProfile.getEmail();
				userType = UserType.CANDIDATE;
			} else if (jp.getSignature().getName()
					.equalsIgnoreCase("testNotification")) {
				Object[] args = jp.getArgs();
				MettlNotification mettlNotification = (MettlNotification) args[0];
				userName = mettlNotification.getEmail();
				userType = UserType.CANDIDATE;
				action = action + mettlNotification.getEventType();
			} else if (jp.getSignature().getName()
					.equalsIgnoreCase("updateAppliedJobStatus")) {
				Object[] args = jp.getArgs();
				targetUser=args[0].toString();
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			} else if (jp.getSignature().getName()
					.equalsIgnoreCase("createAudioInterview")) {
				Object[] args = jp.getArgs();
				AudioInterview audioInterview = (AudioInterview) args[1];
				audioInterview = candidateDao
						.getAudioInterviewBySid(audioInterview.getCallsid());
				currentUser = audioInterview.getCandidateProfile().getUser();
				userName = currentUser.getUserName();
				userType = UserType.CANDIDATE;
			}else if(methodName.equalsIgnoreCase("loginAsAdmin")){
				Object[] args = jp.getArgs();
				User user=(User)args[0];
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
				targetUser=user.getUserName();
			}else if(methodName.equalsIgnoreCase("updateCaseScoreAndGrade")
					||methodName.equalsIgnoreCase("updateVoiceScoreAndGrade")
					||methodName.equalsIgnoreCase("updateEmailSendStatusForAppliedJob")
					||methodName.equalsIgnoreCase("unlockAppliedJob")){
				targetUser=jp.getArgs()[0].toString();
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			}else if (currentUser != null) {
				userName = currentUser.getUserName();
				userType = currentUser.getUserType();
			}
			if (currentUser != null) {
				auditLog.setAction(action);
				auditLog.setUserName(userName);
				auditLog.setUserType(userType);
				auditLog.setIpAddress(auditingUsernameIp);
				auditLog.setUserAgent(userAgent);
				auditLog.setTargetUser(targetUser);
				userDao.createAuditLog(auditLog);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
