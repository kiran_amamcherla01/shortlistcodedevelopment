package com.firstcut.Exception;

public class UserAlreadyExistException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -303667873437595564L;

	public UserAlreadyExistException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserAlreadyExistException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public UserAlreadyExistException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public UserAlreadyExistException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public UserAlreadyExistException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
