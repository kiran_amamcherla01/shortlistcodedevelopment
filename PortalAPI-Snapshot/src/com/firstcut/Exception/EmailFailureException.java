package com.firstcut.Exception;

public class EmailFailureException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2809855468204024594L;

	public EmailFailureException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmailFailureException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

	public EmailFailureException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public EmailFailureException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public EmailFailureException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}
