package  com.firstcut.Exception;

import org.hibernate.HibernateException;

public class DataBaseException extends HibernateException {

	private static final long serialVersionUID = 5567874767993560081L;


	public DataBaseException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataBaseException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataBaseException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
