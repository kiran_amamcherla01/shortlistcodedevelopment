package com.firstcut.Exception;

public class PasswordMismatchException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3207515332730545800L;

	public PasswordMismatchException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PasswordMismatchException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public PasswordMismatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PasswordMismatchException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PasswordMismatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
