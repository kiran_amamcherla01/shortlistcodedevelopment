package com.firstcut.Exception;

public class InActiveUserException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5402273533077119186L;

	public InActiveUserException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InActiveUserException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InActiveUserException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InActiveUserException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InActiveUserException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
