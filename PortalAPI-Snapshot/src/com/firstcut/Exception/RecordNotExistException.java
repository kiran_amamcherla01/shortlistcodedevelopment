package com.firstcut.Exception;

public class RecordNotExistException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7648631297121919357L;

	public RecordNotExistException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RecordNotExistException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public RecordNotExistException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public RecordNotExistException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public RecordNotExistException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
