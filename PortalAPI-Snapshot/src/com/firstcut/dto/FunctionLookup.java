package com.firstcut.dto;

public class FunctionLookup {

	private Integer id;
	private String master;
	private String naukri;
	private String monster;
	private String linkedin;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	public String getNaukri() {
		return naukri;
	}

	public void setNaukri(String naukri) {
		this.naukri = naukri;
	}

	public String getMonster() {
		return monster;
	}

	public void setMonster(String monster) {
		this.monster = monster;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

}
