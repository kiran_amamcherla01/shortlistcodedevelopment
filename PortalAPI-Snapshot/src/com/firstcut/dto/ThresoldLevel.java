package com.firstcut.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.firstcut.enums.AppliedType;

public class ThresoldLevel implements Serializable {
	private static final long serialVersionUID = -4694187486415257876L;
	private int id;
	private double naukriDB;
	private double naukri;
	private double monsterDB;
	private double monster;
	private double iimjobs;
	private double linkedin;
	private double other;
	@JsonBackReference("thresoldLevels")
	private JobOpening jobOpening;
	private AppliedType type;

	@JsonIgnore
	public JobOpening getJobOpening() {
		return jobOpening;
	}

	@JsonProperty
	public void setJobOpening(JobOpening jobOpening) {
		this.jobOpening = jobOpening;
	}

	public double getIimjobs() {
		return iimjobs;
	}

	public void setIimjobs(double iimjobs) {
		this.iimjobs = iimjobs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public AppliedType getType() {
		return type;
	}

	public void setType(AppliedType type) {
		this.type = type;
	}

	public double getNaukriDB() {
		return naukriDB;
	}

	public void setNaukriDB(double naukriDB) {
		this.naukriDB = naukriDB;
	}

	public double getNaukri() {
		return naukri;
	}

	public void setNaukri(double naukri) {
		this.naukri = naukri;
	}

	public double getMonsterDB() {
		return monsterDB;
	}

	public void setMonsterDB(double monsterDB) {
		this.monsterDB = monsterDB;
	}

	public double getMonster() {
		return monster;
	}

	public void setMonster(double monster) {
		this.monster = monster;
	}

	public double getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(double linkedin) {
		this.linkedin = linkedin;
	}

	public double getOther() {
		return other;
	}

	public void setOther(double other) {
		this.other = other;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((jobOpening.getId() == null) ? 0 : jobOpening.getId()
						.hashCode());
		long temp;
		temp = Double.doubleToLongBits(linkedin);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(monster);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(monsterDB);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(naukri);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(naukriDB);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(other);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ThresoldLevel other = (ThresoldLevel) obj;
		if (jobOpening.getId() == null) {
			if (other.jobOpening.getId() != null)
				return false;
		} else if (!jobOpening.getId().equals(other.jobOpening.getId()))
			return false;
		if (Double.doubleToLongBits(linkedin) != Double
				.doubleToLongBits(other.linkedin))
			return false;
		if (Double.doubleToLongBits(monster) != Double
				.doubleToLongBits(other.monster))
			return false;
		if (Double.doubleToLongBits(monsterDB) != Double
				.doubleToLongBits(other.monsterDB))
			return false;
		if (Double.doubleToLongBits(naukri) != Double
				.doubleToLongBits(other.naukri))
			return false;
		if (Double.doubleToLongBits(naukriDB) != Double
				.doubleToLongBits(other.naukriDB))
			return false;
		if (Double.doubleToLongBits(this.other) != Double
				.doubleToLongBits(other.other))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
