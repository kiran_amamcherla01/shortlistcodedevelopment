/**
 * 
 */
package com.firstcut.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author MindBowser-Android
 * 
 */
public class JobScoringRelation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7198638913063333721L;
	private int id;
	@JsonBackReference("jobscoringRelations")
	private JobOpening jobOpening;
	private JobScoringCriteria jobScoringCriteria;
	private String stage;
	private boolean forgiveNr = Boolean.FALSE;
	private boolean exclusion = Boolean.FALSE;
	private int weightage = 1;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public JobScoringRelation() {
	}

	@JsonIgnore
	public JobOpening getJobOpening() {
		return jobOpening;
	}

	@JsonProperty
	public void setJobOpening(JobOpening jobOpening) {
		this.jobOpening = jobOpening;
	}

	public JobScoringCriteria getJobScoringCriteria() {
		return jobScoringCriteria;
	}

	public void setJobScoringCriteria(JobScoringCriteria jobScoringCriteria) {
		this.jobScoringCriteria = jobScoringCriteria;
	}

	public boolean isForgiveNr() {
		return forgiveNr;
	}

	public void setForgiveNr(boolean forgiveNr) {
		this.forgiveNr = forgiveNr;
	}

	public boolean isExclusion() {
		return exclusion;
	}

	public void setExclusion(boolean exclusion) {
		this.exclusion = exclusion;
	}

	public int getWeightage() {
		return weightage;
	}

	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}

	/**
	 * @return the stage
	 */
	public String getStage() {
		return stage;
	}

	/**
	 * @param stage
	 *            the stage to set
	 */
	public void setStage(String stage) {
		this.stage = stage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime
				* result
				+ ((jobOpening.getId() == null) ? 0 : jobOpening.getId()
						.hashCode());
		result = prime
				* result
				+ ((jobScoringCriteria.getId() == null) ? 0
						: jobScoringCriteria.getId().hashCode());
		result = prime * result + ((stage == null) ? 0 : stage.hashCode());
		result = prime * result + weightage;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobScoringRelation other = (JobScoringRelation) obj;
		if (id != other.id)
			return false;
		if (jobOpening.getId() == null) {
			if (other.jobOpening.getId() != null)
				return false;
		} else if (!jobOpening.getId().equals(other.jobOpening.getId()))
			return false;
		if (jobScoringCriteria.getId() == null) {
			if (other.jobScoringCriteria.getId() != null)
				return false;
		} else if (!jobScoringCriteria.getId().equals(
				other.jobScoringCriteria.getId()))
			return false;
		if (stage == null) {
			if (other.stage != null)
				return false;
		} else if (!stage.equals(other.stage))
			return false;
		if (weightage != other.weightage)
			return false;
		return true;
	}

}
