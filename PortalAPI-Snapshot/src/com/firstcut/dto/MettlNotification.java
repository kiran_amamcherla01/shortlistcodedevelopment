package com.firstcut.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MettlNotification {

	@JsonProperty("EVENT_TYPE")
	private String eventType;
	@JsonProperty("invitation_key")
	private String invitationKey;
	// @JsonProperty("assessment_id")
	// private String assessmentId;
	@JsonProperty("candidate_instance_id")
	private String candidateInstanceId;
	@JsonProperty("context_data")
	private String contextData;
	@JsonProperty("timestamp_GMT")
	private String timestampGMT;
	@JsonProperty("notification_url")
	private String notificationUrl;
	@JsonProperty("name")
	private String name;
	@JsonProperty("source_app")
	private String sourceApp;
	@JsonProperty("email")
	private String email;
	@JsonProperty("finish_mode")
	private String finishMode;
	@JsonProperty("percentile")
	private String percentile;
	@JsonProperty("start_time_GMT")
	private String startTimeGMT;
	@JsonProperty("end_time_GMT")
	private String endTimeGMT;
	@JsonProperty("marks_scored")
	private String marksScored;
	@JsonProperty("max_marks")
	private String maxMarks;
	@JsonProperty("attempt_time")
	private String attemptTime;
	@JsonProperty("assessment_name")
	private String assessmentName;
	@JsonProperty("client_id")
	private String clientId;
	@JsonProperty("schedule_title")
	private String scheduleTitle;
	@JsonProperty("start_time")
	private String startTime;
	@JsonProperty("end_time")
	private String endTime;

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getInvitationKey() {
		return invitationKey;
	}

	public void setInvitationKey(String invitationKey) {
		this.invitationKey = invitationKey;
	}

	public String getCandidateInstanceId() {
		return candidateInstanceId;
	}

	public void setCandidateInstanceId(String candidateInstanceId) {
		this.candidateInstanceId = candidateInstanceId;
	}

	public String getContextData() {
		return contextData;
	}

	public void setContextData(String contextData) {
		this.contextData = contextData;
	}

	public String getTimestampGMT() {
		return timestampGMT;
	}

	public void setTimestampGMT(String timestampGMT) {
		this.timestampGMT = timestampGMT;
	}

	public String getNotificationUrl() {
		return notificationUrl;
	}

	public void setNotificationUrl(String notificationUrl) {
		this.notificationUrl = notificationUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFinishMode() {
		return finishMode;
	}

	public void setFinishMode(String finishMode) {
		this.finishMode = finishMode;
	}

	public String getPercentile() {
		return percentile;
	}

	public void setPercentile(String percentile) {
		this.percentile = percentile;
	}

	public String getStartTimeGMT() {
		return startTimeGMT;
	}

	public void setStartTimeGMT(String startTimeGMT) {
		this.startTimeGMT = startTimeGMT;
	}

	public String getEndTimeGMT() {
		return endTimeGMT;
	}

	public void setEndTimeGMT(String endTimeGMT) {
		this.endTimeGMT = endTimeGMT;
	}

	public String getMarksScored() {
		return marksScored;
	}

	public void setMarksScored(String marksScored) {
		this.marksScored = marksScored;
	}

	public String getMaxMarks() {
		return maxMarks;
	}

	public void setMaxMarks(String maxMarks) {
		this.maxMarks = maxMarks;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getScheduleTitle() {
		return scheduleTitle;
	}

	public void setScheduleTitle(String scheduleTitle) {
		this.scheduleTitle = scheduleTitle;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAttemptTime() {
		return attemptTime;
	}

	public void setAttemptTime(String attemptTime) {
		this.attemptTime = attemptTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	// @JsonCreator
	// public static MettlNotification of(
	// @JsonProperty("assessmentId") final String fieldName1,
	// @JsonProperty("assessment_id") final String fieldName2) {
	// final String name = fieldName1 != null ? fieldName1 : fieldName2;
	// return of(name);
	// }
	//
	// // Programmatic version that you can use from code
	// public static MettlNotification of(
	// @JsonProperty("assessmentId") final String fieldName) {
	// return new MettlNotification(fieldName);
	// }
	//
	// private MettlNotification(final String fieldName) {
	// this.assessmentId = fieldName;
	// }

	public MettlNotification() {
		super();
	}

	@Override
	public String toString() {
		return "MettlNotification [eventType=" + eventType + ", invitationKey="
				+ invitationKey + ", candidateInstanceId="
				+ candidateInstanceId + ", contextData=" + contextData
				+ ", timestampGMT=" + timestampGMT + ", notificationUrl="
				+ notificationUrl + ", name=" + name + ", sourceApp="
				+ sourceApp + ", email=" + email + ", finishMode=" + finishMode
				+ ", percentile=" + percentile + ", startTimeGMT="
				+ startTimeGMT + ", endTimeGMT=" + endTimeGMT
				+ ", marksScored=" + marksScored + ", maxMarks=" + maxMarks
				+ ", attemptTime=" + attemptTime + ", assessmentName="
				+ assessmentName + ", clientId=" + clientId
				+ ", scheduleTitle=" + scheduleTitle + ", startTime="
				+ startTime + ", endTime=" + endTime + "]";
	}

}
