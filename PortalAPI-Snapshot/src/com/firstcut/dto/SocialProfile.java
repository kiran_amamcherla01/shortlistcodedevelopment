package com.firstcut.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SocialProfile {

	@JsonIgnore
	private User user;
	private int userId;
	private String firstName;
	private String lastName;
	private String email;
	private String linkedInId;
	private String facebookId;
	private String facebookProfileUrl;
	private String linkedInProfileUrl;
	private String gender;
	private String country;
	private String socialType;

	public String getSocialType() {
		return socialType;
	}

	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLinkedInId() {
		return linkedInId;
	}

	public void setLinkedInId(String linkedInId) {
		this.linkedInId = linkedInId;
	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getFacebookProfileUrl() {
		return facebookProfileUrl;
	}

	public void setFacebookProfileUrl(String facebookProfileUrl) {
		this.facebookProfileUrl = facebookProfileUrl;
	}

	public String getLinkedInProfileUrl() {
		return linkedInProfileUrl;
	}

	public void setLinkedInProfileUrl(String linkedInProfileUrl) {
		this.linkedInProfileUrl = linkedInProfileUrl;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
