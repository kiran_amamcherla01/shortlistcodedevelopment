package com.firstcut.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.firstcut.constant.Constant;

// default package
// Generated Aug 19, 2015 1:55:18 PM by Hibernate Tools 4.0.0

/**
 * CandidateScoring generated by hbm2java
 */
public class CandidateScoring implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6551540224482974405L;
	private Integer id;
	private String exclusionReason;
	private boolean exclude = Boolean.FALSE;
	private String stage;
	@JsonBackReference("scorings")
	private CandidateProfile candidateProfile;
	@JsonIgnoreProperties({ "requirements", "companyProfile",
			"jobscoringRelations", "thresoldLevels", "jobTitle", "jobSummary",
			"jobDetail" })
	private JobOpening jobOpening;
	private double finalScore;
	private double questionnaireScore;
	private double psycScore;
	private double voiceScore;
	private String voiceGrade;
	private double workcaseScore;
	private String workcaseGrade;
	private double shortcaseScore;
	private double skillsScore;
	private double companyToExcludeScore;
	private double companyToIncludeScore;
	private double currentJobTitleScore;
	private double locationScore;
	private double educationScore;
	private double workExperienceScore;
	private double annualSalaryScore;
	private double sectorScore;
	private double sectorSemScore;
	private double sectorScore_1_1;
	private double domainScore;
	private double domainSemScore;
	private double domainScore_1_1;
	private double topUniversitiesScore;
	private double noticePeriodScore;
	private double currentlyEmployedScore;
	private double mobileVerifiedScore;
	private double actonomyScore;

	public double getActonomyScore() {
		return actonomyScore;
	}

	public void setActonomyScore(double actonomyScore) {
		this.actonomyScore = actonomyScore;
	}

	public double getQuestionnaireScore() {
		return questionnaireScore;
	}
	
	public double calculateQuestionnaireScore() {
		return companyToExcludeScore + companyToIncludeScore
				+ currentJobTitleScore + locationScore + educationScore
				+ workExperienceScore + annualSalaryScore + sectorScore
				+ domainScore + topUniversitiesScore + noticePeriodScore
				+ currentlyEmployedScore + skillsScore;
	}

	public double getSectorSemScore() {
		return sectorSemScore;
	}

	public void setSectorSemScore(double sectorSemScore) {
		this.sectorSemScore = sectorSemScore;
	}

	public double getDomainSemScore() {
		return domainSemScore;
	}

	public void setDomainSemScore(double domainSemScore) {
		this.domainSemScore = domainSemScore;
	}

	public double getSectorScore_1_1() {
		return sectorScore_1_1;
	}

	public void setSectorScore_1_1(double sectorScore_1_1) {
		this.sectorScore_1_1 = sectorScore_1_1;
	}

	public double getDomainScore_1_1() {
		return domainScore_1_1;
	}

	public void setDomainScore_1_1(double domainScore_1_1) {
		this.domainScore_1_1 = domainScore_1_1;
	}

	public double getShortcaseScore() {
		return shortcaseScore;
	}

	public void setShortcaseScore(double shortcaseScore) {
		this.shortcaseScore = shortcaseScore;
	}

	public double getPsycScore() {
		return psycScore;
	}

	public void setPsycScore(double psycScore) {
		this.psycScore = psycScore;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	/**
	 * @param excludeReason
	 *            the excludeReason to set
	 */
	public void setExclusionReason(String excludeReason) {
		this.exclusionReason = excludeReason;
	}

	/**
	 * @param excludeReason
	 *            the excludeReason to set
	 */
	public void appendExclusionReason(String excludeReason) {
		if (this.exclusionReason == null) {
			setExclusionReason(excludeReason);
		} else {
			this.exclusionReason = this.exclusionReason
					+ Constant.DELIMITER_DOUBLE_PIPE + excludeReason;
		}
	}

	public String getVoiceGrade() {
		return voiceGrade;
	}

	public void setVoiceGrade(String voiceGrade) {
		this.voiceGrade = voiceGrade;
	}

	public double getFinalScore() {
		return finalScore;
	}

	public void setFinalScore(double finalScore) {
		this.finalScore = finalScore;
	}

	public void setQuestionnaireScore(double questionnaireScore) {
		this.questionnaireScore = questionnaireScore;
	}

	public double getVoiceScore() {
		return voiceScore;
	}

	public void setVoiceScore(double voiceScore) {
		this.voiceScore = voiceScore;
	}

	public double getWorkcaseScore() {
		return workcaseScore;
	}

	public void setWorkcaseScore(double workcaseScore) {
		this.workcaseScore = workcaseScore;
	}

	public String getWorkcaseGrade() {
		return workcaseGrade;
	}

	public void setWorkcaseGrade(String workcaseGrade) {
		this.workcaseGrade = workcaseGrade;
	}

	public double getSkillsScore() {
		return skillsScore;
	}

	public void setSkillsScore(double skillsScore) {
		this.skillsScore = skillsScore;
	}

	public double getCompanyToExcludeScore() {
		return companyToExcludeScore;
	}

	public void setCompanyToExcludeScore(double companyToExcludeScore) {
		this.companyToExcludeScore = companyToExcludeScore;
	}

	public double getCompanyToIncludeScore() {
		return companyToIncludeScore;
	}

	public void setCompanyToIncludeScore(double companyToIncludeScore) {
		this.companyToIncludeScore = companyToIncludeScore;
	}

	public double getCurrentJobTitleScore() {
		return currentJobTitleScore;
	}

	public void setCurrentJobTitleScore(double currentJobTitleScore) {
		this.currentJobTitleScore = currentJobTitleScore;
	}

	public double getLocationScore() {
		return locationScore;
	}

	public void setLocationScore(double locationScore) {
		this.locationScore = locationScore;
	}

	public double getEducationScore() {
		return educationScore;
	}

	public void setEducationScore(double educationScore) {
		this.educationScore = educationScore;
	}

	public double getWorkExperienceScore() {
		return workExperienceScore;
	}

	public void setWorkExperienceScore(double workExperienceScore) {
		this.workExperienceScore = workExperienceScore;
	}

	public double getAnnualSalaryScore() {
		return annualSalaryScore;
	}

	public void setAnnualSalaryScore(double annualSalaryScore) {
		this.annualSalaryScore = annualSalaryScore;
	}

	public double getSectorScore() {
		return sectorScore;
	}

	public void setSectorScore(double sectorScore) {
		this.sectorScore = sectorScore;
	}

	public double getDomainScore() {
		return domainScore;
	}

	public void setDomainScore(double domainScore) {
		this.domainScore = domainScore;
	}

	public double getTopUniversitiesScore() {
		return topUniversitiesScore;
	}

	public void setTopUniversitiesScore(double topUniversitiesScore) {
		this.topUniversitiesScore = topUniversitiesScore;
	}

	public double getNoticePeriodScore() {
		return noticePeriodScore;
	}

	public void setNoticePeriodScore(double noticePeriodScore) {
		this.noticePeriodScore = noticePeriodScore;
	}

	public double getCurrentlyEmployedScore() {
		return currentlyEmployedScore;
	}

	public void setCurrentlyEmployedScore(double currentlyEmployedScore) {
		this.currentlyEmployedScore = currentlyEmployedScore;
	}

	public double getMobileVerifiedScore() {
		return mobileVerifiedScore;
	}

	public void setMobileVerifiedScore(double mobileVerifiedScore) {
		this.mobileVerifiedScore = mobileVerifiedScore;
	}

	public CandidateScoring() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CandidateProfile getCandidateProfile() {
		return candidateProfile;
	}

	public void setCandidateProfile(CandidateProfile candidateProfile) {
		this.candidateProfile = candidateProfile;
	}

	public JobOpening getJobOpening() {
		return jobOpening;
	}

	public void setJobOpening(JobOpening jobOpening) {
		this.jobOpening = jobOpening;
	}

	public boolean isExclude() {
		return exclude;
	}

	public void setExclude(boolean exclude) {
		this.exclude = exclude;
	}

	public String getExclusionReason() {
		return exclusionReason;
	}

	@Override
	public String toString() {
		return "CandidateScoring [id=" + id + ", exclude=" + exclude
				+ ", skillsScore=" + skillsScore + ", companyToExcludeScore="
				+ companyToExcludeScore + ", companyToIncludeScore="
				+ companyToIncludeScore + ", currentJobTitleScore="
				+ currentJobTitleScore + ", locationScore=" + locationScore
				+ ", educationScore=" + educationScore
				+ ", workExperienceScore=" + workExperienceScore
				+ ", annualSalaryScore=" + annualSalaryScore + ", sectorScore="
				+ sectorScore + ", domainScore=" + domainScore
				+ ", topUniversitiesScore=" + topUniversitiesScore
				+ ", noticePeriodScore=" + noticePeriodScore
				+ ", currentlyEmployedScore=" + currentlyEmployedScore
				+ ", mobileVerifiedScore=" + mobileVerifiedScore + "]";
	}
}
