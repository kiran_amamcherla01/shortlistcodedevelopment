package com.firstcut.dto;

public class CityLookup {

	private Integer id;
	private String city;
	private String state;
	private String zone;
	private String type;
	private Integer population;
	private String populationClass;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPopulation() {
		return population;
	}

	public void setPopulation(Integer population) {
		this.population = population;
	}

	public String getPopulationClass() {
		return populationClass;
	}

	public void setPopulationClass(String populationClass) {
		this.populationClass = populationClass;
	}

}
