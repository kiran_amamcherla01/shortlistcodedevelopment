package com.firstcut.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class CaseQuestion {

	private Integer id;
	private String questionText;
	private String answer;
	private String etype;
	private String qtype;
	private String optionValues;
	private int wordCount;
	@JsonIgnoreProperties({ "hiringManager", "companyProfile", "jobTitle",
			"jobSummary", "jobDetail", "keywords", "voiceWeightage",
			"caseWeightage", "CVScoreWeightage", "psyRecmWeightage",
			"psyEmpWeightage", "psyOwnWeightage", "submissionDeadline",
			"jobApplieds", "jobscoringRelations", "thresoldLevels",
			"companyFit", "requirements", "noticePeriod", "jobFit",
			"startupFit", "recommendations", "showSalary", "sendEmail",
			"jobOpeningStatus", "proprietaryThresold", "cvscoreWeightage",
			"exotelNumber", "education2", "minimumExp", "maximumExp",
			"minAnnualSalary", "maxAnnualSalary", "opsMinimumExp",
			"opsMaximumExp", "opsMinAnnualSalary", "opsMaxAnnualSalary",
			"jobLocation", "nearJobLocation", "sector", "domain",
			"postedOn", "education", "type", "voiceQuestions", "dailyBudget",
			"createDate", "modifyDate" })
	private JobOpening jobOpening;
	@JsonIgnore
	private Date createDate;
	private String createdBy;
	@JsonIgnore
	private Date modifyDate;
	private String modifyBy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public int getWordCount() {
		return wordCount;
	}

	public void setWordCount(int wordCount) {
		this.wordCount = wordCount;
	}

	public String getQuestionText() {
		return questionText;
	}

	public JobOpening getJobOpening() {
		return jobOpening;
	}

	public void setJobOpening(JobOpening jobOpening) {
		this.jobOpening = jobOpening;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getEtype() {
		return etype;
	}

	public void setEtype(String etype) {
		this.etype = etype;
	}

	public String getQtype() {
		return qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	/**
	 * @return the optionValues
	 */
	public String getOptionValues() {
		return optionValues;
	}

	/**
	 * @param optionValues
	 *            the optionValues to set
	 */
	public void setOptionValues(String optionValues) {
		this.optionValues = optionValues;
	}

}
