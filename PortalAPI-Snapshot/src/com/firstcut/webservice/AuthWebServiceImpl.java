package com.firstcut.webservice;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.Exception.InActiveUserException;
import com.firstcut.Exception.PasswordMismatchException;
import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.audit.Auditable;
import com.firstcut.audit.AuditingActionType;
import com.firstcut.constant.Constant;
import com.firstcut.dto.SocialProfile;
import com.firstcut.dto.User;
import com.firstcut.enums.UserType;
import com.firstcut.model.ResponseModel;
import com.firstcut.model.UserResponse;
import com.firstcut.service.AuthService;
import com.firstcut.service.UserService;
import com.firstcut.util.ResourceManager;
import com.notify.exception.EmailSendFailureException;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@GZIP
@Transactional
public class AuthWebServiceImpl {

	@Autowired
	private AuthService authService;
	@Autowired
	private UserService userService;
	@Autowired
	private Mapper dozerMapper;
	@Context
	private HttpServletRequest request;

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	@POST
	@Auditable(actionType = AuditingActionType.USER_LOGIN)
	public ResponseModel login(User user) throws PasswordMismatchException, InActiveUserException, ServiceException {
		String userName = user.getUserName();
		String password = user.getPassword();
		if (userName == null || userName.isEmpty()) {
			throw new ServiceException("UserName can't be blank!");
		}
		if (password == null || password.isEmpty()) {
			throw new ServiceException("Password can't be blank!");
		}
		user = authService.authenticate(user.getUserName(), user.getPassword(),
				user.getUserType());
		UserResponse userResponse = dozerMapper.map(user, UserResponse.class);
		return ResponseModel.build(userResponse);
	}

	@POST
	@Path("/loginasadmin")
	@Auditable(actionType = AuditingActionType.ADMIN_AS_LOGIN_USER)
	public ResponseModel loginAsAdmin(User user)
			throws InActiveUserException, ServiceException {
		if (user.getUserName() == null || user.getUserName().isEmpty()) {
			throw new ServiceException("UserName can't be blank!");
		}
		user = userService.getUserByUserNameAndType(user.getUserName(), user.getUserType());
		user.getAuthToken();
		UserResponse userResponse = dozerMapper.map(user, UserResponse.class);
		return ResponseModel.build(userResponse);
	}

	@POST
	@Path("/sociallogin")
	@Auditable(actionType = AuditingActionType.SOCIAL_LOGIN)
	public ResponseModel socialLoginCallBack(SocialProfile socialProfile)
			throws ServiceException {
		User user = authService.saveSocialProfiles(socialProfile);
		UserResponse userResponse = dozerMapper.map(user, UserResponse.class);
		return ResponseModel.build(userResponse);
	}

	@PUT
	@Path("/logout")
	@Auditable(actionType = AuditingActionType.USER_LOGOUT)
	public ResponseModel logout(@HeaderParam("authToken") String token) {
		authService.logout(token);
		return ResponseModel.build();
	}

	@PUT
	@Path("/forgotpassword")
	public ResponseModel forgotPassword(@QueryParam("email") String email,
			@QueryParam("userType") UserType userType)
			throws RecordNotExistException, InActiveUserException,
			EmailSendFailureException {
		authService.forgotPassword(email, userType);
		ResponseModel responseModel = ResponseModel.build();
		responseModel.setMessage(ResourceManager
				.getProperty(Constant.FORGOT_PASSWORD_SENT_MESSAGE));
		return responseModel;
	}

	/*
	 * @GET
	 * 
	 * @Path("/validateuser") public Response validateEmail(@QueryParam("token")
	 * String confirmToken) throws RecordNotExistException,
	 * InActiveUserException { String basePath =
	 * ResourceManager.getProperty(Constant.BASE_URL); User user =
	 * userService.getUserByConfirmToken(confirmToken); return
	 * Response.seeOther(URI.create(basePath + "/portal")).build(); }
	 */
}
