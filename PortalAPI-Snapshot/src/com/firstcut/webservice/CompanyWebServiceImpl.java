package com.firstcut.webservice;

import static com.firstcut.constant.Constant.APP_ENV;
import static com.firstcut.constant.Constant.MEDIA_FOLDER;
import static com.firstcut.constant.Constant.PRODUCTION;
import static com.firstcut.constant.Constant.USER;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.CompanyProfile;
import com.firstcut.dto.HiringManager;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.Media;
import com.firstcut.dto.User;
import com.firstcut.index.IndexCreatorException;
import com.firstcut.index.JobIndexCreator;
import com.firstcut.model.AppliedCandidatesResponse;
import com.firstcut.model.CompanyProfileResponse;
import com.firstcut.model.ResponseModel;
import com.firstcut.model.SearchFilterModel;
import com.firstcut.model.UserResponse;
import com.firstcut.s3.util.AmazonClientUtil;
import com.firstcut.service.CompanyService;
import com.firstcut.util.DozerHelper;
import com.firstcut.util.FileOperations;
import com.firstcut.util.ResourceManager;

@Component
 @Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@GZIP
@Transactional
public class CompanyWebServiceImpl {

	@Autowired
	private CompanyService companyService;
	@Autowired
	private Mapper dozerMapper;

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	/**
	 * Retrieve all the companies
	 * 
	 * @return
	 */
	@GET
	public ResponseModel getAllCompanyProfile(
			@QueryParam("filter") String filter) {
		List<CompanyProfile> candidaProfiles = companyService
				.getAllCompany(filter);
		return ResponseModel.build(candidaProfiles);
	}

	/**
	 * Delete all the companies
	 * 
	 * @return
	 */
	@DELETE
	public ResponseModel deleteAllCompanyProfile() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Retrieve the precise company profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}")
	public ResponseModel getCompanyProfile(@PathParam("companyId") int companyId)
			throws ServiceException {
		CompanyProfile companyProfile = companyService.getCompany(companyId);
		return ResponseModel.build(companyProfile);
	}

	/**
	 * Retrieve the precise company profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyname}/view")
	public ResponseModel getCompanyProfile(
			@QueryParam("companyname") String companyName)
			throws ServiceException {
		CompanyProfile companyProfile = companyService
				.getCompanyByName(companyName);
		return ResponseModel.build(companyProfile);
	}

	/**
	 * create a company profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	public ResponseModel createCompanyProfile(CompanyProfile companyProfile)
			throws ServiceException {
		CompanyProfile company = companyService.createCompany(companyProfile);
		CompanyProfileResponse companyProfileResponse = dozerMapper.map(
				company, CompanyProfileResponse.class);
		return ResponseModel.build(companyProfileResponse);
	}

	/**
	 * update a company profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{companyId}")
	public ResponseModel updateCompanyProfile(
			@PathParam("companyId") int companyId, CompanyProfile companyProfile)
			throws ServiceException {
		CompanyProfile company = companyService.updateCompany(companyId,
				companyProfile);
		CompanyProfileResponse companyProfileResponse = dozerMapper.map(
				company, CompanyProfileResponse.class);
		return ResponseModel.build(companyProfileResponse);
	}

	// ------------------------------------------Media------------------------------------

	/**
	 * get all media for specific company
	 * 
	 * @return
	 */
	@GET
	@Path("/{companyId}/medias")
	public ResponseModel getAllMedia(@PathParam("companyId") int companyId) {
		List<Media> medias = companyService.getAllMedia(companyId);
		return ResponseModel.build(medias);
	}

	/**
	 * delete all media for specific company
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{companyId}/medias")
	public ResponseModel deleteAllMedia(@PathParam("companyId") int companyId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * get specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}/medias/{mediaId}")
	public ResponseModel getMedia(@PathParam("companyId") int companyId,
			@PathParam("mediaId") int mediaId) throws ServiceException {
		Media media = companyService.getMedia(companyId, mediaId);
		return ResponseModel.build(media);
	}

	/**
	 * create specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{companyId}/medias")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public ResponseModel createMedia(@PathParam("companyId") int companyId,
			MultipartBody body) throws ServiceException {
		List<Media> medias = new ArrayList<Media>();
		List<Attachment> allAttachments = body.getAllAttachments();
		for (Attachment attachment : allAttachments) {
			try {
				Media media = new Media();
				String type = attachment.getContentType().getType();
				String imageName = attachment.getDataHandler().getDataSource()
						.getName();
				InputStream inputStream = attachment.getDataHandler()
						.getDataSource().getInputStream();
				String filePath = FileOperations.readMutilPartFile(inputStream,
						imageName, type);
				String key = MEDIA_FOLDER + File.separator + imageName;
				String mediaURL = AmazonClientUtil.getInstance()
						.uploadMultipart(filePath, key);
				media.setUrl(mediaURL);
				media.setMediaType(type);
				medias.add(media);
				companyService.createMedia(companyId, medias);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
		return ResponseModel.build(medias);
	}

	/**
	 * delete specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@DELETE
	@Path("/{companyId}/medias/{mediaId}")
	public ResponseModel deleteMedia(@PathParam("companyId") int companyId,
			@PathParam("mediaId") int mediaId) throws ServiceException {
		companyService.deleteMedia(companyId, mediaId);
		return ResponseModel.build();
	}

	// --------Hiring Manager------------

	/**
	 * get all manager for specific company
	 * 
	 * @return
	 */
	@GET
	@Path("/{companyId}/managers")
	public ResponseModel getAllManager(@PathParam("companyId") int companyId) {
		List<HiringManager> managers = companyService.getAllManager(companyId);
		return ResponseModel.build(managers);
	}

	/**
	 * delete all manager for specific company
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{companyId}/managers")
	public ResponseModel deleteAllManager(@PathParam("companyId") int companyId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * get specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}/managers/{managerId}")
	public ResponseModel getManager(@PathParam("companyId") int companyId,
			@PathParam("managerId") int managerId) throws ServiceException {
		HiringManager manager = companyService.getManager(companyId, managerId);
		return ResponseModel.build(manager);
	}

	/**
	 * create specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{companyId}/managers")
	public ResponseModel createManager(@PathParam("companyId") int companyId,
			HiringManager manager) throws ServiceException {
		manager = companyService.createManager(companyId, manager);
		User user = manager.getUser();
		UserResponse userResponse = dozerMapper.map(user, UserResponse.class);
		return ResponseModel.build(userResponse);
	}

	/**
	 * update specific media for specific comapany
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{companyId}/managers/{managerId}")
	public ResponseModel updateManager(@PathParam("companyId") int companyId,
			@PathParam("managerId") int mediaId, HiringManager manager)
			throws ServiceException {
		throw new UnsupportedOperationException();
	}

	/**
	 * delete specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@DELETE
	@Path("/{companyId}/managers/{managerId}")
	public ResponseModel deleteManager(@PathParam("companyId") int companyId,
			@PathParam("managerId") int managerId) throws ServiceException {
		throw new UnsupportedOperationException();
	}

	// --------Job Openings------------

	/**
	 * get all job opening for a company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}/jobs")
	public ResponseModel getAllJobOpenings(
			@Context HttpServletRequest request,
			@PathParam("companyId") int companyId,
			@QueryParam("jobOpeningStatus") @DefaultValue("OPEN") String jobOpeningStatus)
			throws ServiceException {
		List<JobOpening> jobOpenings = null;
		User user = getCurrentUser();
		if (user.isAdmin()) {
			jobOpenings = companyService.getAllJobOpenings(jobOpeningStatus);
		} else {
			jobOpenings = companyService.getAllJobOpenings(companyId,
					jobOpeningStatus);
		}
		return ResponseModel.build(jobOpenings);
	}

	/**
	 * get all job opening for a company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/alljobs")
	public ResponseModel getAllJobOpenings(
			@QueryParam("jobOpeningStatus") @DefaultValue("OPEN") String jobOpeningStatus)
			throws ServiceException {
		List<JobOpening> jobOpenings = companyService
				.getAllJobOpenings(jobOpeningStatus);
		return ResponseModel.build(jobOpenings);
	}

	/**
	 * delete all job opening for a company
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{companyId}/jobs")
	public ResponseModel deleteAllJobOpenings(
			@PathParam("companyId") int companyId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * get list of s that have applied for any jobs in a company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}/candidates")
	public ResponseModel getCompanySpecificApplieds(
			@PathParam("companyId") int companyId,
			@QueryParam("jobOpeningStatus") @DefaultValue("OPEN") String jobOpeningStatus,
			@QueryParam("jobStatus") String jobStatus) throws ServiceException {
		List<JobOpening> jobOpenings = companyService
				.getAllAppliedCandidatesByCompanyId(companyId,
						jobOpeningStatus, jobStatus);
		List<AppliedCandidatesResponse> applieds = DozerHelper.map(dozerMapper,
				jobOpenings, AppliedCandidatesResponse.class);
		return ResponseModel.build(applieds);
	}

	/**
	 * get the job details based on JobId & company Id to view the job to
	 * 
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyname}/jobs/{jobOpeningId}/view")
	public ResponseModel getJobDetails(
			@PathParam("companyname") String companyname,
			@PathParam("jobOpeningId") int jobOpeningId)
			throws ServiceException {
		JobOpening jobOpening = companyService.getJobOpening(companyname,
				jobOpeningId);
		return ResponseModel.build(jobOpening);
	}

	/**
	 * get specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{companyId}/jobs/{jobOpeningId}")
	public ResponseModel getJobOpening(@PathParam("companyId") int companyId,
			@PathParam("jobOpeningId") int jobOpeningId)
			throws ServiceException {
		JobOpening jobOpening = companyService.getJobOpening(companyId,
				jobOpeningId);
		return ResponseModel.build(jobOpening);
	}

	/**
	 * get specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 * @throws IndexCreatorException
	 */
	@POST
	@Path("/{companyId}/jobs")
	public ResponseModel createJobOpening(
			@PathParam("companyId") int companyId, JobOpening jobOpening)
			throws ServiceException, IndexCreatorException {
		jobOpening.setHiringManager(getCurrentUser().getHiringManager());
		jobOpening = companyService.createJobOpening(companyId, jobOpening);
		if (PRODUCTION.equalsIgnoreCase(ResourceManager.getProperty(APP_ENV)))
			new JobIndexCreator().createIndex(jobOpening);
		return ResponseModel.build(jobOpening);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 * @throws IndexCreatorException
	 */
	@PUT
	@Path("/{companyId}/jobs/{jobOpeningId}")
	public ResponseModel updatejobOpening(
			@PathParam("companyId") int companyId,
			@PathParam("jobOpeningId") int jobOpeningId, JobOpening jobOpening)
			throws ServiceException, IndexCreatorException {
		jobOpening.setId(jobOpeningId);
		jobOpening = companyService.updateJobOpening(companyId, jobOpening);
		if (PRODUCTION.equalsIgnoreCase(ResourceManager.getProperty(APP_ENV)))
			new JobIndexCreator().createIndex(jobOpening);
		return ResponseModel.build(jobOpening);
	}

	/**
	 * delete specific media for specific company
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@DELETE
	@Path("/{companyId}/jobs/{jobOpeningId}")
	public ResponseModel deleteJobOpening(
			@PathParam("companyId") int companyId,
			@PathParam("jobOpeningId") int jobOpeningId)
			throws ServiceException {
		throw new UnsupportedOperationException();
	}

	private User getCurrentUser() {
		ServletRequestAttributes t = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		HttpServletRequest request = t.getRequest();
		return (User) request.getAttribute(USER);
	}
	
	/**
	 * Retrieve the precise company profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/search/jobs")
	public ResponseModel getJobsForCompanyId(@QueryParam("companyIds") String companyId)
			throws ServiceException {
		SearchFilterModel model=new SearchFilterModel();
		if(companyId!=null && !companyId.isEmpty()){
			Set<String> companyIds=StringUtils.commaDelimitedListToSet(companyId);
			model.setCompanyIds(companyIds);
		}
		List<CompanyProfile> companyProfiles = companyService.getJobsByCompanies(model);
		return ResponseModel.build(companyProfiles);
	}

}
