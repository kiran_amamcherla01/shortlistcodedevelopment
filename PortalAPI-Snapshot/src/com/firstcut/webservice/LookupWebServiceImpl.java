package com.firstcut.webservice;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.model.ResponseModel;
import com.firstcut.service.CityLookupService;
import com.firstcut.service.EducationLookupService;
import com.firstcut.service.FunctionLookupService;
import com.firstcut.service.IndustryLookupService;
import com.firstcut.service.LookupService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@GZIP
@Transactional
@Api(value = "/lookup", description = "Lookup Service details")
public class LookupWebServiceImpl {

	@Autowired
	private CityLookupService cityLookupService;
	@Autowired
	private FunctionLookupService functionLookupService;
	@Autowired
	private IndustryLookupService industryLookupService;
	@Autowired
	private EducationLookupService educationLookupService;
	@Autowired
	private LookupService lookupService;

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	@GET
	@Path("/zones")
	public ResponseModel getAllZones() {
		List<String> zones = cityLookupService.getAllZones();
		Collections.sort(zones);
		return ResponseModel.build(zones);
	}

	@GET
	@Path("/states")
	public ResponseModel getStatesByZone(@QueryParam("zone") @DefaultValue("") String zone) {
		List<String> states = cityLookupService.getStatesByZone(zone);
		Collections.sort(states);
		return ResponseModel.build(states);
	}

	@GET
	@Path("/cities")
	@ApiOperation(value = "Get cities by state", response = ResponseModel.class)
	public ResponseModel getCitiesByState(@QueryParam("state") @DefaultValue("") String state) {
		List<String> cities = cityLookupService.getCitiesByState(state);
		Collections.sort(cities);
		return ResponseModel.build(cities);
	}

	@GET
	@Path("/cities")
	public ResponseModel getCitiesByZone(@QueryParam("zone") @DefaultValue("") String zone) {
		List<String> cities = cityLookupService.getCitiesByZone(zone);
		Collections.sort(cities);
		return ResponseModel.build(cities);
	}

	@GET
	@Path("/functions")
	@ApiOperation(value = "Get functions", response = ResponseModel.class)
	public ResponseModel getFunctions() {
		List<String> functions = functionLookupService.getAllFunctions();
		Collections.sort(functions);
		return ResponseModel.build(functions);
	}

	/**
	 * This method is used to get all industries
	 * 
	 * @param request
	 * @param response
	 * @return list of industry
	 */
	@GET
	@Path("/industries")
	public ResponseModel getIndustries() {
		List<String> industries = industryLookupService.getAllIndustries();
		Collections.sort(industries);
		return ResponseModel.build(industries);
	}

	/**
	 * This method is used to get all industries
	 * 
	 * @param request
	 * @param response
	 * @return list of industry
	 */
	@GET
	@Path("/topuniversity")
	public ResponseModel getTopUniversity() {
		List<String> allTopUniversities = lookupService.getAllTopUniversities();
		Collections.sort(allTopUniversities);
		return ResponseModel.build(allTopUniversities);
	}

	/**
	 * This method is used to get all industries
	 * 
	 * @param request
	 * @param response
	 * @return list of industry
	 */
	@GET
	@Path("/educations")
	public ResponseModel getAllEducations(@QueryParam("type") @DefaultValue("") String type) {
		List<String> educations = educationLookupService
				.getAllEducationByType(type);
		Collections.sort(educations);
		return ResponseModel.build(educations);
	}
}
