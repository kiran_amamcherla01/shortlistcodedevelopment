package com.firstcut.webservice;

import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.firstcut.Exception.ServiceException;
import com.firstcut.dto.CaseQuestion;
import com.firstcut.dto.JobOpening;
import com.firstcut.dto.JobScoringCriteria;
import com.firstcut.dto.JobScoringRelation;
import com.firstcut.model.AppliedCandidatesResponse;
import com.firstcut.model.ResponseModel;
import com.firstcut.model.SearchFilterModel;
import com.firstcut.service.JobService;
import com.firstcut.util.DozerHelper;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Transactional
@GZIP
public class JobWebServiceImpl {

	@Autowired
	private JobService jobService;

	@Autowired
	private Mapper dozerMapper;
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{jobOpeningId}/case")
	public ResponseModel getCaseQuestion(
			@PathParam("jobOpeningId") int jobOpeningId,
			@QueryParam("qtype") String qtype) throws ServiceException {
		List<CaseQuestion> caseQuestions = jobService.getCaseQuestions(
				jobOpeningId, qtype);
		return ResponseModel.build(caseQuestions);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{jobOpeningId}/case")
	public ResponseModel createCaseQuestion(
			@PathParam("jobOpeningId") int jobOpeningId,
			List<CaseQuestion> caseQuestions) throws ServiceException {
		jobService.createCaseQuestions(jobOpeningId, caseQuestions);
		return ResponseModel.build(caseQuestions);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{jobOpeningId}/case")
	public ResponseModel updateCaseQuestion(
			@PathParam("jobOpeningId") int jobOpeningId,
			CaseQuestion caseQuestions) throws ServiceException {
		jobService.updateCaseQuestion(jobOpeningId, caseQuestions);
		return ResponseModel.build(caseQuestions);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@DELETE
	@Path("/{jobOpeningId}/case")
	public ResponseModel deleteCaseQuestion(
			@PathParam("jobOpeningId") int jobOpeningId,
			CaseQuestion caseQuestion) throws ServiceException {
		jobService.deleteCaseQuestion(caseQuestion);
		return ResponseModel.build();
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/allscoringcriteria")
	public ResponseModel getAllScoringCriteria() throws ServiceException {
		List<JobScoringCriteria> scoringCriteria = jobService
				.getDefaultScoringCriteria();
		return ResponseModel.build(scoringCriteria);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{jobId}/scoringcriteria")
	public ResponseModel getScoringCriteria(@PathParam("jobId") int jobId)
			throws ServiceException {
		List<JobScoringRelation> scoringCriteria = jobService
				.getJobScoringCriteria(jobId);
		return ResponseModel.build(scoringCriteria);
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{jobId}/scoringcriteria")
	public ResponseModel updateScoringCriteria(@PathParam("jobId") int jobId,
			Set<JobScoringRelation> scoringCriteria) throws ServiceException {
		scoringCriteria = jobService.updateJobScoringCriteria(jobId,
				scoringCriteria);
		return ResponseModel.build();
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{jobId}/scoringcriteria")
	public ResponseModel createScoringCriteria(@PathParam("jobId") int jobId,
			Set<JobScoringRelation> scoringCriteria) throws ServiceException {
		scoringCriteria = jobService.createJobScoringCriteria(jobId,
				scoringCriteria);
		return ResponseModel.build();
	}

	/**
	 * update specific media for specific
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{jobId}/finalscore")
	public ResponseModel calculateFinalScore(@PathParam("jobId") int jobId)
			throws ServiceException {
		jobService.calculateFinalScore(jobId);
		return ResponseModel.build();
	}

	/**
	 * get list of candidates that have applied for particular job.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{jobOpeningId}/candidates")
	public ResponseModel getJobsSpecificApplieds(
			@PathParam("companyId") int companyId,
			@PathParam("jobOpeningId") int jobId,
			@QueryParam("jobStatus") String jobStatus) throws ServiceException {
		Set<String> jobStatuses = StringUtils
				.commaDelimitedListToSet(jobStatus);
		JobOpening jobOpening = jobService.getAllAppliedCandidatesByJobId(
				jobId, jobStatuses);
		AppliedCandidatesResponse applieds = dozerMapper.map(jobOpening,
				AppliedCandidatesResponse.class);
		return ResponseModel.build(applieds);
	}

	/**
	 * get list of s that have applied for particular job.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/search")
	public ResponseModel getCandidatesForSearchCritera(
			@QueryParam("companyId") String companyId,
			@QueryParam("jobId") String jobIds,
			@QueryParam("jobStatus") String jobStatus,
			@QueryParam("skipDeadline") boolean skipDeadlineOver,
			@QueryParam("searchParam") String searchParam,
			@QueryParam("searchText") String searchText)
			throws ServiceException {
		logger.info(companyId +":"+jobIds+":"+jobStatus+""+searchParam+""+searchText);
		SearchFilterModel filters = new SearchFilterModel();
		if (jobStatus != null && !jobStatus.isEmpty()) {
			Set<String> jobStatuses = StringUtils
					.commaDelimitedListToSet(jobStatus);
			filters.setJobStatuses(jobStatuses);
		}
		if (jobIds != null && !jobIds.isEmpty()) {
			Set<String> jobids = StringUtils.commaDelimitedListToSet(jobIds);
			filters.setJobIds(jobids);
		}else if(companyId!=null && !companyId.isEmpty()){
			Set<String> companyIds = StringUtils.commaDelimitedListToSet(companyId);
			filters.setCompanyIds(companyIds);
		}
		filters.setSearchParam(searchParam);
		filters.setSearchText(searchText);
		filters.setSkipDeadlineOver(skipDeadlineOver);
		Set<JobOpening> jobOpenings = jobService
				.getAllAppliedCandidatesForSearchCriteria(filters);
		List<AppliedCandidatesResponse> applieds = DozerHelper.map(dozerMapper,
				jobOpenings, AppliedCandidatesResponse.class);
		return ResponseModel.build(applieds);
	}

}
