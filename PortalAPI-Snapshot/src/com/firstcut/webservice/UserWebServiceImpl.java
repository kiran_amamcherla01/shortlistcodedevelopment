package com.firstcut.webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.firstcut.dto.ContactUs;
import com.firstcut.model.ResponseModel;
import com.firstcut.service.UserService;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@GZIP
public class UserWebServiceImpl {

	@Autowired
	private UserService userService;

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	// @GET
	// public ResponseModel getUser(@HeaderParam("authToken") String token) {
	// User user = userService.getUserByAuthToken(token);
	// return ResponseModel.build(user);
	// }
	//
	// @GET
	// @Path("/roles")
	// public ResponseModel getUserRoles(@HeaderParam("authToken") String token)
	// {
	// User user = userService.getUserByAuthToken(token);
	// return ResponseModel.build(user.getUserRole());
	// }
	//
	// @POST
	// public ResponseModel createUser(User user) throws
	// UserAlreadyExistException {
	// user = userService.createUser(user);
	// return ResponseModel.build(user);
	// }

	// @PUT
	// public ResponseModel updateUser(@HeaderParam("authToken") String token,
	// User user) {
	// user = userService.updateUser(user);
	// return ResponseModel.build(user);
	// }
	//
	// @DELETE
	// public ResponseModel deleteUser(@HeaderParam("authToken") String token) {
	// userService.deleteUser(token);
	// return ResponseModel.build(Status.NO_CONTENT.getReasonPhrase());
	// }

	@POST
	@Path("/contactus")
	public ResponseModel createContactUS(ContactUs contactUs) {
		userService.createConatctUsDetails(contactUs);
		return ResponseModel.build();
	}
}
