package com.firstcut.webservice;

import static com.firstcut.constant.Constant.METTL_BASE_URL;
import static com.firstcut.constant.Constant.METTL_PRIVATE_KEY;
import static com.firstcut.constant.Constant.METTL_PUBLIC_KEY;
import static com.firstcut.constant.Constant.METTL_SOURCE_APP_NAME;
import static com.firstcut.constant.Constant.METTL_TEST_ACCESS_KEY;
import static com.firstcut.constant.Constant.METTL_TEST_NAME;
import static com.firstcut.constant.Constant.METTL_VERSION;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.audit.Auditable;
import com.firstcut.audit.AuditingActionType;
import com.firstcut.constant.Constant;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.MettlNotification;
import com.firstcut.dto.Psychometric;
import com.firstcut.model.ResponseModel;
import com.firstcut.service.CandidateService;
import com.firstcut.service.UserService;
import com.firstcut.util.ResourceManager;
import com.mettl.apisClient.ApiClient;
import com.mettl.model.mettlApis.v1.ApiCreateSchedule;
import com.mettl.model.mettlApis.v1.ApiRegisteredCandidate;
import com.mettl.model.mettlApis.v1.ApiScheduleAssessmentOccurrenceType;
import com.mettl.model.mettlApis.v1.ApiScheduleTestCandidateStatusDetails;
import com.mettl.model.mettlApis.v1.responses.ApiCreateSchedulesResponse;
import com.mettl.model.mettlApis.v1.responses.ApiRegisterCandidatesResponse;
import com.mettl.model.mettlApis.v1.responses.ApiScheduleTestCandidateStatusResponse;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@GZIP
@Transactional
public class MettlWebServiceImpl {
	@Autowired
	private UserService userService;
	@Autowired
	private CandidateService candidateService;
	private static Logger logger = Logger.getLogger(MettlWebServiceImpl.class);

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response
				.ok()
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS,
						"POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*")
				.build();
	}

	//assessemnt:62275
	@POST
	@Path(value = "assessments/{assessment-id}/schedule")
	public ResponseModel createScheduleForAnAssessment(
			@PathParam(value = "assessment-id") int assessmentId)
			throws Exception {
		String baseURL = ResourceManager.getProperty(Constant.BASE_URL);
		ApiCreateSchedule schedule = ApiCreateSchedule.buildOpenForAll(
				assessmentId, ApiScheduleAssessmentOccurrenceType.AlwaysOn);
		// schedule.setAssessmentId(assessmentId);
		// schedule.setScheduleType(ApiScheduleAssessmentOccurrenceType.AlwaysOn);
		schedule.setName(METTL_TEST_NAME);
		schedule.setSourceApp(METTL_SOURCE_APP_NAME);
		schedule.setExitRedirectionUrl(baseURL
				+ "/portal/testcomplete.html");
		schedule.setTestFinishNotificationUrl(baseURL
				+ "/PortalAPI/api/mettl/test-notification");
		schedule.setTestStartNotificationUrl(baseURL
				+ "/PortalAPI/api/mettl/test-notification");
		schedule.setTestGradedNotificationUrl(baseURL
				+ "/PortalAPI/api/mettl/test-notification");
		ApiCreateSchedulesResponse scheduleResponse = getApiClient()
				.createAssessmentSchedules(assessmentId, schedule);
		String accessURL = scheduleResponse.getCreatedSchedule().getAccessUrl();
		return ResponseModel.build(accessURL);
	}

	//  https://tests.mettl.com/authenticateKey/9434c53
	@POST
	@Path(value = "schedule/{candidateId}")
	public ResponseModel registerCandidateForTest(
			@PathParam(value = "candidateId") int candidateId) throws Exception {
		CandidateProfile candidateProfile = candidateService
				.getCandidate(candidateId);
		List<Map<String, String>> registrationDetailsList = new ArrayList<Map<String, String>>();
		Map<String, String> candidate = new HashMap<String, String>();
		candidate.put("Email Address", candidateProfile.getEmail());
		candidate.put("First Name", candidateProfile.getFullName());
		candidate.put("Last Name", candidateProfile.getFullName());
		registrationDetailsList.add(candidate);
		ApiRegisterCandidatesResponse registerCandidatesForTest = getApiClient()
				.registerCandidatesForTest(METTL_TEST_ACCESS_KEY,
						registrationDetailsList);
		List<ApiRegisteredCandidate> registrationStatus = registerCandidatesForTest
				.getRegistrationStatus();
		ApiRegisteredCandidate apiRegisteredCandidate = registrationStatus
				.get(0);
		String url = apiRegisteredCandidate.getUrl();
		if (url == null) {
			return ResponseModel.build().setMessage(
					apiRegisteredCandidate.getMessage());
		}
		return ResponseModel.build(url);
	}

	@GET
	@Path(value = "report/{email}")
	public ResponseModel getPdfReport(@PathParam(value = "email") String email)
			throws Exception {
		logger.info("Fetching Mettl report for email="+email);
		ApiScheduleTestCandidateStatusResponse statusOfCandidateOfScheduleWithEmailId = getApiClient()
				.getStatusOfCandidateOfScheduleWithEmailId(
						METTL_TEST_ACCESS_KEY, email);
		ApiScheduleTestCandidateStatusDetails testStatus = statusOfCandidateOfScheduleWithEmailId
				.getCandidate().getTestStatus();
		return ResponseModel.build(testStatus);
	}

	@POST
	@Path(value = "/test-notification")
	@Auditable(actionType = AuditingActionType.PERSONALITY_TEST)
	public void testNotification(MettlNotification mettlNotification) {
		if("finishTest".equalsIgnoreCase(mettlNotification.getEventType())){
			CandidateProfile candidateByEmail = candidateService
					.getCandidateByEmail(mettlNotification.getEmail());
			Psychometric psychometric=new Psychometric(candidateByEmail, mettlNotification.getEmail(),mettlNotification.getName());
			candidateByEmail.getPsychometrics().add(psychometric);
		}
	}

	private ApiClient getApiClient() {
		return ApiClient.getInstance(Integer.valueOf(-1), METTL_PUBLIC_KEY,
				METTL_PRIVATE_KEY, METTL_VERSION, METTL_BASE_URL);
	}
}
