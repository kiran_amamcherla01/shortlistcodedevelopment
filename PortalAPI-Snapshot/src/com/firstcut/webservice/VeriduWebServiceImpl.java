package com.firstcut.webservice;

import static com.firstcut.constant.Constant.API_KEY;
import static com.firstcut.constant.Constant.SECRET_KEY;
import static com.firstcut.constant.Constant.VERIDU_VERSION;

import java.net.URI;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.firstcut.constant.Constant;
import com.firstcut.dto.User;
import com.firstcut.dto.VeriduUser;
import com.firstcut.model.ResponseModel;
import com.firstcut.service.VeridulService;
import com.firstcut.util.ResourceManager;
import com.veridu.sdk.Request;
import com.veridu.sdk.Session;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON ,MediaType.APPLICATION_XML})
public class VeriduWebServiceImpl {

	@Autowired
	private VeridulService veriduService;
	private Logger logger = Logger.getLogger(this.getClass().getName());

	@GET
	public ResponseModel createSession() {
		Request veriduRequest = new Request(API_KEY, SECRET_KEY);
		veriduRequest.setVersion(VERIDU_VERSION);
		Session veriduSession = new Session(veriduRequest);
		JSONObject createSession = veriduSession.createSession(false);
		return ResponseModel.build(createSession);
	}

	@GET
	@Path("socialsso")
	public Response veriduSsoCallback(@BeanParam VeriduUser veriduUser) {
		String basePath = ResourceManager.getProperty(Constant.BASE_URL);
		logger.info(veriduUser.toString());
		if (veriduUser.getVeriduError() != null) {
			logger.debug(veriduUser.getVeriduError());
			return Response.temporaryRedirect(URI.create(basePath)).build();
		}
		User user = veriduService.saveVeriduUser(veriduUser);
		return Response.seeOther(URI.create(basePath + "landing.html")).entity(user).build();
	}

}
