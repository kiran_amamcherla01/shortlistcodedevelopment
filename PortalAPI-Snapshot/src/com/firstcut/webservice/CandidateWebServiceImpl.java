package com.firstcut.webservice;

import static com.firstcut.constant.Constant.APP_ENV;
import static com.firstcut.constant.Constant.PRODUCTION;

import java.util.List;
import java.util.Set;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.cxf.annotations.GZIP;
import org.apache.cxf.rs.security.cors.CorsHeaderConstants;
import org.apache.cxf.rs.security.cors.LocalPreflight;
import org.apache.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.firstcut.Exception.RecordNotExistException;
import com.firstcut.Exception.ServiceException;
import com.firstcut.audit.Auditable;
import com.firstcut.audit.AuditingActionType;
import com.firstcut.constant.Constant;
import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.CandidateScoring;
import com.firstcut.dto.CaseAnswer;
import com.firstcut.dto.EducationSummary;
import com.firstcut.dto.JobApplied;
import com.firstcut.dto.JobHistory;
import com.firstcut.dto.User;
import com.firstcut.enums.AppliedJobStatus;
import com.firstcut.enums.EmailBlockStatus;
import com.firstcut.index.CandidateIndexCreator;
import com.firstcut.index.IndexCreatorException;
import com.firstcut.model.AssessmentStatusResponse;
import com.firstcut.model.CandidateProfileResponse;
import com.firstcut.model.JobAppliedResponse;
import com.firstcut.model.ResponseModel;
import com.firstcut.model.UserResponse;
import com.firstcut.service.CandidateService;
import com.firstcut.service.UserService;
import com.firstcut.util.DozerHelper;
import com.firstcut.util.ResourceManager;
import com.notify.exception.EmailSendFailureException;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

@Component
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@GZIP
@Transactional
@Api(value = "/candidates", description = "Candidate Details Service")
public class CandidateWebServiceImpl {

	@HeaderParam("Auth-Token")
	String token;
	@Autowired
	private CandidateService candidateService;
	@Autowired
	private UserService userService;
	@Autowired
	private Mapper dozerMapper;

	private static Logger logger = Logger.getLogger(CandidateWebServiceImpl.class);

	// This method will do a preflight check itself
	@OPTIONS
	@LocalPreflight
	public Response options() {
		return Response.ok().header(CorsHeaderConstants.HEADER_AC_ALLOW_METHODS, "POST,DELETE,PUT,GET")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_CREDENTIALS, "true")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_HEADERS,
						"Authorization,Content-Type,Accept,Auth-Token,Accept-Encoding")
				.header(CorsHeaderConstants.HEADER_AC_ALLOW_ORIGIN, "*").build();
	}

	// ------------------------------------------candidate------------------------------

	/**
	 * Retrieve all the candidates
	 * 
	 * @return
	 */
	@GET
	public ResponseModel getAllCandidate(@QueryParam("filter") String filter) {
		List<CandidateProfile> candidaProfiles = candidateService.getAllCandidate();
		return ResponseModel.build(candidaProfiles);
	}

	/**
	 * Delete all the candidates
	 * 
	 * @return
	 */
	@DELETE
	public ResponseModel deleteAllCandidate() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Retrieve the precise candidate profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{candidateId}")
	public ResponseModel getCandidate(@PathParam("candidateId") int candidateId) throws ServiceException {
		CandidateProfile candidate = candidateService.getCandidate(candidateId);
		// CandidateProfileResponse candidateProfileResponse = dozerMapper.map(
		// candidate, CandidateProfileResponse.class);
		return ResponseModel.build(candidate);
	}

	/**
	 * create a candidates profile
	 * 
	 * @return
	 * @throws ServiceException
	 * @throws EmailSendFailureException
	 */
	@POST
	@Auditable(actionType = AuditingActionType.CREATE_CANDIDATE)
	public ResponseModel createCandidate(CandidateProfile candidateProfile)
			throws ServiceException, EmailSendFailureException {
		CandidateProfile createCandidate = candidateService.createCandidate(candidateProfile);
		User user = createCandidate.getUser();
		UserResponse userResponse = dozerMapper.map(user, UserResponse.class);
		return ResponseModel.build(userResponse);
	}

	/**
	 * update a candidate
	 * 
	 * @return
	 * @throws ServiceException
	 * @throws IndexCreatorException
	 */
	@PUT
	@Path("/{candidateId}")
	public ResponseModel updateCandidate(@PathParam("candidateId") int candidateId, CandidateProfile candidateProfile)
			throws ServiceException, IndexCreatorException {
		CandidateProfile candidate = candidateService.updateCandidate(candidateId, candidateProfile);
		CandidateProfileResponse candidateProfileResponse = dozerMapper.map(candidate, CandidateProfileResponse.class);
		if (PRODUCTION.equalsIgnoreCase(ResourceManager.getProperty(APP_ENV)))
			new CandidateIndexCreator().createIndex(candidate);
		logger.info("In updateCandidate end CandidateWebService end");
		return ResponseModel.build(candidateProfileResponse);
	}

	/**
	 * delete a candidate
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}")
	public ResponseModel deleteCandidate(@PathParam("candidateId") int candidateId) {
		throw new UnsupportedOperationException();
	}

	// -----------------------Job History------------

	/**
	 * get all JobHistory for specific candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/jobhistories")
	public ResponseModel getAllJobHistory(@PathParam("candidateId") int candidateId) {
		List<JobHistory> jobs = candidateService.getAllJobHistory(candidateId);
		return ResponseModel.build(jobs);
	}

	/**
	 * delete all JobHistory for specific candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/jobhistories")
	public ResponseModel deleteAllJobHistory(@PathParam("candidateId") int candidateId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * get specific JobHistory for specific candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{candidateId}/jobhistories/{jobId}")
	public ResponseModel getJobHistory(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId)
			throws ServiceException {
		JobHistory job = candidateService.getJobHistory(candidateId, jobId);
		return ResponseModel.build(job);
	}

	/**
	 * create JobHistory for specific candidate.
	 * 
	 * @return
	 */
	@POST
	@Path("/{candidateId}/jobhistories/")
	public ResponseModel createJobHistory(@PathParam("candidateId") int candidateId, Set<JobHistory> jobHistory) {
		jobHistory = candidateService.createJobHistory(candidateId, jobHistory);
		return ResponseModel.build(jobHistory);
	}

	/**
	 * update JobHistory for specific candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{candidateId}/jobhistories/{jobId}")
	public ResponseModel updateJobHistory(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId,
			JobHistory jobHistory) throws ServiceException {
		throw new UnsupportedOperationException();
	}

	/**
	 * delete JobHistory for specific candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/jobhistories/{jobId}")
	public ResponseModel deleteJobHistory(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId) {
		throw new UnsupportedOperationException();
	}

	// ---------------------AppliedJobs--------------
	/**
	 * get all JobHistory for specific candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/appliedjobs")
	@ApiOperation(value = "Get Applied Jobs", response = ResponseModel.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name = "Authorization", value = "Baisc Authorization", required = true, dataType = "string", paramType = "header") })
	public ResponseModel getAllAppliedJob(@PathParam("candidateId") int candidateId,
			@QueryParam("count") int rowCount) {
		List<JobApplied> jobApplieds = candidateService.getAllAppliedJobs(candidateId, rowCount);
		List<JobAppliedResponse> jobAppliedResponses = DozerHelper.map(dozerMapper, jobApplieds,
				JobAppliedResponse.class);
		return ResponseModel.build(jobAppliedResponses);
	}

	/**
	 * Update the job status for specific applied job by candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{candidateId}/appliedjobs/{jobId}/changeStatus/{status}")
	@Auditable(actionType = AuditingActionType.JOB_APPLIED_STATUS_CHANGED)
	public ResponseModel updateAppliedJobStatus(@PathParam("candidateId") int candidateId,
			@PathParam("jobId") int jobId, @PathParam("status") AppliedJobStatus status,
			@QueryParam("sendmail") @DefaultValue("true") String sendMail) throws ServiceException {
		JobApplied updateAppliedJob;
		updateAppliedJob = candidateService.updateAppliedJobStatus(candidateId, jobId, status, sendMail);
		JobAppliedResponse jobAppliedResponses = dozerMapper.map(updateAppliedJob, JobAppliedResponse.class);
		return ResponseModel.build(jobAppliedResponses);
	}

	/**
	 * Update the job status for specific applied job by candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{candidateId}/appliedjobs/{jobId}")
	public ResponseModel updateAppliedJob(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId,
			JobApplied jobApplied) throws ServiceException {
		JobApplied updateAppliedJob;
		updateAppliedJob = candidateService.updateAppliedJob(candidateId, jobId, jobApplied);
		JobAppliedResponse jobAppliedResponses = dozerMapper.map(updateAppliedJob, JobAppliedResponse.class);
		return ResponseModel.build(jobAppliedResponses);
	}

	/**
	 * Update the job status for specific applied job by candidate.
	 * 
	 * @param candidateByEmail
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{email}/appliedjobs/{jobId}/unlock")
	@Auditable(actionType = AuditingActionType.UNLOCK_CANDIDATE)
	public ResponseModel unlockAppliedJob(@PathParam("email") String email, @PathParam("jobId") int jobId)
			throws ServiceException {
		CandidateScoring unlockAppliedJob = candidateService.unlockAppliedJob(email, jobId);
		return ResponseModel.build(unlockAppliedJob);
	}

	/**
	 * Update the email send status for specific applied job by candidate.
	 * 
	 * @param candidateByEmail
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@PUT
	@Path("/{email}/appliedjobs/{jobId}/emailblockstatus/{status}")
	@Auditable(actionType = AuditingActionType.EMAIL_BLOCK_STATUS_CHANGED)
	public ResponseModel updateEmailSendStatusForAppliedJob(@PathParam("email") String email,
			@PathParam("jobId") int jobId, @PathParam("status") String status) throws ServiceException {
		Boolean updateEmailSendStatus = candidateService.updateEmailSendStatusForAppliedJob(jobId, email, status);
		return ResponseModel.build(updateEmailSendStatus);
	}

	/**
	 * get applied job details by candidate.
	 * 
	 * @throws ServiceException
	 * 
	 * @returnw
	 */
	@GET
	@Path("/{candidateId}/appliedjobs/{jobId}")
	public ResponseModel getAppliedJob(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId)
			throws ServiceException {
		JobApplied jobApplied = candidateService.getAppliedJob(candidateId, jobId);
		JobAppliedResponse jobAppliedResponses = dozerMapper.map(jobApplied, JobAppliedResponse.class);
		return ResponseModel.build(jobAppliedResponses);
	}

	/**
	 * Applied job for specific candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{candidateId}/appliedjobs/")
	public ResponseModel createAppliedJob(@PathParam("candidateId") int candidateId, JobApplied jobApplied)
			throws ServiceException {
		jobApplied.setEmailBlockStatus(EmailBlockStatus.UNBLOCKED);
		jobApplied = candidateService.createAppliedJob(candidateId, jobApplied);
		JobAppliedResponse jobAppliedResponses = dozerMapper.map(jobApplied, JobAppliedResponse.class);
		return ResponseModel.build(jobAppliedResponses);
	}

	// ------------------------EducationSummary--------------------------------------

	/**
	 * get all educations for precise candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/educations")
	public ResponseModel getAllEducation(@PathParam("candidateId") int candidateId) {
		List<EducationSummary> educationSummary = candidateService.getAllEducation(candidateId);
		return ResponseModel.build(educationSummary);
	}

	/**
	 * get all educations for precise candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/educations")
	public ResponseModel deleteAllEducation(@PathParam("candidateId") int candidateId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * get specific educations for precise candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{candidateId}/educations/{eId}")
	public ResponseModel getEducation(@PathParam("candidateId") int candidateId, @PathParam("eId") int eId)
			throws ServiceException {
		EducationSummary educationSummary = candidateService.getEducation(candidateId, eId);
		return ResponseModel.build(educationSummary);
	}

	/**
	 * create education for specific candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{candidateId}/educations/")
	public ResponseModel createEducation(@PathParam("candidateId") int candidateId, @PathParam("eId") int eId,
			Set<EducationSummary> educationSummary) throws ServiceException {
		educationSummary = candidateService.createEducation(candidateId, educationSummary);
		return ResponseModel.build(educationSummary);
	}

	/**
	 * update educations for specific candidate.
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/educations/{eId}")
	public ResponseModel updateEducation(@PathParam("candidateId") int candidateId, @PathParam("eId") int eId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * delete specific educations for specific candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/educations/{eId}")
	public ResponseModel deleteEducation(@PathParam("candidateId") int candidateId, @PathParam("eId") int eId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * ------------------------------------------Audio Interviews
	 * ----------------------------
	 */

	/**
	 * get all audio interview for a candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/audios/view")
	public ResponseModel getAllAudioInterview(@PathParam("candidateId") int candidateId) {
		List<AudioInterview> audioInterviews = candidateService.getAllAudioInterview(candidateId);
		return ResponseModel.build(audioInterviews);
	}

	/**
	 * delete all audio interview for a candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/audios")
	public ResponseModel deleteAllAudio(@PathParam("candidateId") int candidateId) {
		throw new UnsupportedOperationException();
	}

	/**
	 * create audio interview for a candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/audios/")
	// @Auditable(actionType = AuditingActionType.VOICE_INTERVIEW_COMPLETED)
	public Response createAudioInterview(@PathParam("candidateId") int candidateId,
			@BeanParam AudioInterview audioInterview) {
		candidateService.createAudioInterview(audioInterview);
		return Response.ok().build();
	}

	/**
	 * delete precise audio interview for a candidate.
	 * 
	 * @return
	 */
	@DELETE
	@Path("/{candidateId}/audios/{audioId}")
	public ResponseModel deleteAudio(@PathParam("candidateId") int candidateId, @PathParam("audioId") int audioId) {
		throw new UnsupportedOperationException();
	}

	// ------------------------- CaseAnswer test -----------------------------

	/**
	 * get all CaseAnswer for precise candidate.
	 * 
	 * @return
	 */
	@GET
	@Path("/{candidateId}/job/{jobId}/workcases")
	public ResponseModel getAllWorkcase(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId,
			@QueryParam("qtype") String qtype) {
		List<CaseAnswer> caseAnswers = candidateService.getWorkcaseByJob(candidateId, jobId, qtype);
		return ResponseModel.build(caseAnswers);
	}

	/**
	 * get specific CaseAnswer test for precise candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@POST
	@Path("/{candidateId}/job/{jobId}/workcases")
	@Auditable(actionType = AuditingActionType.WORKCASE_COMPLETE)
	public ResponseModel createWorkCase(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId,
			List<CaseAnswer> caseAnswers) throws ServiceException {
		caseAnswers = candidateService.createWorkcase(candidateId, jobId, caseAnswers);
		return ResponseModel.build();
	}

	/**
	 * get specific CaseAnswer test for precise candidate.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{candidateId}/workcases/{caseId}")
	public ResponseModel getWorkCase(@PathParam("candidateId") int candidateId, @PathParam("caseId") int caseId)
			throws ServiceException {
		CaseAnswer caseAnswer = candidateService.getWorkcase(candidateId, caseId);
		return ResponseModel.build(caseAnswer);
	}

	/**
	 * get Assessments status for precise candidate.
	 * 
	 * @return
	 * @throws RecordNotExistException
	 */
	@GET
	@Path("/{candidateId}/jobs/{jobId}/assessments/status")
	public ResponseModel getAssessmentStatus(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId)
			throws RecordNotExistException {
		AssessmentStatusResponse statusResponse = candidateService.getAssessmentStatus(candidateId, jobId);
		return ResponseModel.build(statusResponse);
	}

	/**
	 * get candidate scoring
	 * 
	 * @return
	 * @throws IndexCreatorException
	 */
	@GET
	@Path("/{candidateId}/jobs/{jobId}/score/{stage}")
	public ResponseModel getCandidateScoring(@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId,
			@PathParam("stage") String stage) throws IndexCreatorException {
		CandidateScoring candidateScoring = null;
		if (Constant.PRESCREEN.equalsIgnoreCase(stage))
			candidateScoring = candidateService.getCandidatePreScoring(candidateId, jobId);
		else if (Constant.POSTSCREEN.equalsIgnoreCase(stage))
			candidateScoring = candidateService.getCandidatePostScoring(candidateId, jobId);
		return ResponseModel.build(candidateScoring);
	}

	/**
	 * update candidate scoring
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/jobs/{jobId}/score")
	public ResponseModel updateCandidateScoring(CandidateScoring candidateScoring,
			@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId) {
		candidateService.updateCandidateScoring(candidateScoring, candidateId, jobId);
		return ResponseModel.build(candidateScoring);
	}

	/**
	 * update candidate scoring
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/jobs/{jobId}/finalscore")
	public ResponseModel calculateFinalScore(CandidateScoring candidateScoring,
			@PathParam("candidateId") int candidateId, @PathParam("jobId") int jobId) {
		double finalScore = candidateService.calculateFinalScore(candidateId, jobId);
		return ResponseModel.build(finalScore);
	}

	/**
	 * Retrieve the precise candidate profile
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@GET
	@Path("/{jobId}/{candidateId}")
	public ResponseModel getCandidateDataForJob(@PathParam("jobId") int jobId,
			@PathParam("candidateId") int candidateId) throws ServiceException {
		CandidateProfile jobApplied = candidateService.getCandidateForJob(jobId, candidateId);
		// CandidateProfileResponse candidateProfileResponse = dozerMapper.map(
		// candidate, CandidateProfileResponse.class);
		return ResponseModel.build(jobApplied);
	}

	/**
	 * update candidate voice scoring
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/jobs/{jobId}/voice/{score}/{grade}")
	@Auditable(actionType = AuditingActionType.VOICE_SCORE_UPDATED)
	public ResponseModel updateVoiceScoreAndGrade(@PathParam("candidateId") int candidateId,
			@PathParam("score") double score, @PathParam("grade") String grade, @PathParam("jobId") int jobId)
			throws ServiceException {
		boolean scoreUpdated = candidateService.updateCandidateVoiceScoring(candidateId, jobId, score, grade);
		return ResponseModel.build(scoreUpdated);
	}

	/**
	 * update candidate voice scoring
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/jobs/{jobId}/case/{score}/{grade}")
	@Auditable(actionType = AuditingActionType.WORKCASE_SCORE_UPDATED)
	public ResponseModel updateCaseScoreAndGrade(@PathParam("candidateId") int candidateId,
			@PathParam("score") double score, @PathParam("grade") String grade, @PathParam("jobId") int jobId)
			throws ServiceException {
		boolean scoreUpdated = candidateService.updateCandidateCaseScoring(candidateId, jobId, score, grade);
		return ResponseModel.build(scoreUpdated);
	}

	/**
	 * update candidate voice scoring
	 * 
	 * @return
	 */
	@PUT
	@Path("/{candidateId}/updatename/{name}/{phone}")
	@Auditable(actionType = AuditingActionType.NAME_PHONE_UPDATED)
	public ResponseModel updateNameAndPhone(@PathParam("candidateId") int candidateId, @PathParam("name") String name,
			@PathParam("phone") String phone) throws ServiceException {
		boolean nameUpdated = candidateService.updateCandidateNameAndPhone(candidateId, name, phone);
		return ResponseModel.build(nameUpdated);
	}

}
