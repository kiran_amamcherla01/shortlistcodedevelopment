package com.firstcut.indexer;

import static com.firstcut.constant.Constant.APP_ENV;
import static com.firstcut.constant.Constant.PRODUCTION;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.firstcut.dao.CandidateDao;
import com.firstcut.dao.CompanyDao;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.firstcut.enums.JobOpeningStatus;
import com.firstcut.index.CandidateIndexCreator;
import com.firstcut.index.IndexCreatorException;
import com.firstcut.index.JobIndexCreator;
import com.firstcut.util.ResourceManager;

public class IndexCreator {

	@Autowired
	private static CandidateDao candidateDao;
	@Autowired
	private static CompanyDao companyDao;

	public static void main(String[] args) throws IndexCreatorException {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");
		candidateDao = (CandidateDao) context.getBean("candidateDao");
		companyDao = (CompanyDao) context.getBean("companyDao");
		new IndexCreator().generateAllJobIndex();
		new IndexCreator().generateAllCandiadteIndex();
	}

	public void generateAllJobIndex() throws IndexCreatorException {
		List<JobOpening> jobOpenings = companyDao
				.getAllJobOpenings(JobOpeningStatus.OPEN.toValue());
		for (JobOpening jobOpening : jobOpenings) {
			if (PRODUCTION.equalsIgnoreCase(ResourceManager
					.getProperty(APP_ENV)))
				new JobIndexCreator().createIndex(jobOpening);
		}
	}

	public void generateAllCandiadteIndex() throws IndexCreatorException {
		List<CandidateProfile> candidates = candidateDao.getAllCandidate();
		for (CandidateProfile candidate : candidates) {
			if (candidate.getProfileStatus().equalsIgnoreCase("complete")) {
				if (PRODUCTION.equalsIgnoreCase(ResourceManager
						.getProperty(APP_ENV)))
					new CandidateIndexCreator().createIndex(candidate);
			}
		}
	}

	public void generateCandiadteIndexById(int candidateid)
			throws IndexCreatorException {
		CandidateProfile candidate = candidateDao.getCandidateById(candidateid);
		if (candidate.getProfileStatus().equalsIgnoreCase("complete")) {
			if (PRODUCTION.equalsIgnoreCase(ResourceManager
					.getProperty(APP_ENV)))
				new CandidateIndexCreator().createIndex(candidate);
		}
	}
}
