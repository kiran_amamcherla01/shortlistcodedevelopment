package com.firstcut.model;

import java.util.ArrayList;
import java.util.List;

import com.firstcut.enums.EmailStatus;
import com.notify.model.NotificationStatusModel;

public class EmailStatusModel {

	private List<NotificationStatusModel> status = new ArrayList<>();
	private int jobId;
	private EmailStatus emailStatus;

	public EmailStatusModel(int integer, EmailStatus emailStatus) {
		this.jobId = integer;
		this.emailStatus = emailStatus;
	}

	public List<NotificationStatusModel> getStatus() {
		return status;
	}

	public void setStatus(List<NotificationStatusModel> status) {
		this.status = status;
	}

	public int getJobId() {
		return jobId;
	}

	public EmailStatus getEmailStatus() {
		return emailStatus;
	}

	@Override
	public String toString() {
		return "EmailStatusModel [status=" + status + ", jobId=" + jobId
				+ ", emailStatus=" + emailStatus + "]";
	}

}
