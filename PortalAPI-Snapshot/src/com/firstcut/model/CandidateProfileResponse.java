package com.firstcut.model;

// default package
// Generated Jul 15, 2015 7:20:39 PM by Hibernate Tools 4.0.0

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CandidateProfileResponse {

	private int userId;
	private String education;
	private Date dob;
	private String fullName;
	private String source;
	private String address;
	private String skills;
	private String email;
	private Boolean ownHouse;
	private String gender;
	private String phone;
	private String currentJobStatus;
	private String nationality;
	private String currency;
	private String profileUrl;
	@JsonIgnore
	private String resumeUrl;
	@JsonIgnore
	private String profilePicURL;
	private Double currentAnnualSalary;
	private Double expectedAnnualSalary;
	private Double totalExperience;
	private Integer yearsAtCurrentResidence;
	private String hobbies;
	private String languagesSpoken;
	private String interests;
	private String audioPin;
	private String targetIndustry;
	private String targetFunction;
	private String tagLine;
	private String maritalStatus;
	private Integer noticePeriod;
	private String relocationCities;
	private boolean verifiedMobile;
	private Date lastActive;
	private String culture;
	private String strengths;
	private String targetIndustry2;
	private String targetFunction2;
	private String profileStatus;
	private String currentJobTitle;
	private String currentCompany;

	
	public String getProfileUrl() {
		return profileUrl;
	}

	public void setProfileUrl(String profileUrl) {
		this.profileUrl = profileUrl;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getProfileStatus() {
		return profileStatus;
	}

	public void setProfileStatus(String profileStatus) {
		this.profileStatus = profileStatus;
	}

	@JsonProperty
	public String getProfilePicURL() {
		return profilePicURL;
	}

	@JsonIgnore
	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}

	public String getAudioPin() {
		return audioPin;
	}

	public void setAudioPin(String audioPin) {
		this.audioPin = audioPin;
	}

	public boolean isVerifiedMobile() {
		return verifiedMobile;
	}

	public void setVerifiedMobile(boolean verifiedMobile) {
		this.verifiedMobile = verifiedMobile;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Date getLastActive() {
		return lastActive;
	}

	public void setLastActive(Date lastActive) {
		this.lastActive = lastActive;
	}

	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}

	public String getStrengths() {
		return strengths;
	}

	public void setStrengths(String strengths) {
		this.strengths = strengths;
	}

	public String getTargetIndustry2() {
		return targetIndustry2;
	}

	public void setTargetIndustry2(String targetIndustry2) {
		this.targetIndustry2 = targetIndustry2;
	}

	public String getTargetFunction2() {
		return targetFunction2;
	}

	public void setTargetFunction2(String targetFunction2) {
		this.targetFunction2 = targetFunction2;
	}

	public CandidateProfileResponse() {
	}

	public CandidateProfileResponse(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLanguagesSpoken() {
		return languagesSpoken;
	}

	public void setLanguagesSpoken(String languagesSpoken) {
		this.languagesSpoken = languagesSpoken;
	}

	public Integer getNoticePeriod() {
		return noticePeriod;
	}

	public void setNoticePeriod(Integer noticePeriod) {
		this.noticePeriod = noticePeriod;
	}

	public String getRelocationCities() {
		return relocationCities;
	}

	public void setRelocationCities(String relocationCities) {
		this.relocationCities = relocationCities;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getSkills() {
		return this.skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getOwnHouse() {
		return this.ownHouse;
	}

	public void setOwnHouse(Boolean ownHouse) {
		this.ownHouse = ownHouse;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCurrentJobStatus() {
		return currentJobStatus;
	}

	public void setCurrentJobStatus(String currentJobStatus) {
		this.currentJobStatus = currentJobStatus;
	}

	public String getNationality() {
		return this.nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	@JsonProperty
	public String getResumeUrl() {
		return this.resumeUrl;
	}

	@JsonIgnore
	public void setResumeUrl(String resumeUrl) {
		this.resumeUrl = resumeUrl;
	}

	public Double getCurrentAnnualSalary() {
		return currentAnnualSalary;
	}

	public void setCurrentAnnualSalary(Double currentAnnualSalary) {
		this.currentAnnualSalary = currentAnnualSalary;
	}

	public Double getExpectedAnnualSalary() {
		return expectedAnnualSalary;
	}

	public void setExpectedAnnualSalary(Double expectedAnnualSalary) {
		this.expectedAnnualSalary = expectedAnnualSalary;
	}

	public Double getTotalExperience() {
		return totalExperience;
	}

	public void setTotalExperience(Double totalExperience) {
		this.totalExperience = totalExperience;
	}

	public Integer getYearsAtCurrentResidence() {
		return this.yearsAtCurrentResidence;
	}

	public void setYearsAtCurrentResidence(Integer yearsAtCurrentResidence) {
		this.yearsAtCurrentResidence = yearsAtCurrentResidence;
	}

	public String getHobbies() {
		return this.hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getInterests() {
		return this.interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getTargetIndustry() {
		return this.targetIndustry;
	}

	public void setTargetIndustry(String targetIndustry) {
		this.targetIndustry = targetIndustry;
	}

	public String getTargetFunction() {
		return this.targetFunction;
	}

	public void setTargetFunction(String targetFunction) {
		this.targetFunction = targetFunction;
	}

	public String getTagLine() {
		return this.tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getCurrentJobTitle() {
		return currentJobTitle;
	}

	public void setCurrentJobTitle(String currentJobTitle) {
		this.currentJobTitle = currentJobTitle;
	}

	public String getCurrentCompany() {
		return currentCompany;
	}

	public void setCurrentCompany(String currentCompany) {
		this.currentCompany = currentCompany;
	}

}
