package com.firstcut.model;


public class CandidateScoringResponse {
	private Integer id;
	private String exclusionReason;
	private boolean exclude = Boolean.FALSE;
	private String stage;
	private double finalScore;
	private double questionnaireScore;
	private double psycScore;
	private double voiceScore;
	private String voiceGrade;
	private double workcaseScore;
	private String workcaseGrade;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getExclusionReason() {
		return exclusionReason;
	}
	public void setExclusionReason(String exclusionReason) {
		this.exclusionReason = exclusionReason;
	}
	public boolean isExclude() {
		return exclude;
	}
	public void setExclude(boolean exclude) {
		this.exclude = exclude;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	public double getFinalScore() {
		return finalScore;
	}
	public void setFinalScore(double finalScore) {
		this.finalScore = finalScore;
	}
	public double getQuestionnaireScore() {
		return questionnaireScore;
	}
	public void setQuestionnaireScore(double questionnaireScore) {
		this.questionnaireScore = questionnaireScore;
	}
	public double getPsycScore() {
		return psycScore;
	}
	public void setPsycScore(double psycScore) {
		this.psycScore = psycScore;
	}
	public double getVoiceScore() {
		return voiceScore;
	}
	public void setVoiceScore(double voiceScore) {
		this.voiceScore = voiceScore;
	}
	public String getVoiceGrade() {
		return voiceGrade;
	}
	public void setVoiceGrade(String voiceGrade) {
		this.voiceGrade = voiceGrade;
	}
	public double getWorkcaseScore() {
		return workcaseScore;
	}
	public void setWorkcaseScore(double workcaseScore) {
		this.workcaseScore = workcaseScore;
	}
	public String getWorkcaseGrade() {
		return workcaseGrade;
	}
	public void setWorkcaseGrade(String workcaseGrade) {
		this.workcaseGrade = workcaseGrade;
	}

}
