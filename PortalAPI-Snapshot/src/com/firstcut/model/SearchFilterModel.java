package com.firstcut.model;

import java.util.HashSet;
import java.util.Set;

public class SearchFilterModel {

	private String searchParam;
	private String searchText;
	private Set<Integer> companyIds=new HashSet<>(0);
	private Set<Integer> jobIds=new HashSet<>(0);
	private boolean skipDeadlineOver;
	private Set<String> jobStatuses;

	public String getSearchParam() {
		return searchParam;
	}

	public void setSearchParam(String searchParam) {
		this.searchParam = searchParam;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Set<Integer> getCompanyIds() {
		return companyIds;
	}

	public void setCompanyIds(Set<String> companyIds) {
		for (String id : companyIds) {
			try {
				int companyId = Integer.parseInt(id);
				this.companyIds.add(companyId);
			} catch (Exception e) {
				continue;
			}
		}
	}

	public Set<Integer> getJobIds() {
		return jobIds;
	}

	public void setJobIds(Set<String> jobIds) {
		for (String id : jobIds) {
			try {
				int jobId = Integer.parseInt(id);
				this.jobIds.add(jobId);
			} catch (Exception e) {
				continue;
			}
		}
	}

	public boolean isSkipDeadlineOver() {
		return skipDeadlineOver;
	}

	public void setSkipDeadlineOver(boolean skipDeadlineOver) {
		this.skipDeadlineOver = skipDeadlineOver;
	}

	public Set<String> getJobStatuses() {
		return jobStatuses;
	}

	public void setJobStatuses(Set<String> jobStatuses) {
		this.jobStatuses = jobStatuses;
	}

}
