package com.firstcut.model;

import java.io.Serializable;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.mettl.model.mettlApis.v1.ApiError;
import com.mettl.model.mettlApis.v1.ApiErrorType;
import com.mettl.model.mettlApis.v1.MettlApiException;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ErrorModel implements Serializable {
	private static final long serialVersionUID = -271581896020647350L;
	private String errorType;
	private int code;
	private String message;

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static ErrorModel build(Exception e,String statuscode) {
		ErrorModel err = new ErrorModel();
		err.setMessage(e.getMessage());
		return err;
	}
}
