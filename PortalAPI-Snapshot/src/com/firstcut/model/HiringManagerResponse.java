package com.firstcut.model;

public class HiringManagerResponse {

	private int userId;
	private String fullName;
	private CompanyProfileResponse companyProfile;

	
	public CompanyProfileResponse getCompanyProfile() {
		return companyProfile;
	}

	public void setCompanyProfile(CompanyProfileResponse companyProfile) {
		this.companyProfile = companyProfile;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getUserId() {
		return this.userId;
	}
}
