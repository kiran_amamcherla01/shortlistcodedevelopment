package com.firstcut.model;

import java.util.List;

import com.firstcut.dto.AudioInterview;
import com.firstcut.dto.Psychometric;
import com.firstcut.dto.CaseAnswer;

public class AssessmentStatusResponse {
	private boolean psychometric;
	private boolean audioInterview;
	private boolean workcase;
	private List<CaseAnswer> workcases;
	private List<AudioInterview> audioInterviews;
	private List<Psychometric> psychometrics;

	public boolean isPsychometric() {
		return psychometric;
	}

	public void setPsychometric(boolean psychometric) {
		this.psychometric = psychometric;
	}

	public boolean isAudioInterview() {
		return audioInterview;
	}

	public void setAudioInterview(boolean audioInterview) {
		this.audioInterview = audioInterview;
	}

	public boolean isWorkcase() {
		return workcase;
	}

	public void setWorkcase(boolean workcase) {
		this.workcase = workcase;
	}

	public List<CaseAnswer> getWorkcases() {
		return workcases;
	}

	public void setWorkcases(List<CaseAnswer> workcases) {
		this.workcases = workcases;
	}

	public List<AudioInterview> getAudioInterviews() {
		return audioInterviews;
	}

	public void setAudioInterviews(List<AudioInterview> audioInterviews) {
		this.audioInterviews = audioInterviews;
	}

	public List<Psychometric> getPsychometrics() {
		return psychometrics;
	}

	public void setPsychometrics(List<Psychometric> psychometrics) {
		this.psychometrics = psychometrics;
	}

}
