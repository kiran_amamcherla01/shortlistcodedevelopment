package com.firstcut.model;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.firstcut.constant.Constant;
import com.firstcut.enums.ApiResponseStatusType;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ResponseModel {
	private ErrorModel error;
	private Object data;
	private ApiResponseStatusType status;
	private String message;

	public ErrorModel getError() {
		return error;
	}

	public void setError(ErrorModel error) {
		this.error = error;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public ResponseModel setMessage(String message) {
		this.message = message;
		return this;
	}

	public static ResponseModel build(Object data) {
		ResponseModel response = new ResponseModel();
		response.setStatus(ApiResponseStatusType.SUCCESS);
		response.setData(data);
		return response;
	}
	
	public static ResponseModel build(ApiResponseStatusType type,Object data) {
		ResponseModel response = new ResponseModel();
		response.setStatus(type);
		response.setData(data);
		return response;
	}
	
	public static ResponseModel build(ApiResponseStatusType type) {
		ResponseModel response = new ResponseModel();
		response.setStatus(type);
		return response;
	}

	public Object getData() {
		return data;
	}

	public ApiResponseStatusType getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public static ResponseModel build() {
		ResponseModel response = new ResponseModel();
		response.setStatus(ApiResponseStatusType.SUCCESS);
		return response;
	}

	public void setStatus(ApiResponseStatusType status) {
		this.status = status;
	}

}
