package com.firstcut.model;

// default package
// Generated Jul 15, 2015 7:20:39 PM by Hibernate Tools 4.0.0

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.firstcut.dto.Media;

public class CompanyProfileResponse {

	private Integer id;
	private String industry;
	private String companyName;
	@JsonIgnore
	private String companyLogoUrl;
	private String twitter;
	private String function;
	private String linkedinUrl;
	private String facebookUrl;
	private String shortDesc;
	private String longDesc;
	private String foundedYear;
	private String website;
	private Integer employeeCount;
	private String founders;
	private String investors;
	private String boardMembers;
	private String location;
	private String otherLocations;
	private String products;
	private String categories;
	private String city;
	private String state;
	private String country;
	private String zipcode;
	private String culture;
	private String strengths;
	private Set<Media> medias;

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getOtherLocations() {
		return otherLocations;
	}

	public void setOtherLocations(String otherLocations) {
		this.otherLocations = otherLocations;
	}

	public String getCulture() {
		return culture;
	}

	public void setCulture(String culture) {
		this.culture = culture;
	}

	public String getStrengths() {
		return strengths;
	}

	public void setStrengths(String strengths) {
		this.strengths = strengths;
	}

	public String getIndustry() {
		return this.industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@JsonProperty
	public String getCompanyLogoUrl() {
		return this.companyLogoUrl;
	}

	@JsonIgnore
	public void setCompanyLogoUrl(String companyLogoUrl) {
		this.companyLogoUrl = companyLogoUrl;
	}

	public String getLinkedinUrl() {
		return this.linkedinUrl;
	}

	public void setLinkedinUrl(String linkedinUrl) {
		this.linkedinUrl = linkedinUrl;
	}

	public String getFacebookUrl() {
		return this.facebookUrl;
	}

	public void setFacebookUrl(String facebookUrl) {
		this.facebookUrl = facebookUrl;
	}

	public String getShortDesc() {
		return this.shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return this.longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public String getFoundedYear() {
		return foundedYear;
	}

	public void setFoundedYear(String foundedYear) {
		this.foundedYear = foundedYear;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Integer getEmployeeCount() {
		return this.employeeCount;
	}

	public void setEmployeeCount(Integer employeeCount) {
		this.employeeCount = employeeCount;
	}

	public String getFounders() {
		return this.founders;
	}

	public void setFounders(String founders) {
		this.founders = founders;
	}

	public String getInvestors() {
		return this.investors;
	}

	public void setInvestors(String investors) {
		this.investors = investors;
	}

	public String getBoardMembers() {
		return this.boardMembers;
	}

	public void setBoardMembers(String boardMembers) {
		this.boardMembers = boardMembers;
	}

	public String getProducts() {
		return this.products;
	}

	public void setProducts(String products) {
		this.products = products;
	}

	public String getCategories() {
		return this.categories;
	}

	public void setCategories(String categories) {
		this.categories = categories;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipcode() {
		return this.zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public Set<Media> getMedias() {
		return this.medias;
	}

	public void setMedias(Set<Media> medias) {
		this.medias = medias;
	}

}
