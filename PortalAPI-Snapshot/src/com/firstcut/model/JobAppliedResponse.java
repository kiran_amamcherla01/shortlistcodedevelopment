package com.firstcut.model;

import java.util.Date;

public class JobAppliedResponse {
	private Integer id;
	private JobOpeningResponse jobOpening;
	private CandidateProfileResponse candidateProfile;
	private String jobStatus;
	private String emailBlockStatus;
	private String emailStatus;
	private String rejectionReason;
	private String summary;
	private String salaryNegotiable;
	private String willingToRelocate;
	private Date modifyDate;
	private CandidateScoringResponse candidateScoring;

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public String getEmailBlockStatus() {
		return emailBlockStatus;
	}

	public void setEmailBlockStatus(String emailBlockStatus) {
		this.emailBlockStatus = emailBlockStatus;
	}

	public CandidateScoringResponse getCandidateScoring() {
		return candidateScoring;
	}

	public void setCandidateScoring(CandidateScoringResponse candidateScoring) {
		this.candidateScoring = candidateScoring;
	}

	public String getSalaryNegotiable() {
		return salaryNegotiable;
	}

	public void setSalaryNegotiable(String salaryNegotiable) {
		this.salaryNegotiable = salaryNegotiable;
	}

	public String getWillingToRelocate() {
		return willingToRelocate;
	}

	public void setWillingToRelocate(String willingToRelocate) {
		this.willingToRelocate = willingToRelocate;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public JobOpeningResponse getJobOpening() {
		return jobOpening;
	}

	public void setJobOpening(JobOpeningResponse jobOpening) {
		this.jobOpening = jobOpening;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public CandidateProfileResponse getCandidateProfile() {
		return candidateProfile;
	}

	public void setCandidateProfile(CandidateProfileResponse candidateProfile) {
		this.candidateProfile = candidateProfile;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

}
