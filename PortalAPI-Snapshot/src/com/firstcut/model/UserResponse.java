package com.firstcut.model;

// default package
// Generated Jul 15, 2015 7:20:39 PM by Hibernate Tools 4.0.0

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.firstcut.dto.UserRole;
import com.firstcut.enums.UserType;
import com.firstcut.util.TokenGenerator;

/**
 * UserResponse generated by hbm2java
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = UserResponse.class)
public class UserResponse implements java.io.Serializable {

	private static final long serialVersionUID = -1076627515559229766L;
	private Integer id;
	private String userName;
	private UserType userType;
	private String authToken;
	private Set<UserRole> userRole;
	private Date lastLogin = new Date();
	private CandidateProfileResponse candidateProfile;
	private HiringManagerResponse hiringManager;

	public String generateAuthToken() {
		this.authToken = TokenGenerator.generateToken();
		return authToken;
	}

	public Set<UserRole> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	public void disableAuthToken() {
		this.authToken = null;
	}

	public Integer getId() {
		return this.id;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public UserType getUserType() {
		return userType;
	}

	public String getAuthToken() {
		return this.authToken;
	}

	public Date getLastLogin() {
		return this.lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public CandidateProfileResponse getCandidateProfile() {
		return candidateProfile;
	}

	public void setCandidateProfile(CandidateProfileResponse candidateProfile) {
		this.candidateProfile = candidateProfile;
	}

	public HiringManagerResponse getHiringManager() {
		return hiringManager;
	}

	public void setHiringManager(HiringManagerResponse hiringManager) {
		this.hiringManager = hiringManager;
	}

}
