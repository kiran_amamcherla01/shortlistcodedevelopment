package com.firstcut.model;

import java.util.Set;

public class AppliedCandidatesResponse {
	private Integer id;
	private String jobTitle;
	private String companyFit;
	private String jobFit;
	private String startupFit;
	private String voiceQuestions;
	private Set<JobAppliedResponse> jobApplieds;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCompanyFit() {
		return companyFit;
	}

	public void setCompanyFit(String companyFit) {
		this.companyFit = companyFit;
	}

	public String getJobFit() {
		return jobFit;
	}

	public void setJobFit(String jobFit) {
		this.jobFit = jobFit;
	}

	public String getStartupFit() {
		return startupFit;
	}

	public void setStartupFit(String startupFit) {
		this.startupFit = startupFit;
	}

	public String getVoiceQuestions() {
		return voiceQuestions;
	}

	public void setVoiceQuestions(String voiceQuestions) {
		this.voiceQuestions = voiceQuestions;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Set<JobAppliedResponse> getJobApplieds() {
		return jobApplieds;
	}

	public void setJobApplieds(Set<JobAppliedResponse> jobApplieds) {
		this.jobApplieds = jobApplieds;
	}

}
