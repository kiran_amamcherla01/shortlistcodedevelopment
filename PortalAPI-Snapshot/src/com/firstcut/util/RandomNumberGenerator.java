package com.firstcut.util;

public class RandomNumberGenerator {
	/*
	 * private ArrayList<Integer> numbersList = new ArrayList<Integer>();
	 * 
	 * public RandomNumberGenerator(int length) { for (int x = 1; x <= length;
	 * x++) numbersList.add(x); Collections.shuffle(numbersList); }
	 * 
	 * public int generateNewRandom(int n) { return numbersList.get(n); }
	 */

	public static long perfectlyHashThem(int a, int b) {
		long A = (long) (a >= 0 ? 2 * (long) a : -2 * (long) a - 1);
		long B = (long) (b >= 0 ? 2 * (long) b : -2 * (long) b - 1);
		long C = (long) ((A >= B ? A * A + A + B : A + B * B) / 2);
		return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
	}

	public static int perfectlyHashThem(short a, short b) {
		int A = (int) (a >= 0 ? 2 * a : -2 * a - 1);
		int B = (int) (b >= 0 ? 2 * b : -2 * b - 1);
		int C = (int) ((A >= B ? A * A + A + B : A + B * B) / 2);
		return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
	}
}