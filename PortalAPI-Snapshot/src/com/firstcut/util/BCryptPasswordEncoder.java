package com.firstcut.util;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCrypt;

@SuppressWarnings("deprecation")
public class BCryptPasswordEncoder implements PasswordEncoder {

	public String encodePassword(String rawPass, Object salt) {
		return BCrypt.hashpw(rawPass, salt.toString());
	}

	public String encodePassword(String rawPass) {
		String salt = BCrypt.gensalt();
		return BCrypt.hashpw(rawPass, salt);
	}

	public boolean isPasswordValid(String encPass, String rawPass, Object salt) {
		String hashpw = BCrypt.hashpw(rawPass, salt.toString());
		return hashpw.equals(encPass);
	}

	public static void main(String[] args) {
//		String gensalt = BCrypt.gensalt();
//		System.out.println(gensalt);
//		String encodePassword = new BCryptPasswordEncoder().encodePassword(
//				"admin123", gensalt);
//		System.out.println(encodePassword);
//		System.out.println(new BCryptPasswordEncoder().isPasswordValid(
//				encodePassword, "admin123", gensalt));
//		System.out.println(new Timestamp(new Date().getTime()));
	}

}
