package com.firstcut.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public class TokenGenerator {

	public static String generateToken() {
		String token = null;
		try {
			// Initialize SecureRandom
			SecureRandom prng = SecureRandom.getInstance("SHA1PRNG");
			// generate a random number
			String randomNum = new Integer(prng.nextInt()).toString();
			// get its digest
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] result = sha.digest(randomNum.getBytes());
			token = hexEncode(result);
		} catch (NoSuchAlgorithmException ex) {
			System.err.println(ex);
		}
		return token;
	}
	
	public static String generateOAuthToken() {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("/PortalAPI/oauth/token");
		String accessToken = "";
		try {

			httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");
			httppost.addHeader("Authorization", "Basic a2lyYW46cGFzc3dvcmQ=");

			/* set the post body request */
			StringEntity entity = new StringEntity("grant_type=client_credentials");
			httppost.setEntity(entity);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String serviceresponse = httpclient.execute(httppost, responseHandler);
			System.out.println("serviceresponse : " + serviceresponse);
			JSONObject response = new JSONObject(serviceresponse);
			accessToken = response.getString("access_token");
			System.out.println("accessToken : " + accessToken);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
		}
		return accessToken;
	}

	static private String hexEncode(byte[] aInput) {
		StringBuilder result = new StringBuilder();
		char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		for (int idx = 0; idx < aInput.length; ++idx) {
			byte b = aInput[idx];
			result.append(digits[(b & 0xf0) >> 4]);
			result.append(digits[b & 0x0f]);
		}
		return result.toString();
	}
}
