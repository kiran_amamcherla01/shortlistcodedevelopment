package com.firstcut.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;

public class FileOperations {
	public static String writeFile(String base64String, String fileName,String fileExtension) {
		String filePath = null;
		try {
			if (base64String == null || base64String.isEmpty()) {
				throw new Exception("Base64 string is null");
			}
			File temp = File.createTempFile(fileName, fileExtension);
			byte[] fileBytes = Base64.decodeBase64(base64String.getBytes());
			FileOutputStream fileOuputStream = new FileOutputStream(temp);
			fileOuputStream.write(fileBytes);
			fileOuputStream.close();
			filePath = temp.getAbsolutePath();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filePath;
	}
	
	public static String readMutilPartFile(InputStream inputStream, String fileName,String fileExtension) throws Exception {
		  long time = new Date().getTime();
		  fileName = fileName+"_"+time;
		  File temp = File.createTempFile(fileName, fileExtension);
		  FileOutputStream outputStream = new FileOutputStream(temp);
		  byte[] bytes = new byte[1024];
		  int bytesRead = -1;
		  do {
		   bytesRead = inputStream.read(bytes);
		   if (bytesRead != -1) {
		    outputStream.write(bytes, 0, bytesRead);
		   }
		  } while (bytesRead != -1);
		  outputStream.flush();
		  outputStream.close();
		  return temp.getAbsolutePath();
		 }
}
