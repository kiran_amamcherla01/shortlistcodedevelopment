package com.firstcut.util;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

import com.firstcut.constant.Constant;

public class AESEncryptionUtil {
	
	public static String getAESEncryptedString(String originalString){
		String encryptedString;
		try {
			final Cipher encryptCipher = Cipher.getInstance("AES");	        				
			encryptCipher.init(Cipher.ENCRYPT_MODE, generateMySQLAESKey(Constant.AES_KEY, "UTF-8"));
			encryptedString=new String(Hex.encodeHex(encryptCipher.doFinal(originalString.getBytes("UTF-8")))).toUpperCase();
	    } catch (Exception e) {
	    	encryptedString=null;
	    } 
		return encryptedString;
	}

	public static String getAESDecryptedString(String encryptedString){
		String originalString;
		try {
			final Cipher decryptCipher = Cipher.getInstance("AES");	        				
			decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(Constant.AES_KEY, "UTF-8"));
			originalString=new String(decryptCipher.doFinal(Hex.decodeHex(encryptedString.toCharArray())));
	    }  catch (Exception e) {
	    	originalString=null;
	    } 
		return originalString;
	}
	
	private static SecretKeySpec generateMySQLAESKey(final String key, final String encoding) {
		try {
			final byte[] finalKey = new byte[16];
			int i = 0;
			for(byte b : key.getBytes(encoding))
				finalKey[i++%16] ^= b;			
			return new SecretKeySpec(finalKey, "AES");
		} catch(UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void main(String[] args){
		System.out.println(getAESEncryptedString("niranjan@gmail.com"));
		System.out.println(getAESDecryptedString("701A1D4E533B39A0E71D100F473047AD1DDDEBC9516DB1A516F0525510E44A17"));
		
	}
}
