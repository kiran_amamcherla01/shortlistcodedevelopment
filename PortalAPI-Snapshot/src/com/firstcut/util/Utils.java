package com.firstcut.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.firstcut.constant.Constant;
import com.firstcut.dto.CandidateProfile;
import com.firstcut.dto.JobOpening;
import com.notify.model.ContentModel;

public class Utils {

	public static ContentModel getContentModel(CandidateProfile candidate,
			JobOpening jobOpening) {
		ContentModel contentModel = new ContentModel();
		if (candidate != null) {
			contentModel.setFullName(getFirstNameFromFullName(candidate.getFullName()));
			contentModel.setEmail(candidate.getEmail());
			contentModel.setCurrentCompanyName(candidate.getCurrentCompany());
			contentModel.setSource(candidate.getSource().toString());
			contentModel.setCurrentJobTitle(candidate.getCurrentJobTitle());
		}
		if (jobOpening != null) {
			if (jobOpening.getSubmissionDeadline() != null) {
				DateFormat dateInstance = SimpleDateFormat.getDateInstance();
				Calendar instance = Calendar.getInstance();
				instance.setTime(jobOpening.getSubmissionDeadline());
				String format = dateInstance.format(instance.getTime());
				contentModel.setSubmissionDeadline(format);
			}
			contentModel.setJobSummary(jobOpening.getJobSummary());
			contentModel.setJobTitle(jobOpening.getJobTitle());
			if (jobOpening.getCompanyProfile() != null) {
				contentModel.setLongDesc(jobOpening.getCompanyProfile()
						.getLongDesc());
				contentModel.setCompanyName(jobOpening.getCompanyProfile()
						.getCompanyName());
				contentModel.setWebsite(jobOpening.getCompanyProfile()
						.getWebsite());
				contentModel.setCompanyImage(jobOpening.getCompanyProfile()
						.getMedia().getUrl());
				contentModel.setJobLink(ResourceManager
						.getProperty(Constant.BASE_URL)
						+ "/#/jobdetails/"
						+ jobOpening.getCompanyProfile().getCompanyName()
						+ "/"
						+ jobOpening.getId());
			}
			contentModel.setSurveyLink(ResourceManager
					.getProperty(Constant.SURVEY_LINK));
		}
		return contentModel;

	}

	public static boolean checkNUllOrEmpty(String input) {
		if (input == null || input.trim().isEmpty()){
			return true;
		}
		return false;
	}

	public static boolean stringContainsItemFromList(String input,
			List<String> items) {
		if (input != null) {
			for (int i = 0; i < items.size(); i++) {
				if (input.contains(items.get(i))) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean isDeadlineOver(Date deadline) {
		Date currentDate = getZeroTimeDate(new Date());
		return deadline.before(currentDate);
	}

	public static boolean isDeadlineYetToOver(Date deadline) {
		Date currentDate = getZeroTimeDate(new Date());
		return deadline.after(currentDate);
	}

	public static Date getZeroTimeDate(Date fecha) {
		Date resultDate = fecha;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		resultDate = calendar.getTime();
		return resultDate;
	}
	
	private static String getFirstNameFromFullName(String fullname){
		String firstName="";
		if(fullname==null || fullname.trim().isEmpty()){
			firstName=fullname;
		}else{
			fullname=fullname.replaceAll("[-+_#$%@!&^:,]","").trim();
			String[] nameSplits=fullname.split("\\s+");
			if(nameSplits.length==1){
				firstName=nameSplits[0];
			}else if(nameSplits.length>1){
				firstName=nameSplits[0].length() > 2 ? nameSplits[0] : fullname;
			}else{
				firstName=fullname;
			}
		}
		return firstName;
	}
}
