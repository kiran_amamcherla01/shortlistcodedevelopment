package com.firstcut.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONObject;


public class OAuthTokenUtil {
	private static Logger logger = Logger.getLogger(OAuthTokenUtil.class);
	public static String getOAuthToken(String authToken) {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://localhost:8080/oauthsecurity-1.0/oauth/token");
		String serviceresponse = "";
		String oAuthToken ="";
		String basictoken = base64Encoder(authToken+":"+authToken);//this is set as username:password
		logger.info("Auth token OAuthTokenUtil "+authToken);
		logger.info("Basic token "+basictoken);
		try {

			httppost.addHeader("Content-Type", "application/x-www-form-urlencoded");
			httppost.addHeader("Authorization", "Basic "+basictoken);
			/* set the post body request */
			StringEntity entity = new StringEntity("grant_type=client_credentials");
			httppost.setEntity(entity);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			serviceresponse = httpclient.execute(httppost, responseHandler);
			logger.info("serviceresponse : " + serviceresponse);
			JSONObject response = new JSONObject(serviceresponse);
			oAuthToken = response.getString("access_token");
			logger.info("accessToken : " + oAuthToken);

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(Exception ex) {
			logger.info("Exception in getOAuthToken OAuthTokenUtil "+ex);
		}
		finally {
		}
		return oAuthToken;
	}

	private static String base64Encoder(String message) {
		byte[] encodedBytes = Base64.encodeBase64(message.getBytes());
		return new String(encodedBytes);
	}
	
	public static void main(String[] args) {
		String authToken = "e2513438d52ed8f86b0a427e2da85e724a1a8a81";
		System.out.println("oauth token "+getOAuthToken(authToken));
	}

}
