package com.firstcut.constant;

public class Constant {
	// Message constant
	public static final String SHORTLIST = "Shortlist";
	public static final String FORGOT_PASSWORD_SUBJECT = "forgot.password.subject";
	public static final String WELCOME_SUBJECT = "welcome.subject";
	public static final String FORGOT_PASSWORD_MESSAGE = "forgot.password.message";
	public static final String WELCOME_MESSAGE = "welcome.message";
	public static final String FORGOT_PASSWORD_SENT_MESSAGE = "forgot.password.sent.message";
	public static final String FACEBOOK = "facebook";
	public static final String LINKEDIN = "linkedin";
	public static final String DATBASE_EXCEPTION_MESSAGE = "unable to perform database operation";

	// exception constant
	public static final String INVALID_TOKEN_EXCEPTION = "invalid.user.token";
	public static final String INVALID_USERNAME_EXCEPTION = "invalid.username";
	public static final String INVALID_PASSWORD_EXCEPTION = "invalid.password";
	public static final String INVALID_USERTYPE_EXCEPTION = "invalid.usertype";
	public static final String UNVERIFIED_USER_EXCEPTION = "unverified.user";
	public static final String INACTIVE_USER_EXCEPTION = "inactive.user";
	public static final String EMAIL_FAILURE_EXCEPTION = "email.failure";
	public static final String USER_ALREADY_EXIST_EXCEPTION = "user.already.exist";
	public static final String CANDIDATE_ALREADY_EXIST_EXCEPTION = "candidate.already.exist";
	public static final String COMPANY_ALREADY_EXIST_EXCEPTION = "company.already.exist";
	public static final String CANDIDATE_NOT_EXIST_EXCEPTION = "candidate.not.exist";
	public static final String MEDIA_NOT_EXIST_EXCEPTION = "media.not.exist";
	public static final String MANAGER_NOT_EXIST_EXCEPTION = "manager.not.exist";
	public static final String JOBOPENING_NOT_EXIST_EXCEPTION = "jobopening.not.exist";
	public static final String JOBHISTORY_NOT_EXIST_EXCEPTION = "jobhistory.not.exist";
	public static final String COMPANY_NOT_EXIST_EXCEPTION = "company.not.exist";
	public static final String EDUCATION_NOT_EXIST_EXCEPTION = "education.not.exist";
	public static final String AUDIO_INTERVIEW_NOT_EXIST_EXCEPTION = "audio.interview.not.exist";
	public static final String WORKCASE_NOT_EXIST_EXCEPTION = "education.not.exist";
	public static final String CANDIDATE_NOT_APPLIED_FOR_JOB = "candidate.not.applied.job";
	public static final String CANDIDATE_ALREADY_APPLIED_FOR_JOB = "candidate.already.applied.job";
	public static final String JOB_ALREADY_EXPIRED = "job.already.expired";
	

	// Placeholder constant
	public static final String PASSWORD_HOLDER = ":password";
	public static final String VERIFICATION_LINK = ":verificationlink";
	public static final String VERIFICATION_URL = "email.verification";
	public static final String FULLNAME = ":fullname";

	// common constant
	public static final String SUCCESS = "Success";
	public static final String ERROR = "Error";
	public static final String USER = "user";
	public static final String SPACE = " ";
	public static final String UNDER_SCORE = "_";
	public static final String COMMA = ",";
	public static final String COMPLETE_STATUS = "COMPLETE";
	public static final String IMAGE = "image";
	public static final String VIDEO = "video";
	public static final String LONG = "LONG";
	public static final String SHORT = "SHORT";
	public static final String UG = "UG";
	public static final String PG = "PG";
	public static final String STAGING = "staging";
	public static final String PRODUCTION = "production";
	public static final String APP_ENV = "env";

	// veridu constant
	public static final String API_KEY = "8fb7e44daabf94dcfb1ce0d089967f14bfe031c1";
	public static final String SECRET_KEY = "95114585abd76630ab5328935062f3fafb51b9f5";
	public static final String VERIDU_VERSION = "0.4";
	public static final String BASE_URL = "base.url";
	public static final String SURVEY_LINK = "survey.link";
	

	// Mettl Constant
	public static final String METTL_SOURCE_APP_NAME = "Shortlist-APP";
	public static final String METTL_TEST_NAME = "Shortlist Career Personality Assessment";
	public static final String METTL_TEST_ACCESS_KEY = "9434c53";
	public static final String METTL_BASE_URL = "http://api.mettl.com";
	public static final String METTL_VERSION = "v1";
	public static final String METTL_PUBLIC_KEY = "70f857d3-e843-442a-af0b-d891cca33f84";
	public static final String METTL_PRIVATE_KEY = "b1966eac-017c-42e5-a148-814f99fdc738";

	// Interview Mocha
	public static final String MOCHA_BASE_URL = "http://api.interviewmocha.com/api";
	public static final String MOCHA_API_KEY = "WlSmIOkDGKJUynt2nlfWww";

	// Amazon S3
	public static final String S3_PATH = "https://s3-ap-southeast-1.amazonaws.com/";
	public static final String COMPANYLOGO_FOLDER = "companylogo";
	public static final String PROFILEPIC_FOLDER = "profilepic";
	public static final String MEDIA_FOLDER = "medias";
	public static final String RESUME_FOLDER = "resumes";
	
	//Task Constant
	public static final int TWO_DAYS_INTERVAL = 2;
//	public static final int INACTIVE_FOLLOWUP2_INTERVAL = 2;
//	public static final int INCOMPLETE_FOLLOWUP1_INTERVAL = 2;
//	public static final int INCOMPLETE_FOLLOWUP2_INTERVAL = 2;
//	public static final int REJECTION_CLOSURE_INTERVAL = 2;
	public static final int SEVEN_DAYS_INTERVAL = 7;
	
	public static final int ZERO_DAYS_INTERVAL = 0;
	public static final String COMPANY_PLACEHOLDER = "<<COMPANY>>";
	
	public static final String FOLLOWUP="FOLLOWUP";
	public static final String TRUE="true";
	public static final String NOTIFICATION_ENABLED="notifications.enabled";
	public static final String DELIMITER_DOUBLE_PIPE="||";
	public static final String PRESCREEN="PRESCREEN";
	public static final String POSTSCREEN="POSTSCREEN";
	
	//Employer rejection reasons
	public static final String PREVIOUS_EXPERIENCE_NOT_RELEVANT="Previous experience not relevant";
	public static final String TOO_SENIOR_FOR_THE_ROLE="Too senior for the role";
	public static final String COMMUNICATION_SKILLS_NOT_STRONG_ENOUGH="Communication skills not strong enough";
	public static final String CASE_EXERCISE_NOT_STRONG_ENOUGH="Case exercise not strong enough";
	public static final String CULTURAL_FIT_NOT_RIGHT="Cultural fit not right";
	
	//Employer rejection messages
	public static final String PREVIOUS_EXPERIENCE_NOT_RELEVANT_MESSAGE="your experience is not quite right for this role, and they will not be moving your application forward.";
	public static final String TOO_SENIOR_FOR_THE_ROLE_MESSAGE="your experience is not quite right for this role, and they will not be moving your application forward.";
	public static final String COMMUNICATION_SKILLS_NOT_STRONG_ENOUGH_MESSAGE="your voice assessment was not as strong as some of the other candidates for this role, and they will not be moving your application forward.";
	public static final String CASE_EXERCISE_NOT_STRONG_ENOUGH_MESSAGE="your case exercise was not as strong as some of the other candidates for this role, and they will not be moving your application forward.";
	public static final String CULTURAL_FIT_NOT_RIGHT_MESSAGE="your experience is not quite right for this role, and they will not be moving your application forward.";
	public static final String OTHER_REJECTION_REASON_MESSAGE="your experience is not quite right for this role, and they will not be moving your application forward.";
	
	public static final String AES_KEY="careercalling";

}
